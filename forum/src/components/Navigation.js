import { Link } from "react-router-dom";
import { Navbar } from "react-bootstrap";
import { Nav } from "react-bootstrap";
import { Container } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { useContext, useState } from "react";
import AuthContext from "../providers/AuthContext";
import { useHistory } from 'react-router-dom';
import Modal from 'react-bootstrap/Modal';

const Navigation = () => {

  const { user, setUser } = useContext(AuthContext);

  const history = useHistory();


  const logOut = () => {
    setUser(null);
    localStorage.removeItem('token');
    history.push('/public');
  };

  function Example() {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
      <>
        <Button variant="dark" style={{ fontSize: '22px' }} onClick={handleShow}>Log out</Button>

        <Modal
          show={show}
          onHide={handleClose}
          backdrop="static"
          keyboard={false}
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title>Are you sure you want to log out?</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Press Cancel to go back at the page
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Cancel
            </Button>
            <Button variant="primary" onClick={logOut}>Confirm</Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  };


  return (
    <div>
      {user ?
        <div>
          <Navbar bg="dark" variant="dark">
            <Container>
              <Navbar.Brand><Link to='/home'><Button variant='dark' style={{ fontSize: '22px' }}>Home</Button></Link></Navbar.Brand>
              <Nav className="me-auto">
                <Nav.Link >
                  <Link to='/allPosts'>
                    <Button variant='dark' style={{ fontSize: '22px' }}>
                      Posts
                    </Button>
                  </Link>
                </Nav.Link>
                <Nav.Link>
                  <Link to="/users">
                    <Button variant='dark' style={{ fontSize: '22px' }}>
                      Users
                    </Button>
                  </Link>
                </Nav.Link>
                <Nav.Link>
                  <Link to={`/profile/${user?.id}`}>
                    <Button variant='dark' style={{ fontSize: '22px' }}>
                      Profile
                    </Button>
                  </Link>
                </Nav.Link>
                <Example />
              </Nav>
            </Container>
          </Navbar>
        </div>
        :
        <div>
          <Navbar bg="dark" variant="dark">
            <Container>
              <Navbar.Brand ><Link to='/home'> <Button variant='dark' style={{ fontSize: '22px' }}>
                Home
              </Button></Link></Navbar.Brand>
              <Nav className="me-auto">
                <Nav.Link>
                  <Link to="/public">
                    <Button variant='dark' style={{ fontSize: '22px' }}>
                      Login / Register
                    </Button>
                  </Link>
                </Nav.Link>
              </Nav>
            </Container>
          </Navbar>
        </div>
      }
    </div >
  )
};

export default Navigation;