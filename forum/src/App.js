
import './App.css';
import Navigation from './components/Navigation';
import AllPosts from './pages/All-posts/All-posts';
import CreatePosts from './pages/Create-posts/Create-posts';
import HomePage from './pages/Home/Home';
import { SinglePostView } from './pages/Single-post/SinglePost';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import PublicPage from './pages/LoginAndRegister/PublicPage';
import { useState } from 'react';
import AllUsers from './pages/Users/Users';
import SingleUserView from './pages/Users/SingleUsersView.js'
import { useEffect } from 'react';
import AuthContext from './providers/AuthContext';
import { getUserFromToken } from './utils/Token';
import Profile from './pages/Profile/Profile';
import { getNews } from './requests/NewsRequests';
import NewsContext from './providers/NewsContext';

function App() {
  const [user, setUser] = useState(getUserFromToken(localStorage.getItem('token')));
  const [news, setNews] = useState({ articles: [] });

  useEffect(() => {
    if (!user) return;

    const timer = setTimeout(() => {
      setUser(null);
      localStorage.removeItem('token');
    }, 360000000);

    return () => clearTimeout(timer);
  }, [user]);

  useEffect(() => {
    getNews()
      .then(data => setNews(data));
  }, []);

  return (
    <BrowserRouter>
      <AuthContext.Provider value={{ user, setUser }}>
        <NewsContext.Provider value={news}>
          <div className="App">
            <Navigation />
            <Switch>
              <Redirect path="/" exact to="/home" />
              <Route exact path="/public" component={PublicPage} />
              <Route exact path="/home" component={HomePage} />
              <Route exact path="/allPosts" component={AllPosts} />
              <Route exact path="/createPosts" component={CreatePosts} />
              <Route exact path="/singlePost/:id" component={SinglePostView} />
              <Route exact path="/users" component={AllUsers} />
              <Route exact path="/users/:id" component={SingleUserView} />
              <Route exact path="/profile/:id" component={Profile} />
            </Switch>
          </div>
        </NewsContext.Provider>
      </AuthContext.Provider>
    </BrowserRouter >
  );
}

export default App;
