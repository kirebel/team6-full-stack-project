
import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import { useHistory } from 'react-router';
import { deletePost } from '../../requests/PostsRequests';

const DeletePostModal = ({ postId, deletePosts, setDeletePosts }) => {

    const history = useHistory();


    const deletePostFunc = (id) => {
        deletePost(id);
    };

    return (
        <Modal show={deletePosts}>
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                Are you sure about that?
            </div>
            <Modal.Body>
                <div style={{ textAlign: 'center' }}>
                    <Button variant='danger' onClick={() => { setDeletePosts(false) }}>
                        Cancel
                    </Button>&nbsp;
                    <Button variant='success' onClick={() => {
                        deletePostFunc(postId);
                        setDeletePosts(false);
                        history.goBack();
                    }}>
                        Submit
                    </Button>
                </div>
            </Modal.Body>
        </Modal>
    )

}

export default DeletePostModal;