import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import { flagPostInDatabase } from '../../requests/PostsRequests';

const FlagModal = ({ flagModalShow, setFlagModalShow, setFlagPost, postId }) => {

    return (
        <Modal show={flagModalShow}>
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                Are you sure about that? Only administrator can set the post not flagged.
            </div>
            <Modal.Body>
                <div style={{ textAlign: 'center' }}>
                    <Button variant='danger' onClick={() => { setFlagModalShow(false) }}>
                        Cancel
                    </Button>&nbsp;
                    <Button variant='success' onClick={() => {
                        flagPostInDatabase(postId);
                        setFlagPost(1);
                        setFlagModalShow(false)
                    }}>
                        Submit
                    </Button>
                </div>
            </Modal.Body>
        </Modal>
    )
}

export default FlagModal;