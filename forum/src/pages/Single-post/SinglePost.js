import React, { useEffect, useState } from "react";
import Post from "./Post";
import { getSinglePost, getPostLikes } from "../../requests/PostsRequests";
import AllCommentsOfAPost from "../Comments/Comments";
import { Card } from "react-bootstrap";
import PostLikes from "../Likes/PostLikes";
import backGroundImage from '../../media/posts-background.jfif'
import backGround from '../../media/background1.jpg'

export const SinglePostView = (props) => {
    const id = +props.match.params.id;

    const [post, setPost] = useState([]);
    const [lockPost, setLockPost] = useState(0);
    const [flagPost, setFlagPost] = useState(0);
    const [likes, setLikes] = useState([])


    useEffect(() => {
        getPostLikes(id)
            .then((data) => setLikes(data));
        getSinglePost(id)
            .then((data) => {
                setPost(data);
                setLockPost(data.isLocked);
                setFlagPost(data.isFlagged);
            })
    }, [id]);


    return (
        <div style={{ backgroundImage: `url(${backGround})`, height: '580px' }}>
            <Card style={{ width: '800px', backgroundImage: `url(${backGroundImage})` }}>
                <Card.Body>
                    <Card.Title>
                        <span>
                            <Post post={post} setPost={setPost}
                                isFlagged={post.isFlagged}
                                lockPost={lockPost}
                                setLockPost={setLockPost}
                                flagPost={flagPost}
                                setFlagPost={setFlagPost}
                                id={id} likes={likes}
                                setLikes={setLikes}
                                numberLikes={likes.length} />
                        </span>
                        <span>
                            <PostLikes likes={likes} setLikes={setLikes}
                                numberLikes={likes.length}
                                postId={id}
                                lockPost={lockPost} />
                            <hr></hr>
                        </span>
                    </Card.Title>
                    <Card.Text>
                        <AllCommentsOfAPost postId={id}
                            lockPost={lockPost}
                            setLockPost={setLockPost} />
                    </Card.Text>
                </Card.Body>
            </Card>
        </div>
    )
}



export default SinglePostView;