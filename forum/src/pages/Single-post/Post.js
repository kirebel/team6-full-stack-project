import { dateFormatter } from '../../common/HelpFunctions';
import deleteButton from '../../media/delete-button.png'
import editButton from '../../media/modify.png'
import { updatePostRequest, lockPostInTheDatabase, unLockPost, unFlagPost } from "../../requests/PostsRequests";
import { Button } from "react-bootstrap";
import { useState } from 'react';
import { useContext } from "react";
import AuthContext from '../../providers/AuthContext';
import DeletePostModal from './DeleteModal';
import LockIcon from '../../media/lock-icon.png';
import FlagIcon from '../../media/flag-icon.jpg';
import FlagModal from './FlagModal';



const Post = ({ post, setPost, flagPost, setFlagPost, lockPost, setLockPost, likes, setLikes, numberLikes, id }) => {

    const { user } = useContext(AuthContext);



    const [deleteMode, setInDeleteMode] = useState(false);
    const [editMode, setEditMode] = useState(false);
    const [minTitleLength, setMinTitleLength] = useState(false);
    const [minTextLength, setMinTextLength] = useState(false);
    const [postText, setPostText] = useState(post.text);
    const [postTitle, setPostTitle] = useState(post.title);
    const [flagModalShow, setFlagModalShow] = useState(false);

    const editPost = (id, data) => {
        updatePostRequest(id, data)
            .then(data => {
                setPost({ ...data, title: postTitle, text: postText })
            })
    };

    return (
        <>

            <div >
                {!editMode ?
                    <span>
                        {flagPost
                            ? <span> <span style={{ color: 'red', fontSize: '25px' }}>
                                This post is flagged!
                            </span> &nbsp;
                                {user?.role === 'admin' && flagPost === 1
                                    ? <Button variant='success' onClick={() => { unFlagPost(post.id); setFlagPost(0) }} >Unflag</Button>
                                    : null}
                            </span>
                            : null}
                        <div style={{ textAlign: 'left', fontSize: '30px', marginBottom: '15px' }} >
                            {post.title}
                        </div>
                        <hr></hr>
                        <div style={{ textAlign: 'left', fontWeight: 'normal' }}>
                            <span >
                                {post.username}
                            </span>
                            <span style={{ marginLeft: '50px' }}>{dateFormatter(post.post_date)}
                            </span>
                        </div>&nbsp;

                        <div style={{ fontSize: '18px', textAlign: 'left', marginBottom: '15px' }}>
                            {post.text}
                        </div>
                    </span>
                    :
                    <div >
                        <div>
                            Title
                        </div>
                        <div>
                            <textarea type="text" style={{ height: '150px', width: '450px' }} value={postTitle}
                                onChange={(e) => {
                                    if (e.target.value.length <= 10) {
                                        setMinTitleLength(true);
                                    } else {
                                        setMinTitleLength(false)
                                    } setPostTitle(e.target.value)
                                }}>
                            </textarea>&nbsp;
                            {minTitleLength
                                ? <div style={{ color: 'red' }}>You must write minimum 10 symbols!</div>
                                : null}
                        </div>
                        <div>
                            Text
                        </div>
                        <div>
                            <textarea type="text" style={{ height: '150px', width: '450px' }} value={postText} onChange={(e) => {
                                if (e.target.value.length <= 15) {
                                    setMinTextLength(true);
                                } else {
                                    setMinTextLength(false);
                                }
                                setPostText(e.target.value)
                            }}></textarea>
                            {minTextLength
                                ? <div style={{ color: 'red' }}>You must write minimum 15 symbols!</div>
                                : null}
                        </div>
                        <br></br>
                        <Button variant='danger'
                            onClick={() => {
                                setMinTextLength(false);
                                setMinTitleLength(false); setEditMode(false);
                            }}>
                            Cancel
                        </Button>&nbsp;
                        {!(minTextLength || minTitleLength)
                            ?
                            <Button variant='success' onClick={() => {
                                setEditMode(false);
                                editPost(post.id, { title: postTitle, text: postText });
                            }}>
                                Submit
                            </Button>
                            : null}
                    </div>
                }<div style={{ textAlign: 'right' }}>
                    {((user?.id === post.users_id || user?.role === 'admin') && user?.isBanned === 0 && lockPost === 0) ?
                        <span style={{ textAlign: 'right' }}>
                            <Button variant='light'
                                onClick={() => {
                                    setEditMode(true);
                                    setPostTitle(post.title); setPostText(post.text)
                                }}>
                                <img style={{ width: '30px', height: '30px' }} src={editButton} alt='Edit' />
                            </Button>
                            <Button variant='light' onClick={() => { setInDeleteMode(true) }}>
                                <img style={{
                                    width: '30px',
                                    height: '30px',
                                }} src={deleteButton} alt='Delete' />
                            </Button>
                            <DeletePostModal postId={post.id}
                                deletePosts={deleteMode} setDeletePosts={setInDeleteMode} />
                        </span>
                        : null}
                    <FlagModal flagModalShow={flagModalShow}
                        setFlagModalShow={setFlagModalShow}
                        setFlagPost={setFlagPost} postId={post.id} />
                    {flagPost === 0 && user?.isBanned === 0 && lockPost === 0
                        ? <Button variant='light' onClick={() => { setFlagModalShow(true); }}>
                            <img style={{ width: '30px', height: '30px' }} src={FlagIcon} alt='Flag' />
                        </Button>
                        : null}
                    <span>
                        {user?.role === 'admin'
                            ?
                            lockPost === 0
                                ? <Button variant='light'
                                    onClick={() => { setLockPost(1); lockPostInTheDatabase(post.id); }}>
                                    <img src={LockIcon} style={{ width: '30px', height: '30px' }} alt='Lock' />
                                </Button>
                                : <Button variant='success'
                                    onClick={() => { setLockPost(0); unLockPost(post.id); }}>Unlock</Button>
                            : null}
                    </span>
                </div>
            </div>
        </>
    )
}

export default Post;