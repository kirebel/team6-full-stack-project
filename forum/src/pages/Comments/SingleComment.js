
import React from 'react';
import { dateFormatter } from "../../common/HelpFunctions";
import { Button } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import deleteButton from '../../media/delete-button.png'
import editButtonPic from '../../media/modify.png'
import { useState, useEffect } from "react";
import { useContext } from "react";
import AuthContext from "../../providers/AuthContext";
import DeleteModal from "./DeleteModal";
import CommentEditMode from "./EditMode";
import { getCommentLikes } from '../../requests/CommentsRequests';
import CommentLikes from '../Likes/CommentLikes';

const SingleCommentView = (props) => {
    const [commentText, setCommentText] = useState(props.text)
    const [inEditMode, setInEditMode] = useState(false);
    const [deleteComment, setDeleteComment] = useState(false);
    const [inDeleteMode, setInDeleteMode] = useState(false);

    const [likes, setLikes] = useState([])

    const { user } = useContext(AuthContext);


    useEffect(() => {
        getCommentLikes(props.id)
            .then((data) => setLikes(data));
    }, [props.id]);



    return (
        <>
            {!deleteComment
                ?
                <div style={{ marginTop: '50px' }} >
                    <div style={{ textAlign: 'left', fontWeight: 'bold', marginBottom: '10px' }}>
                        <span >
                            {props.username}
                        </span> <span style={{ marginLeft: '50px' }}>{dateFormatter(props.post_date)}
                        </span>
                    </div>
                    <div style={{ textAlign: 'left' }}>
                        {props.text}
                    </div>
                    <div style={{ textAlign: 'right' }}>
                        {(user?.id === props.users_id || user?.role === 'admin') && user?.isBanned === 0 && props.lockPost === 0 ?
                            <span style={{ textAlign: 'right' }}>
                                <Button variant='light' onClick={() => { setInEditMode(true); setCommentText(props.text) }}>
                                    <img style={{
                                        width: '25px',
                                        height: '25px',
                                    }} src={editButtonPic} alt='Edit'
                                    />
                                </Button>&nbsp;
                                <Button variant='light' onClick={() => setInDeleteMode(true)}>
                                    <img style={{ width: '30px', height: '30px' }} src={deleteButton} alt='Delete' />
                                </Button>
                                <DeleteModal inDeleteMode={inDeleteMode} commentId={props.id}
                                    postId={props.commentId} setInDeleteMode={setInDeleteMode} setDeleteComment={setDeleteComment} />
                            </span>

                            : null}
                    </div>
                    <div>
                        {!inEditMode
                            ? null
                            : <CommentEditMode commentText={commentText} setCommentText={setCommentText}
                                setInEditMode={setInEditMode} userId={props.id}
                                postId={props.posts_id} comments={props.comments}
                                commentState={props.state} />}
                        <CommentLikes likes={likes}
                            lockPost={props.lockPost}
                            setLikes={setLikes} id={props.id}
                            numberLikes={likes.length}
                            postId={props.posts_id} />
                        <hr></hr>
                    </div>
                </div>
                : null}
        </>
    )
};


export default SingleCommentView;

