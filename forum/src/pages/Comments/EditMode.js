import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import { editComment } from '../../requests/CommentsRequests';

const CommentEditMode = ({ commentText, setCommentText, setInEditMode, userId, postId, comments, commentState }) => {

    const [minTextLength, setMinTextLength] = useState(true);

    const editCommentText = (commentId, postId, data) => {
        editComment(commentId, postId, data)
            .then((changedComment) => {
                const updated = comments.map(comment =>
                    comment.id === changedComment.id ? changedComment : comment)
                commentState(updated);
            });
    };

    return (
        <>
            <textarea type='text' style={{ height: '150px', width: '450px' }} value={commentText}
                onChange={(e) => {
                    if (e.target.value.length <= 5) {
                        setMinTextLength(false);
                    } else {
                        setMinTextLength(true);
                    }
                    setCommentText(e.target.value)
                }} />
            <div>
                {!minTextLength
                    ? <div style={{ color: 'red' }}>You must write minimum 5 symbols!</div>
                    : null}
                <Button variant='danger' onClick={() => { setInEditMode(false); setMinTextLength(false); }}>
                    Cancel
                </Button>&nbsp;
                {minTextLength
                    ?
                    <Button variant='success' onClick={() => {
                        setInEditMode(false);
                        editCommentText(userId, postId, { text: commentText });
                    }}>
                        Submit
                    </Button>
                    : null}
            </div>
        </>
    )
}


export default CommentEditMode;