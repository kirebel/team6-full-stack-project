import React, { useContext, useEffect, useState } from "react";
import { createComment, getAllCommentsOfAPost } from "../../requests/CommentsRequests";
import SingleCommentView from "./SingleComment";
import replyPic from './../../media/reply.jpg';
import { Button } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import AuthContext from "../../providers/AuthContext";


const AllCommentsOfAPost = ({ postId, lockPost, setLockPost }) => {

    const [comments, setComments] = useState([]);
    const [createForm, setCreateForm] = useState(false);
    const [commentText, setCommentText] = useState({
        text: ''
    });

    const [minTextLength, setTextMinLength] = useState(false);

    const { user } = useContext(AuthContext);

    const updateCommentText = (prop, value) => {
        commentText[prop] = value;
        setCommentText({ ...commentText })
    };

    const addComment = () => {
        createComment(commentText, postId)
            .then(res => setComments([...comments, res]))
            .then(setCreateForm(false))
    }

    useEffect(() => {
        getAllCommentsOfAPost(postId)
            .then((data) => {
                setComments(data);
            })
    }, [postId]);



    const commentsToShow = comments
        .sort((a, b) => new Date(a.post_date) - new Date(b.post_date))
        .map((comment, index) => {
            return (
                <SingleCommentView
                    key={comment.id}
                    id={comment.id}
                    {...comment}
                    state={setComments}
                    comments={comments}
                    index={index}
                    userId={comment.users_id}
                    lockPost={lockPost}
                    setLockPost={setLockPost}
                />
            )
        })

    return (
        <>
            <div>{commentsToShow}
                <div >
                    &nbsp;
                    <div>
                        {createForm ?
                            <div>
                                <div>Type your comment here</div>
                                <label htmlFor="comment-text"></label>
                                <textarea style={{ width: '300px', height: '150px' }} onChange={e => {
                                    if (e.target.value.length <= 5) {
                                        setTextMinLength(false)
                                    } else {
                                        setTextMinLength(true);
                                    } updateCommentText('text', e.target.value)
                                }}
                                    id="comment-text" name="comment-text"
                                    placeholder="Type your comment here"></textarea><br />
                                {minTextLength
                                    ? <Button variant='light' onClick={() => addComment()} >Add comment</Button>
                                    : null}
                                {!minTextLength
                                    ? <div style={{ color: 'red' }}>Minimum 5 symbols required!</div>
                                    : null}
                                <Button variant='light' onClick={() => { setCreateForm(false); }}>Back</Button>
                            </div> :
                            lockPost === 0 && user?.isBanned === 0
                                ? <Button variant='light' onClick={() => { setCreateForm(true) }}>
                                    <img style={{ width: '30px', height: '30px' }} src={replyPic} alt='Reply' />&nbsp;
                                    Add Comment
                                </Button>
                                : null}
                    </div>
                </div>
            </div>
        </>
    )

};






export default AllCommentsOfAPost;