import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import { removeComment } from '../../requests/CommentsRequests';


const DeleteModal = ({ inDeleteMode, setInDeleteMode, commentId, postId, setDeleteComment }) => {

    const remove = (commentId, postId) => {
        removeComment(commentId, postId)
            .then((deletedComment) => setDeleteComment(true));
    }

    return (
        <Modal show={inDeleteMode}>
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                Are you sure about that?
            </div>
            <Modal.Body>
                <div style={{ textAlign: 'center' }}>
                    <Button variant='danger' onClick={() => { setInDeleteMode(false) }}>
                        Cancel
                    </Button>&nbsp;
                    <Button variant='success' onClick={() => {
                        remove(commentId, postId);
                        setInDeleteMode(false);
                    }}>
                        Submit
                    </Button>
                </div>
            </Modal.Body>
        </Modal>
    )
}

export default DeleteModal;