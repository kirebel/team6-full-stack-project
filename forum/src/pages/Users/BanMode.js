import React, { useState } from 'react';
import { banUser } from '../../requests/UsersRequests';
import { Button } from 'react-bootstrap';


const BanMode = ({ user, setInBanMode, setUser, setBanState }) => {

    const [banReason, setBanReason] = useState('');

    const ban = (id, data) => {
        banUser(id, data)
            .then(bannedUser => setUser({ ...bannedUser, isBanned: 1 }));
    }

    return (
        <div>
            <span>
                <label htmlFor="ban_reason-text">Ban reason:</label>
                < input maxLength={100} value={banReason}
                    onChange={e => { setBanReason(e.target.value); }}
                    name="ban_reason-text"
                /><br />
            </span>
            <div>
                <Button variant='danger' onClick={() => { setInBanMode(false) }}>
                    Cancel
                </Button>&nbsp;
                <Button variant='success' onClick={() => {
                    ban(user.id, { ban_reason: banReason });
                    setInBanMode(false);
                    setBanState(true);
                }}>
                    Submit
                </Button>
                <hr></hr>
            </div>
        </div>
    )

};



export default BanMode;