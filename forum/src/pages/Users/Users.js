import React, { useState } from 'react';
import { useEffect } from 'react';
import { showAllUsers } from '../../requests/UsersRequests';
import { Link } from 'react-router-dom';
import { Card } from 'react-bootstrap';
import './Users.css';
import MostCommentedAndLikedPosts from '../MostLikedAndCommentedPosts/MostCommentedAndLikedPosts';
import { Container, Row, Col } from 'react-bootstrap';
import backGround from '../../media/background1.jpg'
import cardBackGround from '../../media/posts-background.jfif'

const AllUsers = () => {

    const [users, setUsers] = useState([]);

    useEffect(() => {
        showAllUsers().then((data) => {
            setUsers(data);
        });
    }, [])

    return (
        <div style={{ backgroundImage: `url(${backGround})` }}>
            <Container >
                <Row>
                    <Col> <div className="postsView">
                        {users.map(u =>

                            <Card key={u.id} style={{
                                width: '550px', textAlign: 'left',
                                backgroundImage: `url(${cardBackGround})`,
                                marginBottom: '15px', marginTop: '15px'
                            }}
                                className='card'>
                                <Card.Body>
                                    <Card.Title>
                                        <Link to={`/users/${u.id}`} className='link' style={{ fontSize: '20px' }}>
                                            {u.username}&nbsp;&nbsp;&nbsp;
                                        </Link>
                                        {u.role === 'admin'
                                            ? <span style={{ fontWeight: 'normal', fontSize: '18px' }}>administrator</span>
                                            : null}
                                        <span style={{ marginLeft: '50px', textAlign: 'right' }}>
                                            Registered on: {new Date(u.reg_date).toLocaleDateString()}
                                        </span>
                                    </Card.Title>
                                </Card.Body>
                            </Card>
                        )}
                    </div></Col>
                    <Col><MostCommentedAndLikedPosts /></Col>
                </Row>

            </Container>
        </div>
    )

};




export default AllUsers;


