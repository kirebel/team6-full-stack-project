import React from 'react';
import { useEffect, useState, useContext } from 'react';
import { getSingleUser, getUserLikedComments, getUserLikedPosts, unbanUser } from '../../requests/UsersRequests';
import { getFriendshipInfo } from '../../requests/UsersRequests';
import { Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import deleteButton from '../../media/delete-button.png'
import editButtonPic from '../../media/modify.png'
import banButton from '../../media/ban.jpg'
import EditMode from './EditMode';
import BanMode from './BanMode';
import DeleteModal from './DeleteModal';
import AuthContext from '../../providers/AuthContext';
import Activity from '../Activity/Activity';
import FriendshipState from '../Friendship/FriendshipState';
import FriendsList from '../Friendship/FrindsList';
import FriendRequests from '../Friendship/FriendshipRequest/FriendRequests';
import FlaggedPostList from '../FlaggedPostsList/FlaggedPostsList';
import UserPosts from './UserPosts';
import { Container, Row, Col } from 'react-bootstrap';
import MostCommentedAndLikedPosts from '../MostLikedAndCommentedPosts/MostCommentedAndLikedPosts';
import background1 from '../../media/background1.jpg'

const SingleUserView = ({ match }) => {

    const { user } = useContext(AuthContext);


    const pathId = +match.params.id;

    const [inDeleteMode, setInDeleteMode] = useState(false);
    const [singleUser, setSingleUser] = useState({});
    const [inEditMode, setInEditMode] = useState(false);
    const [createForm, setCreateForm] = useState(false);
    const [removeUser, setRemoveUser] = useState(false);
    const [inBanMode, setInBanMode] = useState(false);
    const [showFriendList, setShowFriendList] = useState(false);
    const [showFriendRequest, setShowFriendRequests] = useState(false);
    const [showActivity, setShowActivity] = useState(false);
    const [friendListButtonColor, setFriendListButtonColor] = useState('light');
    const [friendRequestsButtonColor, setFriendRequestsButtonColor] = useState('light');
    const [activityButtonColor, setActivityButtonColor] = useState('light');
    const [showFlaggedList, setShowFlaggedList] = useState(false);
    const [flaggedListButtonColor, setFlaggedListButtonColor] = useState('light');
    const [friendsInfo, setFriendsInfo] = useState([]);
    const [banState, setBanState] = useState(false);
    const [likedPosts, setLikedPosts] = useState([]);
    const [likedCommentsCount, setLikedCommentsCount] = useState({})

    useEffect(() => {
        getUserLikedComments(pathId)
            .then((data) => setLikedCommentsCount(data));
        getUserLikedPosts(pathId)
            .then((data) => setLikedPosts(data))
        getSingleUser(pathId)
            .then((data) => {
                setSingleUser(data);
            })
    }, [pathId]);

    useEffect(() => {
        getFriendshipInfo().then(res => setFriendsInfo(res));
    }, []);


    return (
        <div style={{ backgroundImage: `url(${background1})`, backgroundSize: '100%' }}>
            <Container >
                <Row>
                    <Col> <>
                        {!removeUser
                            ?
                            <div>
                                <div style={{ textAlign: 'left', marginTop: '10px', marginBottom: '10px' }}>
                                    <span style={{ fontWeight: 'bold' }}>
                                        Username:</span> &nbsp;&nbsp;
                                    <span>
                                        {singleUser.username}
                                    </span>
                                </div>
                                <div style={{ textAlign: 'left', marginBottom: '10px' }}>
                                    <span style={{ fontWeight: 'bold' }}>
                                        Registered on:</span>&nbsp;&nbsp;
                                    <span>
                                        {new Date(singleUser.reg_date).toLocaleString()}
                                    </span>
                                </div>
                                <span>
                                    {user?.id !== singleUser.id
                                        ? <div style={{ textAlign: 'left', marginBottom: '10px' }}>
                                            <span style={{ fontWeight: 'bold' }}>
                                                Friendship status:</span>&nbsp;&nbsp;
                                            <FriendshipState acceptId={pathId} inBanMode={inBanMode} />
                                        </div>
                                        : null}
                                    {user?.id === singleUser.id || user?.role === 'admin'
                                        ? <span>

                                        </span>
                                        : null}
                                    <div style={{ marginBottom: '10px', textAlign: 'left' }}>
                                        <span style={{ fontWeight: 'bold' }}>
                                            Statistic:
                                        </span>
                                        <div>
                                            Total posts created: {singleUser.posts_count}
                                        </div>
                                        <div>
                                            Total comments: {singleUser.comments_count}
                                        </div>
                                        <span>
                                            Total likes: {likedPosts.length + likedCommentsCount.count}
                                        </span>
                                    </div>
                                </span>
                                <div style={{ textAlign: 'right' }}>
                                    {user?.id === singleUser.id
                                        ?
                                        <Button variant='light' onClick={() => { setCreateForm(true); setInEditMode(true) }}>
                                            <img style={{
                                                width: '30px',
                                                height: '30px',
                                            }} src={editButtonPic} alt='Delete' />
                                        </Button>
                                        : null}
                                    <span>
                                        {user?.role === 'admin'
                                            ? <Button variant='light' onClick={() => setInDeleteMode(true)}>
                                                <img style={{ width: '30px', height: '30px' }} src={deleteButton} alt='Delete' />
                                            </Button>
                                            : null}
                                        <DeleteModal inDeleteMode={inDeleteMode} userId={singleUser.id} setInDeleteMode={setInDeleteMode} />
                                    </span> &nbsp;
                                    {user?.role === 'admin'
                                        ?
                                        singleUser.isBanned === 0 && !banState
                                            ? <span>
                                                {user?.id !== singleUser.id
                                                    ? <Button variant='light' onClick={() => { setCreateForm(true); setInBanMode(true) }}>
                                                        <img style={{ width: '30px', height: '30px' }} src={banButton} alt='Delete' />
                                                    </Button>
                                                    : null}
                                            </span>

                                            : <Button variant='light' onClick={() => {
                                                unbanUser(singleUser.id)
                                                    .then(user => setSingleUser({ ...user, isBanned: 0 })); setBanState(false)
                                            }} >
                                                Unban
                                            </Button>
                                        : null
                                    }
                                    <hr></hr>
                                </div>
                                {
                                    !inEditMode
                                        ? null
                                        : <EditMode user={singleUser} setUser={setSingleUser} setInEditMode={setInEditMode} />
                                }
                                {!inBanMode
                                    ? null
                                    : <BanMode user={singleUser} setInBanMode={setInBanMode} setUser={setSingleUser} setBanState={setBanState} />}
                            </div>
                            : null}
                        <div>
                            <span>
                                <span>
                                    <Button variant={friendListButtonColor}
                                        onClick={() => {
                                            setShowFriendList(true);
                                            setShowFlaggedList(false);
                                            setFriendListButtonColor('primary');
                                            setShowFriendRequests(false);
                                            setShowActivity(false);
                                            setFriendRequestsButtonColor('light');
                                            setActivityButtonColor('light');
                                            setFlaggedListButtonColor('light');
                                        }}>
                                        Friends list
                                    </Button>&nbsp;
                                </span>
                            </span>
                            <span>
                                <span>
                                    {user?.id === pathId
                                        ?
                                        <Button variant={friendRequestsButtonColor}
                                            style={{ marginRight: '5px' }}
                                            onClick={() => {
                                                setShowFriendRequests(true);
                                                setShowFlaggedList(false);
                                                setShowFriendList(false);
                                                setShowActivity(false)
                                                setFriendRequestsButtonColor('primary');
                                                setActivityButtonColor('light');
                                                setFriendListButtonColor('light');
                                                setFlaggedListButtonColor('light');
                                            }}>
                                            Friends Request
                                        </Button>
                                        : null}
                                </span>
                            </span>
                            {((singleUser.id === user?.id) || friendsInfo.some((el) => (el.users_id === user?.id
                                || el.users_id1 === user?.id)
                                && (el.users_id === singleUser.id
                                    || el.users_id1 === singleUser.id)
                                && el.friendship === 1))
                                || user?.role === 'admin'
                                ? <Button variant={activityButtonColor}
                                    style={{ marginRight: '5px' }}
                                    onClick={() => {
                                        setShowActivity(true);
                                        setShowFlaggedList(false);
                                        setShowFriendList(false);
                                        setShowFriendRequests(false);
                                        setActivityButtonColor('primary');
                                        setFriendRequestsButtonColor('light');
                                        setFriendListButtonColor('light');
                                        setFlaggedListButtonColor('light');
                                    }}>
                                    Activity
                                </Button>
                                : null}
                            {(singleUser.id === user?.id) && user?.role === 'admin'
                                ? <span><Button variant={flaggedListButtonColor}
                                    onClick={() => {
                                        setShowFlaggedList(true);
                                        setShowActivity(false);
                                        setShowFriendList(false);
                                        setShowFriendRequests(false);
                                        setActivityButtonColor('light');
                                        setFriendRequestsButtonColor('light');
                                        setFriendListButtonColor('light');
                                        setFlaggedListButtonColor('primary');
                                    }}>
                                    Flagged posts list
                                </Button>&nbsp;
                                </span>
                                : null}
                        </div>


                        <div>
                            <hr style={{ borderColor: 'black' }}></hr>
                            {showFriendList
                                ? <div><FriendsList id={pathId} />
                                    <Button variant='light'
                                        onClick={() => { setShowFriendList(false); setFriendListButtonColor('light') }}>
                                        Back
                                    </Button>
                                </div>
                                : null}
                            {showFriendRequest
                                ? <div><FriendRequests id={pathId} />
                                    <Button variant='light'
                                        onClick={() => { setShowFriendRequests(false); setFriendRequestsButtonColor('light'); }}>
                                        Back
                                    </Button>
                                </div>
                                : null}
                            {showActivity
                                ? <div><Activity id={pathId} />
                                    <Button variant='light'
                                        onClick={() => { setShowActivity(false); setActivityButtonColor('light') }}>
                                        Back
                                    </Button>
                                </div>
                                : null}
                            {showFlaggedList
                                ? <div>  <FlaggedPostList />
                                    <Button variant='light'
                                        onClick={() => { setShowFlaggedList(false); setFlaggedListButtonColor('light') }}>
                                        Back
                                    </Button>
                                </div>
                                : null}
                        </div>
                        {!showFriendRequest && !showActivity && !showFlaggedList && !showFriendList
                            ? <UserPosts id={singleUser.id} likedPosts={likedPosts} />
                            : null}
                    </></Col>
                    <Col><MostCommentedAndLikedPosts /></Col>
                </Row>

            </Container>
        </div>
    )

}



export default SingleUserView;

