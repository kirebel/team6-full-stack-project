import React, { useContext, useEffect, useState } from 'react';
import { getAllPosts } from '../../requests/PostsRequests';
import AuthContext from '../../providers/AuthContext';
import { Link } from 'react-router-dom';
import { Card } from 'react-bootstrap';
import backGroundImage from '../../media/posts-background.jfif'


const UserPosts = ({ likedPosts, id }) => {

    const { user } = useContext(AuthContext);

    const [posts, setPosts] = useState([]);

    useEffect(() => {
        getAllPosts()
            .then(data => setPosts(data));
    }, []);

    return (
        <>

            {posts.filter((el) => el.users_id === id).length !== 0
                ? <div><div style={{ fontSize: '30px', textAlign: 'center' }}>User posts</div>
                    {posts.sort((a, b) => b.id - a.id)
                        .filter((el) => el.users_id === id)
                        .map(post =>
                            <div key={post.id} style={{ marginBottom: '5px' }}>
                                <Card style={{ width: '600px', backgroundImage: `url(${backGroundImage})` }}>
                                    <Card.Body>
                                        <Card.Text>
                                            <div style={{ textAlign: 'left' }}>
                                                {post.username}
                                            </div>
                                            <Link to={`/singlePost/${post.id}`} className='link' style={{ fontSize: '30px' }}>
                                                {post.title}
                                            </Link>
                                            <div className="date">
                                                Created on: {new Date(post.post_date).toLocaleDateString()}
                                            </div>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                        )
                    }
                </div>
                : null}
            <hr></hr>
            {likedPosts.length !== 0
                ? <div>
                    <div style={{ fontSize: '30px', textAlign: 'center' }}>Liked posts</div>
                    {likedPosts.sort((a, b) => b.id - a.id)
                        .map(post =>
                            <div key={post.id} style={{ marginBottom: '5px' }}>
                                <Card style={{ width: '600px', backgroundImage: `url(${backGroundImage})` }}>
                                    <Card.Body>
                                        <Card.Text>
                                            <div style={{ textAlign: 'left' }}>{post.username}</div>
                                            <Link to={`/singlePost/${post.id}`} className='link' style={{ fontSize: '30px' }}>{post.title}</Link>
                                            <div className="date">Created on: {new Date(post.post_date).toLocaleDateString()}</div>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                        )
                    }
                </div>
                : null}
        </>
    )

}


export default UserPosts;

