import React from 'react';
import { Button } from 'react-bootstrap';
import { updateUser } from '../../requests/UsersRequests';
import { useState } from 'react';

const EditMode = ({ setUser, user, setInEditMode }) => {

    const [username, setUsername] = useState('');
    const [showSubmitButton, setShowSubmitButton] = useState(false);

    const editUser = (id, data) => {
        updateUser(id, data)
            .then((editedUser) => {
                setUser({ ...editedUser, username: username, reg_date: user.reg_date })
            });
    };

    return (
        <div>
            <span>
                <label htmlFor="username-text">New username: &nbsp;</label>
                < input maxLength={200} value={username} onChange={e => {
                    if (e.target.value.length > 8) {
                        setShowSubmitButton(true);
                    }
                    else {
                        setShowSubmitButton(false);
                    }
                    setUsername(e.target.value)
                }}
                    name="username-text"
                /><br />
            </span>
            <div>
                {!showSubmitButton
                    ? <div>The username must be at least 8 characters!</div>
                    : null}
                <Button variant='danger' onClick={() => { setInEditMode(false) }}>
                    Cancel
                </Button>&nbsp;
                {showSubmitButton
                    ? <Button variant='success' onClick={() => {
                        editUser(user.id, { username: username });
                        setInEditMode(false);
                    }}>
                        Submit
                    </Button>
                    : null}
                <hr></hr>
            </div>
        </div>
    )
};


export default EditMode;