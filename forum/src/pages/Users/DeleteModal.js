import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import { deleteUser } from '../../requests/UsersRequests';
import { useHistory } from 'react-router';

const DeleteModal = ({ inDeleteMode, userId, setInDeleteMode }) => {

    const history = useHistory();

    const remove = (id) => {
        deleteUser(id)
            .then()
    }


    return (
        <Modal show={inDeleteMode}>
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                Are you sure about that?
            </div>
            <Modal.Body>
                <div style={{ textAlign: 'center' }}>
                    <Button variant='danger' onClick={() => { setInDeleteMode(false) }}>
                        Cancel
                    </Button>&nbsp;
                    <Button variant='success' onClick={() => {
                        remove(userId);
                        setInDeleteMode(false);
                        history.goBack();
                    }}>
                        Submit
                    </Button>
                </div>
            </Modal.Body>
        </Modal>
    )
}

export default DeleteModal;