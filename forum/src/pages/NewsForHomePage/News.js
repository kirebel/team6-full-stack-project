import React from 'react';
import { Card, Button } from 'react-bootstrap';
import backGround from '../../media/posts-background.jfif';

const News = ({ author, title, content, url }) => {

    return (
        <Card style={{ marginTop: '20px', width: '700px', backgroundImage: `url(${backGround})`, marginLeft: '380px' }} >
            <Card.Body>
                <Card.Title>
                    <div style={{ marginBottom: '10px' }}>
                        Title:&nbsp;  {title}
                    </div>
                </Card.Title>
                <Card.Text>
                    <span>
                        {content.slice(0, 70)}...
                    </span>
                    <span style={{ marginLeft: '20px' }}>
                        <Button onClick={() => window.open(url, '_blank')}>Read more</Button>
                    </span>
                    <div style={{ textAlign: 'right' }}>
                        Author:&nbsp;  {author}
                    </div>
                </Card.Text>
            </Card.Body>
        </Card>
    )
}


export default News;