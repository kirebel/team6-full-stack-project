import { Button } from 'react-bootstrap';
import React, { useState } from 'react';
import { useContext, useEffect } from 'react';
import AuthContext from '../../providers/AuthContext';
import { getFriendshipInfo, friendshipRequest, unFriend } from '../../requests/UsersRequests';

const FriendshipState = ({ acceptId, inBanMode }) => {

    const { user } = useContext(AuthContext);
    const id = acceptId;
    const [friendsInfo, setFriendsInfo] = useState([]);
    const [sentRequest, setSentRequest] = useState(false);
    const [unfriend, setUnfriend] = useState(false);

    useEffect(() => {
        getFriendshipInfo()
            .then((res) => setFriendsInfo([...res, { users_id: 0, users_id1: 0 }]));
    }, []);

    return (
        !friendsInfo.some((el) => (el.users_id === acceptId && el.users_id1 === user?.id) && el.friendship !== 1)
            ?
            !inBanMode
                ? <span>
                    {friendsInfo.some((el) => (el.users_id === id || el.users_id === user?.id)
                        && (el.users_id1 === id || el.users_id1 === user?.id)
                        && el.friendship === 1)
                        ? unfriend
                            ? <span>You are not friend with this user now!</span>
                            : <Button variant='light' onClick={() => { unFriend(acceptId); setUnfriend(true); }}>
                                Unfriend</Button>
                        : friendsInfo.some((el) => el.users_id1 === id
                            && el.users_id === user?.id
                            && el.friendship === 0)
                            ? <span>Friend request sent!</span>
                            : friendsInfo.some((el) => el.users_id !== user?.id
                                && el.users_id1 !== id)
                                ? sentRequest
                                    ? <span>Friend request sent!</span>
                                    : <Button variant='light'
                                        onClick={() => { friendshipRequest(acceptId); setSentRequest(true) }}>
                                        Ask for friendship
                                    </Button>
                                : null}
                </span>
                : null
            : <span>This user has sent you a friend request. Check your friend request list!</span>
    )

};





export default FriendshipState;


