import React, { useState } from 'react';
import { acceptFriendship } from '../../../requests/UsersRequests';
import { Button } from 'react-bootstrap';

const AcceptButton = ({ elementId, elementName, state, setState, secondState }) => {

    const [accept, setAccept] = useState(false)

    return (
        <span>
            {!accept && !state && !secondState
                ?
                <Button style={{ marginRight: '10px' }}
                    variant='light' onClick={() => {
                        acceptFriendship(elementId);
                        setAccept(true); setState(true)
                    }}>
                    Accept
                </Button>
                : state
                    ?
                    <div>You are now friends with {elementName}</div>
                    : null
            }
        </span>
    )

};


export default AcceptButton;