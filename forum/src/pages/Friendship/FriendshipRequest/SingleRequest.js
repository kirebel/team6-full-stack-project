import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import AcceptButton from './AcceptButton';
import DeclineButton from './DeclineButton';
import { Link } from 'react-router-dom';

const SingleRequest = ({ elementId, elementName }) => {

    const [state, setState] = useState(false);
    const [secondState, setSecondState] = useState(false);

    return (
        <>
            <div style={{ marginBottom: '15px' }}>
                <div>
                    <span style={{ textAlign: 'center' }} >
                        <span style={{ fontWeight: 'bold' }}>{elementName}</span>
                        <span> wants to be your friend</span>
                    </span>
                    <span> <Link to={`/users/${elementId}`}>
                        <Button style={{ marginLeft: '10px' }} variant='light'>
                            View Profile
                        </Button>
                    </Link>
                    </span>
                    <div style={{ marginTop: '20px' }}>
                        <AcceptButton elementId={elementId} elementName={elementName}
                            state={state} setState={setState} secondState={secondState} />
                        <DeclineButton elementId={elementId} state={state}
                            setState={setState} setSecondState={setSecondState} />
                    </div>
                </div>
                <hr></hr>
            </div>

        </>
    )

};


export default SingleRequest;