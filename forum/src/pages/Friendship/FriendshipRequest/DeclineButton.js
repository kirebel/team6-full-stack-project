import React, { useState } from 'react'
import { declineFriendship } from '../../../requests/UsersRequests';
import { Button } from 'react-bootstrap';

const DeclineButton = ({ elementId, state, setState, setSecondState }) => {

    const [decline, setDecline] = useState(false);

    return (
        <span>
            {!decline && !state
                ? <Button variant='light'
                    onClick={() => { declineFriendship(elementId); setDecline(true); setState(false); setSecondState(true) }}>
                    Decline
                </Button>
                : !state
                    ? <div>Friendship request declined</div>
                    : null
            }
        </span>

    )

};

export default DeclineButton;