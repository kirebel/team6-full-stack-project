import React, { useEffect, useState } from 'react';
import { getFriendshipInfo } from '../../../requests/UsersRequests';
import SingleRequest from './SingleRequest';


const FriendRequests = ({ id }) => {

    const [friendsInfo, setFriendsInfo] = useState([]);

    useEffect(() => {
        getFriendshipInfo()
            .then((res) => setFriendsInfo(res));
    }, []);

    return (
        <>
            <span>
                {friendsInfo.map((element) =>
                    element.friendship === 0 && element.users_id1 === id
                        ?
                        <SingleRequest key={element.id} elementId={element.users_id}
                            elementName={element.first_username} />
                        : null
                )}
            </span>
        </>
    )
}


export default FriendRequests;


