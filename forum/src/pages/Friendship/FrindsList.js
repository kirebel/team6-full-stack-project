import React, { useEffect, useState } from 'react';
import { getFriendshipInfo } from '../../requests/UsersRequests';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


const FriendsList = ({ id }) => {


    const [friendsInfo, setFriendsInfo] = useState([]);
    const [unfriend, setUnfriend] = useState(false);

    useEffect(() => {
        getFriendshipInfo()
            .then((res) => setFriendsInfo(res));
    }, []);


    return (
        <>
            {friendsInfo.map((element) =>
                element.friendship === 1 && (element.isDeleted1 === 0 && element.isDeleted2 === 0) && (element.users_id === id || element.users_id1 === id)
                    ? id === element.users_id
                        ?
                        <div key={element.id} style={{ marginBottom: '10px' }}>
                            <div >
                                {element.second_username}&nbsp;&nbsp;&nbsp;
                                {!unfriend
                                    ?
                                    <Link to={`/users/${element.users_id1}`}>
                                        <Button variant='light'
                                        >
                                            View Profile
                                        </Button>
                                    </Link>
                                    : null
                                }
                            </div>
                            <hr></hr>
                        </div>
                        : <div key={element.id} style={{ marginBottom: '10px' }}>
                            <div>{element.first_username}&nbsp;&nbsp;&nbsp;
                                {!unfriend
                                    ?
                                    <Link to={`/users/${element.users_id}`}>
                                        <Button variant='light'
                                        >
                                            View Profile
                                        </Button>
                                    </Link>
                                    : null
                                }</div>
                            <hr></hr>
                        </div>

                    : null)}
        </>
    )
};


// && element.users_id === friendsInfo.find((el) => el.first_username === element.first_username).users_id

export default FriendsList;

