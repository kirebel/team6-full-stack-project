
import { useContext, useEffect, useState } from 'react';
import { getAllPosts } from "../../requests/PostsRequests";
import { Link } from "react-router-dom";
import './All-posts.css';
import { Button } from 'react-bootstrap';
import CreatePosts from '../Create-posts/Create-posts';
import { Container, Row, Col } from 'react-bootstrap';
import MostCommentedAndLikedPosts from '../MostLikedAndCommentedPosts/MostCommentedAndLikedPosts';
import { Card } from 'react-bootstrap';
import backGround from '../../media/background1.jpg'
import postsBackGround from '../../media/posts-background.jfif'
import AuthContext from '../../providers/AuthContext';
import Search from '../SearchEngine/Search';
import { searchPostsFromTheDatabase } from '../../requests/PostsRequests';
import { searchUsersFromTheDataBase } from '../../requests/UsersRequests';
import SearchResults from '../SearchEngine/SearchResults';

const AllPosts = () => {

  const [posts, setPosts] = useState([]);
  const [createPost, setCreatePost] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const [search, setSearch] = useState(false);
  const [searchUsers, setSearchUsers] = useState([]);
  const [searchPosts, setSearchPosts] = useState([]);


  const { user } = useContext(AuthContext);

  useEffect(() => {
    getAllPosts()
      .then(data => setPosts(data))
  }, []);

  useEffect(() => {
    searchPostsFromTheDatabase(searchTerm)
      .then((data) => setSearchPosts(data))
  }, [searchTerm])

  useEffect(() => {
    searchUsersFromTheDataBase(searchTerm)
      .then((data) => setSearchUsers(data))
  }, [searchTerm])

  return (
    <div style={{ backgroundImage: `url(${backGround})` }}>
      <Container >
        <Row>
          <Col>
            {!search
              ? <div className="postsView">
                <div style={{ textAlign: 'right' }}>
                  <Search search={search} setSearch={setSearch}
                    searchTerm={searchTerm} setSearchTerm={setSearchTerm}
                    searchUsers={searchUsers} setSearchUsers={setSearchUsers}
                    searchPosts={searchPosts} setSearchPosts={setSearchPosts} />
                </div>
                {createPost || user?.isBanned !== 0
                  ? <CreatePosts setCreatePost={setCreatePost} create={createPost} setPosts={setPosts} posts={posts} />
                  : <div style={{ textAlign: 'center' }}>
                    <Button style={{ marginTop: '20px', marginBottom: '20px' }} variant='light' onClick={() => setCreatePost(true)}>
                      Create Post
                    </Button>
                    <hr></hr>
                  </div>}
                {
                  posts.sort((a, b) => b.id - a.id).map(post =>
                    <div key={post.id}>
                      <Card style={{ width: '700px', marginBottom: '10px', marginTop: '15px', backgroundImage: `url(${postsBackGround})` }}>
                        <Card.Body>
                          <Card.Text>
                            <div >{post.username}</div>
                            <Link to={`/singlePost/${post.id}`} className='link' style={{ fontSize: '30px' }}>{post.title}</Link>
                            <div className="date">Created on: {new Date(post.post_date).toLocaleDateString()}</div>
                          </Card.Text>
                        </Card.Body>
                      </Card>
                    </div>)
                }

              </div>
              :
              <>
                <SearchResults setSearchPosts={setSearchPosts} searchPosts={searchPosts} setSearchUsers={setSearchUsers}
                  searchUsers={searchUsers} searchTerm={searchTerm} />
                <Button variant='light'
                  style={{ marginTop: '15px' }}
                  onClick={() => setSearch(false)}>Back</Button>
              </>}
          </Col>
          <Col><MostCommentedAndLikedPosts /></Col>
        </Row>
        <Row></Row>
      </Container >
    </div>
  )

}


{/* <div>
<div style={{ fontWeight: 'bold', fontSize: '34px' }}>Posts Found</div><hr></hr>
{searchPosts.length !== 0
  ? searchPosts.map(post =>
    <div key={post.id} className="singlePostView">
      <div className='username' style={{ textAlign: 'left' }}>{post.username}</div>
      <Link to={`/singlePost/${post.id}`} className='link' style={{ fontSize: '30px' }}>{post.title}</Link>
      <div className="date">Created on: {new Date(post.post_date).toLocaleDateString()}</div>
    </div>)
  : <div> <div>No posts found!</div>
    <hr></hr></div>}
<div style={{ fontWeight: 'bold', fontSize: '34px' }}>Users Found</div><hr></hr>
{searchUsers.length !== 0
  ? searchUsers.map(u =>
    <Card key={u.id} style={{ width: '400px', textAlign: 'center' }} className='card'>
      <Card.Body>
        <Card.Title>
          <Link to={`/users/${u.id}`} className='link' style={{ fontSize: '15px' }}>
            {u.username}
          </Link>
          <span style={{ marginLeft: '70px', textAlign: 'right' }}>
            Registered on: {new Date(u.reg_date).toLocaleDateString()}
          </span>
        </Card.Title>
      </Card.Body>
    </Card>
  )
  : <div>No users found!</div>}
</div> */}



// {
//   posts.sort((a, b) => b.id - a.id).map(post =>
//     <div key={post.id} className="singlePostView">
//       <Card style={{ width: '800px', backgroundImage: `url(${backGroundImage})` }}>
//         <Card.Body>
//           <Card.Text>
//             <div className='username'>{post.username}</div>
//             <Link to={`/singlePost/${post.id}`} className='link' style={{ fontSize: '30px' }}>{post.title}</Link>
//             <div className="date">Created on: {new Date(post.post_date).toLocaleDateString()}</div>
//           </Card.Text>
//         </Card.Body>
//       </Card>
//     </div>)
// }


export default AllPosts;


// <div className="postsView">
// {createPost
//   ? <CreatePosts setCreatePost={setCreatePost} create={createPost} setPosts={setPosts} posts={posts} />
//   : <div style={{ textAlign: 'center' }}>
//     <Button variant='light' onClick={() => setCreatePost(true)}>
//       Create Post
//     </Button>
//   </div>}
// {posts.sort((a, b) => b.id - a.id).map(post =>
//   <div key={post.id} className="singlePostView">
//     <div className='username'>{post.username}</div>
//     <Link to={`/singlePost/${post.id}`} className='link' style={{ fontSize: '30px' }}>{post.title}</Link>
//     <div className="date">Created on: {new Date(post.post_date).toLocaleDateString()}</div>
//   </div>)}
// </div>
// )