import React, { useEffect, useState } from 'react'
import { getMostCommentedPosts, getMostLikedPosts } from '../../requests/PostsRequests';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import backGroundImage from '../../media/posts-background.jfif'

const MostCommentedAndLikedPosts = () => {

    const [mostLiked, setMostLiked] = useState([]);
    const [mostCommented, setMostCommented] = useState([]);

    useEffect(() => {
        getMostCommentedPosts()
            .then(data => setMostCommented(data));
        getMostLikedPosts()
            .then(data => setMostLiked(data));
    }, []);

    return (
        <>
            <Card style={{ marginTop: '20px', width: '350px', backgroundImage: `url(${backGroundImage})` }} >
                <Card.Body>
                    <Card.Title style={{ fontSize: '30px' }}>
                        Most liked posts
                    </Card.Title>
                    <hr></hr>
                    <Card.Text>
                        {mostLiked.map((el) =>
                            <div key={el.id} >
                                <div style={{ textAlign: 'left' }}>{el.username}</div>
                                <Link to={`/singlePost/${el.id}`} className='link' style={{ fontSize: '30px' }}>{el.title}</Link>
                                <div>Created on: {new Date(el.post_date).toLocaleDateString()}
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    {el.number_likes} likes
                                </div>
                                <hr></hr>
                            </div>)}
                    </Card.Text>
                </Card.Body>
            </Card>
            <Card style={{ marginTop: '20px', width: '350px', backgroundImage: `url(${backGroundImage})` }} >
                <Card.Body>
                    <Card.Title style={{ fontSize: '30px' }}>
                        Most commented posts
                    </Card.Title>
                    <hr></hr>
                    <Card.Text>
                        {mostCommented.map((el) =>
                            <div key={el.id} >
                                <div style={{ textAlign: 'left' }}>{el.username}</div>
                                <Link to={`/singlePost/${el.id}`} className='link' style={{ fontSize: '30px' }}>{el.title}</Link>
                                <div>Created on: {new Date(el.post_date).toLocaleDateString()}
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    {el.number_comments} comments
                                </div>
                                <hr></hr>
                            </div>)}
                    </Card.Text>
                </Card.Body>
            </Card>
        </>
    )

};






export default MostCommentedAndLikedPosts;