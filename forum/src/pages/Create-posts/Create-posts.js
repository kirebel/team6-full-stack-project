import { useState } from "react";
import { createPost } from "../../requests/PostsRequests";
import { Modal, Button } from "react-bootstrap";

const CreatePosts = ({ setCreatePost, create, setPosts, posts }) => {

    const [minTitleLength, setMinTitleLength] = useState(false);
    const [minTextLength, setMinTextLength] = useState(false);


    const [post, setPost] = useState({
        title: '',
        text: ''
    });

    const updatePosts = (prop, value) => {
        post[prop] = value;
        setPost({ ...post })
    };

    const createNewPost = () => {
        createPost(post).then(data => setPosts([...posts, data]));
        setCreatePost(false);

    }

    return (
        <Modal show={create}>
            <Modal.Body>
                <div style={{ textAlign: 'center' }}>
                    <h1>Description</h1>
                    <div>
                        <label htmlFor="post-title">Title</label>
                    </div>
                    <textarea maxLength={50} style={{ height: '70px', width: '450px' }} onChange={e => {
                        if (e.target.value.length <= 10) {
                            setMinTitleLength(false);
                        } else {
                            setMinTitleLength(true);
                        }
                        updatePosts('title', e.target.value)
                    }}
                        id="post-title" name="post-title"></textarea><br />
                    {!minTitleLength
                        ? <div style={{ color: 'red' }}>You must write minimum 10 characters!</div>
                        : null}
                    <div>
                        <label htmlFor="post-text">Text</label>
                    </div>
                    <textarea style={{ height: '150px', width: '450px' }} maxLength={500} onChange={e => {
                        if (e.target.value.length <= 15) {
                            setMinTextLength(false)
                        } else {
                            setMinTextLength(true)
                        } updatePosts('text', e.target.value)
                    }} id="post-text" name="post-text"></textarea><br />
                    {!minTextLength
                        ? <div style={{ color: 'red' }}>You must write minimum 15 symbols!</div>
                        : null}
                    <Button variant='danger' onClick={() => { setCreatePost(false) }}>
                        Cancel
                    </Button>&nbsp;
                    {(minTextLength && minTitleLength)
                        ? <Button variant='success' onClick={() => {
                            setCreatePost(true); createNewPost()
                        }}>
                            Submit
                        </Button>
                        : null}
                </div>
            </Modal.Body>
        </Modal>

    )
}



export default CreatePosts;
