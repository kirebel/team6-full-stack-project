import { Button } from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import { getFlaggedPosts } from '../../requests/PostsRequests';
import { Link } from 'react-router-dom';


const FlaggedPostList = () => {


    const [posts, getPosts] = useState([]);

    useEffect(() => {
        getFlaggedPosts()
            .then((data) => getPosts(data));
    }, [])

    return (
        posts.map((element) =>
            <div key={element.id}>
                <span>Post title:  {element.title}</span>
                <span>
                    <Link to={`/singlePost/${element.id}`} className='link' style={{ fontSize: '15px' }}>
                        <Button variant='light' style={{ marginLeft: '10px' }}>
                            View Post
                        </Button>
                    </Link>
                    <hr></hr>
                </span>
            </div>
        )
    )

};

export default FlaggedPostList;