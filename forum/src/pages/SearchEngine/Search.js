import React, { useContext, useState } from 'react';
import searchIcon from '../../media/search-icon.png'
import AuthContext from '../../providers/AuthContext';

const Search = ({ setSearch, setSearchTerm }) => {

    const [input, setInput] = useState(false);

    const { user } = useContext(AuthContext);

    return (

        <div>
            {
                input && user !== null
                    ? <span>
                        <input maxLength={200}
                            onKeyPress={(e) => {
                                if (e.key === 'Enter') {
                                    setInput(false);
                                    setSearch(true);
                                }
                            }}
                            onChange={(e) => {
                                setSearchTerm(e.target.value);
                            }}></input>
                    </span>
                    : null
            }
            {user !== null
                ? <img
                    style={{ height: '30px', width: '30px', marginTop: '10px', marginLeft: '5px' }}
                    onClick={() => setInput(true)}
                    src={searchIcon} alt='Search'
                /> : null}

        </div>
    )
}

export default Search;