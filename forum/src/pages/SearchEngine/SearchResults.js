import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Card } from 'react-bootstrap';
import { searchPostsFromTheDatabase } from '../../requests/PostsRequests';
import { searchUsersFromTheDataBase } from '../../requests/UsersRequests';
import { Container, Col, Row } from 'react-bootstrap';
import resultsBackGroundImage from '../../media/posts-background.jfif'
import backGround from '../../media/background1.jpg'

const SearchResults = ({ setSearchPosts, searchPosts, setSearchUsers, searchUsers, searchTerm }) => {

    useEffect(() => {
        searchPostsFromTheDatabase(searchTerm)
            .then((data) => setSearchPosts(data))
    }, [searchTerm])

    useEffect(() => {
        searchUsersFromTheDataBase(searchTerm)
            .then((data) => setSearchUsers(data))
    }, [searchTerm])


    return (
        <Container style={{ backgroundImage: `url(${backGround})` }}>
            <Row>
                <Col> <div>
                    {[...searchPosts, ...searchUsers].length !== 0
                        ?
                        <div>
                            <div style={{ marginTop: '20px', fontWeight: 'bold', fontSize: '25px' }}>Results</div>
                            <hr></hr>
                            {[...searchPosts, ...searchUsers]
                                .map(element =>
                                    element.title !== undefined
                                        ?
                                        <div key={element.id}>
                                            <Card style={{ marginTop: '20px', width: '700px', backgroundImage: `url(${resultsBackGroundImage})` }} >
                                                <Card.Body>
                                                    <Card.Text>
                                                        <div key={element.id} >
                                                            <div style={{ textAlign: 'left', fontWeight: 'bold', marginBottom: '10px' }}>
                                                                Post
                                                            </div>
                                                            <div style={{ textAlign: 'left' }}>
                                                                {element.username}
                                                            </div>
                                                            <Link to={`/singlePost/${element.id}`}
                                                                className='link' style={{ fontSize: '30px' }}>
                                                                {element.title}
                                                            </Link>
                                                            <div style={{ textAlign: 'right' }}>
                                                                Created on: {new Date(element.post_date).toLocaleDateString()}
                                                            </div>
                                                        </div>
                                                    </Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        : <div key={element.id}>
                                            <Card style={{ marginTop: '20px', width: '700px', backgroundImage: `url(${resultsBackGroundImage})` }} >
                                                <Card.Body>
                                                    <Card.Text>
                                                        <div key={element.id} >
                                                            <div style={{ textAlign: 'left', fontWeight: 'bold', marginBottom: '10px' }}>
                                                                User
                                                            </div>
                                                            <div style={{ textAlign: 'left' }}>
                                                                <Link className='link' to={`/users/${element.id}`}>
                                                                    {element.username}
                                                                </Link>
                                                            </div>
                                                            <div style={{ textAlign: 'right' }}>
                                                                Registered on: {new Date(element.reg_date).toLocaleDateString()}
                                                            </div>
                                                        </div>
                                                    </Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                )}
                        </div>
                        : <div>No results!</div>}
                </div></Col>
            </Row>
        </Container >
    )
}






export default SearchResults;


{/* <div key={element.id} className="singlePostView">
<div className='username' style={{ textAlign: 'left' }}>{element.username}</div>
<Link to={`/singlePost/${element.id}`} className='link' style={{ fontSize: '30px' }}>{element.title}</Link>
<div className="date">Created on: {new Date(element.post_date).toLocaleDateString()}</div>
</div> */}