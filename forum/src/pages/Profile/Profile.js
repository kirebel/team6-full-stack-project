import React, { useContext } from 'react';
import SingleUserView from '../Users/SingleUsersView';
import AuthContext from '../../providers/AuthContext';

const Profile = () => {

    const { user } = useContext(AuthContext);

    const match = { params: { id: user.id } };

    return (
        <SingleUserView match={match} />
    )

};

export default Profile;