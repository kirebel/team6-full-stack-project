import React from 'react';
import { Modal, Button } from 'react-bootstrap';



const BanModal = ({ banModal, setBanModal, banReason }) => {

    return (
        <Modal show={banModal}>
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                {banReason}
            </div>
            <Modal.Body>
                <div style={{ textAlign: 'center' }}>
                    <Button variant='danger' onClick={() => { setBanModal(false) }}>
                        OK
                    </Button>&nbsp;

                </div>
            </Modal.Body>
        </Modal>
    )
};


export default BanModal;