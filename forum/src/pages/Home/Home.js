import { useContext, useEffect, useState } from 'react';
import AuthContext from '../../providers/AuthContext';
import { getLikesCount, getPostsCount } from '../../requests/PostsRequests';
import './Home.css';
import BanModal from './UserBannedModal';

import NewsContext from '../../providers/NewsContext';
import News from '../NewsForHomePage/News';
import backGround from '../../media/background1.jpg'
import { getFriendshipsCount, usersCount } from '../../requests/UsersRequests';
import { getCommentsCount } from '../../requests/CommentsRequests';
import { Button } from 'react-bootstrap';

const HomePage = () => {

  const { user } = useContext(AuthContext);


  const [users1Count, setUsers1Count] = useState([]);
  const [postsCount, setPostsCount] = useState([]);
  const [banModal, setBanModal] = useState(true);
  const [likesCount, setLikesCount] = useState([]);
  const [friendshipsCount, setFriendshipsCount] = useState([]);
  const [commentsCount, setCommentsCount] = useState([]);
  const [showNews, setShowNews] = useState(true);
  const [showAbout, setShowAbout] = useState(false);
  const [showStatistic, setShowStatistic] = useState(false);
  const [showStatisticButtonColor, setShowStatisticButtonColor] = useState('light');
  const [showNewsButtonColor, setShowNewsButtonColor] = useState('primary');
  const [showAboutButtonColor, setShowAboutButtonColor] = useState('light');

  const news = useContext(NewsContext);


  useEffect(() => {
    getPostsCount()
      .then(data => setPostsCount(data));
    getLikesCount()
      .then(data => setLikesCount(data));
    getFriendshipsCount()
      .then(data => setFriendshipsCount(data))
    usersCount()
      .then(data => setUsers1Count(data));
    getCommentsCount()
      .then(data => setCommentsCount(data));
  }, [])

  return (
    <div style={{ backgroundImage: `url(${backGround})`, height: '600px' }}>
      <div style={{ backgroundImage: `url(${backGround})` }} >
        {user?.isBanned === 1
          ?
          banModal
            ? <BanModal banModal={banModal} setBanModal={setBanModal} banReason={user.ban_reason} />
            : null
          : null}
        <div >
          <div>

            <div style={{ fontWeight: 'bold', fontSize: '50px', marginBottom: '35px' }}>
              Welcome to our community
            </div>
            <Button variant={showNewsButtonColor}
              onClick={() => {
                setShowNews(true); setShowStatistic(false); setShowAbout(false);
                setShowNewsButtonColor('primary');
                setShowStatisticButtonColor('light');
                setShowAboutButtonColor('light');
              }}>
              JavaScript articles
            </Button>
            <Button variant={showStatisticButtonColor}
              onClick={() => {
                setShowStatistic(true); setShowNews(false); setShowAbout(false);
                setShowNewsButtonColor('light');
                setShowStatisticButtonColor('primary');
                setShowAboutButtonColor('light');
              }} style={{ marginLeft: '20px' }}>
              Statistic
            </Button>
            <Button variant={showAboutButtonColor} onClick={() => {
              setShowAbout(true); setShowNews(false);
              setShowStatistic(false); setShowNewsButtonColor('light');
              setShowStatisticButtonColor('light');
              setShowAboutButtonColor('primary');
            }} style={{ marginLeft: '20px' }}>About</Button>
            <hr></hr>
            {showStatistic ?
              <div style={{ height: '520px' }}>
                <div style={{ fontSize: '30px' }}>
                  Users registered:  &nbsp; {users1Count.c}
                </div>
                <div style={{ fontSize: '30px' }}>
                  Posts published:  &nbsp; {postsCount.c}
                </div>
                <div style={{ fontSize: '30px' }}>
                  Replies made:  &nbsp; {commentsCount.c}
                </div>
                <div style={{ fontSize: '30px' }}>
                  Likes set:  &nbsp; {likesCount.c}
                </div>
                <div style={{ fontSize: '30px' }}>
                  Friendships made:  &nbsp; {friendshipsCount.c}
                </div>
              </div>
              : null}
            <div>
              {showNews ?
                news.articles
                  .sort((a, b) => new Date(b.publishedAt) - new Date(a.publishedAt))
                  .map((element, index) => <News key={index} author={element.author}
                    title={element.title} content={element.content} url={element.url} />)
                : null}
              {showAbout
                ? <div style={{ height: '520px' }}>
                  <div style={{ fontSize: '25px', fontWeight: 'bold', marginBottom: '10px' }}>
                    This forum is for people who want to learn something new about programming or
                    need help for their projects and ect. Any form of spam or verbal abuse is not tolerated.
                  </div>
                  <hr></hr>
                  <div style={{ textAlign: 'center', fontSize: '40px', marginBottom: '15px' }}>
                    Maintainers:
                  </div>
                  <div style={{ fontSize: '30px' }}>Kiril Belchinski</div>
                  <div style={{ marginLeft: '10px', marginBottom: '20px' }}>
                    Contact:  kiril.belchinski.a29@learn.telerikacademy.com
                  </div>
                  <div style={{ fontSize: '30px' }}>Milen Petkov</div>
                  <div style={{ marginLeft: '10px' }}>
                    Contact:  milen.petkov.a29@learn.telerikacademy.com
                  </div>
                </div>
                : null}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}




export default HomePage;
