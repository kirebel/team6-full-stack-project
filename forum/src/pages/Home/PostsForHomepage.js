import './PostsForHomepage.css';

const PostsForHomepage = ({ post }) => {
    return (
        <div className="allPosts">
            <div className="post">
                {post.title}
            </div>
        </div>

    )
}

export default PostsForHomepage;