import React, { useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { createUser, loginUser } from '../../requests/UsersRequests';
import { getUserFromToken } from '../../utils/Token';
import { useHistory } from 'react-router';
import AuthContext from '../../providers/AuthContext';

const RegistrationForm = ({ reg }) => {

    const history = useHistory();

    const { setUser } = useContext(AuthContext);

    const [registration, setRegistration] = useState({
        username: '',
        password: '',
    })

    const [duplicateMessage, setDuplicateMessage] = useState(false);
    const [successMessage, setSuccessMessage] = useState(false);
    const [minUsernameLength, setMinUserNameLength] = useState(true);
    const [minPasswordLength, setMinPasswordNameLength] = useState(true);

    const login = (e, data) => {
        e.preventDefault();
        loginUser(data)
            .then((res) => res.json())
            .then(response => {
                setUser(getUserFromToken(response.token));
                localStorage.setItem('token', response.token);
            });
        history.push('/home');
    }

    const create = (prop, value) => {
        registration[prop] = value;
        setRegistration({ ...registration })
    };

    const registerUser = (data) => {
        createUser(data)
            .then((response) => {
                if (response.status === 409) {
                    setDuplicateMessage(true);
                    return null;
                }
                setDuplicateMessage(false);
                setSuccessMessage(true);
                response.json();
            })
            ;


    }

    return (
        <Form style={{ height: '300px', width: '400px', margin: '0 auto' }}>
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label style={{ fontSize: '30px', color: 'black' }}>Username</Form.Label>
                <Form.Control maxLength={15} onClick={() => setDuplicateMessage(false)} onChange={(e) => {
                    if (e.target.value.length < 5) {
                        setMinUserNameLength(true);
                    }
                    else {
                        setMinUserNameLength(false);
                    }
                    create('username', e.target.value)
                }}
                    type="username" placeholder="The username must be maximum 15 symbols" />
            </Form.Group>
            {duplicateMessage
                ? <div style={{ color: 'red' }}>Username already exist!</div>
                : null}
            <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label style={{ fontSize: '30px', color: 'black' }}>Password</Form.Label>
                <Form.Control maxLength={20} onClick={() => setDuplicateMessage(false)}
                    onChange={(e) => {
                        if (e.target.value.length < 5) {
                            setMinPasswordNameLength(true);
                        }
                        else {
                            setMinPasswordNameLength(false);
                        }
                        create('password', e.target.value)
                    }}
                    type="password" placeholder="Password" />
                <div>Username and password must be at least 5 symbols!</div>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicCheckbox">
            </Form.Group>
            {successMessage
                ? <div style={{ textAlign: 'center' }}>
                    <div style={{ color: 'green' }}>You have registered successfully!</div>
                    <Button variant="primary" onClick={(e) => { login(e, registration) }}
                        style={{ marginRight: '20px' }} type="submit">
                        Login
                    </Button>
                    <Button variant='primary' onClick={() => { reg(false) }} >Back</Button>
                </div>
                : <div>
                    <Button variant='primary' style={{ marginLeft: '120px', marginRight: '30px' }} onClick={() => { reg(false) }} >Cancel</Button>
                    {!(minUsernameLength || minPasswordLength)
                        ? <Button
                            variant='primary' onClick={() => { registerUser(registration) }}>
                            Confirm</Button>
                        : null}

                </div>}
        </Form>
    )
};

export default RegistrationForm;