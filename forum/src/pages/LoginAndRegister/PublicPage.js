import React, { useContext, useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import background1 from '../../media/background1.jpg'
import { useHistory } from 'react-router-dom';
import Register from './Register'
import { loginUser } from '../../requests/UsersRequests';
import AuthContext from '../../providers/AuthContext';
import { getUserFromToken } from '../../utils/Token';


const PublicPage = () => {

    const { setUser } = useContext(AuthContext);

    const image = {
        backgroundImage: `url(${background1})`,
    }

    const [errorMessage, setErrorMessage] = useState(false);

    const [loginInfo, setLoginInfo] = useState({
        username: '',
        password: '',
    })

    const collectLoginData = (prop, value) => {
        loginInfo[prop] = value;
        setLoginInfo({ ...loginInfo })
    };

    const history = useHistory();

    const login = (e, data) => {
        e.preventDefault();
        loginUser(data)
            .then((response) => {
                if (response.status === 400) {
                    setErrorMessage(true)
                    return null;
                }
                response.json()
                    .then((res) => {
                        setUser(getUserFromToken(res.token));
                        localStorage.setItem('token', res.token);
                    })
                history.push('/home');
            }
            )
    }

    return (
        <div style={image} >
            <div style={{ height: '50px' }}> </div>
            <Form style={{ height: '720px', width: '400px', margin: '0 auto' }}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label style={{ color: 'black', fontSize: '30px' }}>Username</Form.Label>
                    <Form.Control maxLength={20} onClick={() => setErrorMessage(false)}
                        type="username" placeholder="Enter username"
                        onChange={(e) => {
                            collectLoginData('username', e.target.value)
                        }} />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label style={{ color: 'black', fontSize: '30px' }}>Password</Form.Label>
                    <Form.Control maxLength={20} onClick={() => setErrorMessage(false)}
                        onChange={(e) => { collectLoginData('password', e.target.value) }}
                        type="password" placeholder="Password" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicCheckbox">
                </Form.Group>
                {errorMessage
                    ? <div style={{ color: 'black', marginBottom: '10px', fontSize: '20px' }}>
                        Invalid username or password!
                    </div>
                    : null}
                <Button variant="light" onClick={(e) => { login(e, loginInfo); }}
                    style={{ marginRight: '20px' }} type="submit">
                    Login
                </Button>
                <Register>
                </Register>
            </Form >
        </div>
    )
}


export default PublicPage;