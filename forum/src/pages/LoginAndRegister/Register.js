import React, { useState } from 'react';
import { Modal, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { render } from '@testing-library/react';
import RegistrationForm from './RegistrationForm';

const Register = () => {

    const [showRegistration, setShowRegistration] = useState(false);

    return (
        <span>
            <Button variant='light' onClick={() => setShowRegistration(true)}>Registration</Button>
            <Modal show={showRegistration}>
                <Modal.Body>
                    <RegistrationForm reg={setShowRegistration} />
                </Modal.Body>
            </Modal>
        </span>
    );
}

render(Register)

export default Register;