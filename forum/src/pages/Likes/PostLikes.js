import React, { useContext, useState } from 'react';
import { Button } from 'react-bootstrap';
import { deletePostLike, getPostLikes, setPostLike } from '../../requests/PostsRequests';
import AuthContext from '../../providers/AuthContext';
import { Link } from 'react-router-dom';
import { Card } from 'react-bootstrap';

const PostLikes = ({ postId, numberLikes, likes, setLikes, lockPost }) => {

    const { user } = useContext(AuthContext);

    const [isLiked, setIsLiked] = useState(false);
    const [additionalInfo, setAdditionalInfo] = useState(false);



    const likePost = (id) => {
        setPostLike(id)
            .then((data) => { setLikes(data); setIsLiked(true) });
    };

    const disLikePost = (id) => {
        deletePostLike(id)
            .then((data) => setLikes(data));
    }

    const render = (id) => {
        getPostLikes(id)
            .then(res => setLikes(res));
    }

    return (
        <>
            <div style={{ textAlign: 'left' }}>
                {lockPost === 0 && user?.isBanned === 0 ?
                    !isLiked && !likes.some((el) => el.users_id === user?.id)
                        ? <span style={{ textAlign: 'left' }}>
                            {user?.isBanned === 0
                                ? <Button variant='light'
                                    style={{ marginRight: '5px' }}
                                    onClick=
                                    {() => { likePost(postId); setIsLiked(true); setAdditionalInfo(false) }}

                                >Like</Button>
                                : null}
                        </span>
                        : <span style={{ textAlign: 'left' }} >
                            <Button
                                onClick={
                                    () => { disLikePost(postId); setIsLiked(false); setAdditionalInfo(false) }
                                }>Dislike</Button>
                        </span >
                    : null
                }
                {numberLikes !== 0
                    ? <span style={{ textAlign: 'left', marginLeft: '10px', cursor: 'pointer' }} >{numberLikes}&nbsp;
                        <span onClick={() => { setAdditionalInfo(true); render(postId) }} style={{ color: 'blue' }}>
                            people
                        </span> like this post</span>
                    :
                    lockPost === 0
                        ? <span>Be the first to like</span>
                        : null}
                {additionalInfo
                    ? <Card style={{ width: '400px', textAlign: 'right', margin: 0, marginTop: '10px' }} >
                        <Card.Text >
                            <Button variant='light' onClick={() => setAdditionalInfo(false)}>X</Button>
                            <div > {likes.map((el) => <span key={el.id}>
                                <div style={{ marginBottom: '10px', textAlign: 'left' }}> &nbsp;{el.username}
                                    <Link to={`/users/${el.users_id}`}>
                                        <Button variant='light'>ViewProfile</Button>
                                    </Link>
                                </div></span>)
                            }
                            </div>
                        </Card.Text>
                    </Card>
                    : null}
            </div>
        </>
    )
}

export default PostLikes;