import React, { useState } from 'react';
import { useContext } from 'react';
import AuthContext from '../../providers/AuthContext';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { setCommentLike, deleteCommentLike, getCommentLikes } from '../../requests/CommentsRequests';


const CommentLikes = ({ likes, setLikes, id, numberLikes, lockPost, postId }) => {
    const { user } = useContext(AuthContext);

    const [isLiked, setIsLiked] = useState(false);
    const [additionalInfo, setAdditionalInfo] = useState(false);

    const likeComment = (id, postId) => {
        setCommentLike(id, postId)
            .then((data) => { setLikes(data); setIsLiked(true) });
    };

    const disLikeComment = (id, postId) => {
        deleteCommentLike(id, postId)
            .then((data) => setLikes(data));
    }

    const render = (id) => {
        getCommentLikes(id)
            .then(res => setLikes(res));
    }

    return (
        <>

            <div style={{ textAlign: 'left' }}>
                {lockPost === 0 ?
                    !isLiked && !likes.some((el) => el.users_id === user?.id)
                        ? <span style={{ textAlign: 'left' }}>
                            {user?.isBanned === 0
                                ? <Button variant='light'
                                    style={{ marginRight: '5px' }}
                                    onClick=
                                    {() => { likeComment(id, postId); setIsLiked(true); setAdditionalInfo(false) }}

                                >Like</Button>
                                : null}
                        </span>
                        : <span style={{ textAlign: 'left' }} >
                            {user?.isBanned === 0
                                ? <Button
                                    onClick={
                                        () => { disLikeComment(id, postId); setIsLiked(false); setAdditionalInfo(false) }
                                    }>Dislike</Button>
                                : null}
                        </span >
                    : null
                }
                {numberLikes !== 0
                    ?
                    <span style={{ textAlign: 'left', marginLeft: '10px', cursor: 'pointer' }} >{numberLikes}&nbsp;
                        <span onClick={() => { setAdditionalInfo(true); render(id) }} style={{ color: 'blue' }}>
                            people
                        </span> like this comment</span>
                    : lockPost === 0
                        ? <span>Be the first to like</span>
                        : null}
                {additionalInfo
                    ?
                    <Card style={{ width: '400px', textAlign: 'right', margin: 0, marginTop: '10px' }} >
                        <Card.Text >
                            <Button variant='light' onClick={() => setAdditionalInfo(false)}>X</Button>
                            <div > {likes.map((el) => <span key={el.id}>
                                <div style={{ marginBottom: '10px', textAlign: 'left' }}>{el.username}
                                    <Link to={`/users/${el.users_id}`}>
                                        <Button variant='light'>ViewProfile</Button>
                                    </Link>
                                </div></span>)}
                            </div>
                        </Card.Text>
                    </Card>
                    : null}
            </div>
        </>
    )
};



export default CommentLikes;