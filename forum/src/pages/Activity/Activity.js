import { Button } from 'react-bootstrap';
import React, { useEffect, useState } from 'react';
import { getCommentsActivity, getPostsActivity, getUsersActivity } from '../../requests/UsersRequests';
import { useHistory } from 'react-router';

const Activity = ({ id }) => {

    const history = useHistory();

    const [commentsActivity, setCommentsActivity] = useState([]);
    const [postsActivity, setPostsActivity] = useState([]);
    const [usersActivity, setUsersActivity] = useState([])

    useEffect(() => {
        getCommentsActivity(id)
            .then((res) => setCommentsActivity(res));
    }, [id])

    useEffect(() => {
        getPostsActivity(id)
            .then((res) => setPostsActivity(res));
    }, [id])

    useEffect(() => {
        getUsersActivity(id)
            .then((res) => setUsersActivity(res));
    }, [id]);

    return (
        [...commentsActivity, ...postsActivity, ...usersActivity]
            .sort((a, b) => new Date(b.date) - new Date(a.date))
            .map((element) => {
                if (element.comments_id === null && element.description === 'GET') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>Post read</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            Post title : &nbsp;
                            {element.title}&nbsp;&nbsp;&nbsp;
                            {element.isDeleted === 0
                                ?
                                <Button variant='light' onClick={() => history.push(`/singlePost/${element.posts_id}`)}>
                                    View Post
                                </Button>
                                : <span style={{ color: 'red' }}>This post has been deleted!</span>}
                        </div>
                        <span>Visited on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                }
                if (element.comments_id === null && element.description === 'put') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>Post edit</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            Post title : &nbsp;
                            {element.title}&nbsp;&nbsp;&nbsp;
                            {element.isDeleted === 0
                                ? <Button variant='light' onClick={() => history.push(`/singlePost/${element.posts_id}`)}>
                                    View Post
                                </Button>
                                : <span style={{ color: 'red' }}>This post has been deleted!</span>}
                        </div>
                        <span>Edited on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                }
                if (element.comments_id === null && element.description === 'post') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>Post creation</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            Post title : &nbsp;
                            {element.title}&nbsp;&nbsp;&nbsp;
                            {element.isDeleted === 0
                                ? <Button variant='light' onClick={() => history.push(`/singlePost/${element.posts_id}`)}>
                                    View Post
                                </Button>
                                : <span style={{ color: 'red' }}>This post has been deleted!</span>}
                        </div>
                        <span>Created on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                }
                if (element.comments_id === null && element.description === 'del') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>Post deletion</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            Post title : &nbsp;
                            {element.title}&nbsp;&nbsp;&nbsp;
                        </div>
                        <span>Created on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                } if (element.comments_id !== null && element.description === 'put') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>Comment edit</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            At post with title : &nbsp;
                            {element.title}&nbsp;&nbsp;&nbsp;
                            {element.isDeleted === 0
                                ? <Button variant='light' onClick={() => history.push(`/singlePost/${element.posts_id}`)}>
                                    View Post
                                </Button>
                                : <span style={{ color: 'red' }}>This post has been deleted!</span>}
                        </div>
                        <span>Edited on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                }
                if (element.comments_id !== null && element.description === 'del') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>Comment deletion</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            At post with title : &nbsp;
                            {element.title}&nbsp;&nbsp;&nbsp;
                            {element.isDeleted === 0
                                ? <Button variant='light' onClick={() => history.push(`/singlePost/${element.posts_id}`)}>
                                    View Post
                                </Button>
                                : <span style={{ color: 'red' }}>This post has been deleted!</span>}
                        </div>
                        <span>Deleted on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                }
                if (element.comments_id !== null && element.description === 'post') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>Comment creation</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            At post with title : &nbsp;
                            {element.title}&nbsp;&nbsp;&nbsp;
                            {element.isDeleted === 0
                                ? <Button variant='light' onClick={() => history.push(`/singlePost/${element.posts_id}`)}>
                                    View Post
                                </Button>
                                : <span style={{ color: 'red' }}>This post has been deleted!</span>}
                        </div>
                        <span>Created on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                }
                if (element.comments_id === null && element.description === 'flag') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>Post flag</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            Post title : &nbsp;
                            {element.title}&nbsp;&nbsp;&nbsp;
                            {element.isDeleted === 0
                                ?
                                <Button variant='light' onClick={() => history.push(`/singlePost/${element.posts_id}`)}>
                                    View Post
                                </Button>
                                : <span style={{ color: 'red' }}>This post has been deleted!</span>}
                        </div>
                        <span>Flagged on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                }
                if (element.comments_id === null && element.description === 'unflag') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>Post unflag</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            Post title : &nbsp;
                            {element.title}&nbsp;&nbsp;&nbsp;
                            {element.isDeleted === 0
                                ?
                                <Button variant='light' onClick={() => history.push(`/singlePost/${element.posts_id}`)}>
                                    View Post
                                </Button>
                                : <span style={{ color: 'red' }}>This post has been deleted!</span>}
                        </div>
                        <span>Unflagged on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                }
                if (element.comments_id === null && element.description === 'lock') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>Post lock</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            Post title : &nbsp;
                            {element.title}&nbsp;&nbsp;&nbsp;
                            {element.isDeleted === 0
                                ?
                                <Button variant='light' onClick={() => history.push(`/singlePost/${element.posts_id}`)}>
                                    View Post
                                </Button>
                                : <span style={{ color: 'red' }}>This post has been deleted!</span>}
                        </div>
                        <span>Locked on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                }
                if (element.comments_id === null && element.description === 'unlock') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>Post unlock</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            Post title : &nbsp;
                            {element.title}&nbsp;&nbsp;&nbsp;
                            {element.isDeleted === 0
                                ?
                                <Button variant='light' onClick={() => history.push(`/singlePost/${element.posts_id}`)}>
                                    View Post
                                </Button>
                                : <span style={{ color: 'red' }}>This post has been deleted!</span>}
                        </div>
                        <span>Unlocked on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                }
                if (element.comments_id === null && element.description === 'like') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>Post like</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            Post title : &nbsp;
                            {element.title}&nbsp;&nbsp;&nbsp;
                            {element.isDeleted === 0
                                ?
                                <Button variant='light' onClick={() => history.push(`/singlePost/${element.posts_id}`)}>
                                    View Post
                                </Button>
                                : <span style={{ color: 'red' }}>This post has been deleted!</span>}
                        </div>
                        <span>Liked on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                }
                if (element.comments_id === null && element.description === 'dislike') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>Post dislike</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            Post title : &nbsp;
                            {element.title}&nbsp;&nbsp;&nbsp;
                            {element.isDeleted === 0
                                ?
                                <Button variant='light' onClick={() => history.push(`/singlePost/${element.posts_id}`)}>
                                    View Post
                                </Button>
                                : <span style={{ color: 'red' }}>This post has been deleted!</span>}
                        </div>
                        <span>Disliked on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                }
                if (element.comments_id !== null && element.description === 'dislike') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>Comment dislike</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            At post with title : &nbsp;
                            {element.title}&nbsp;&nbsp;&nbsp;
                            {element.isDeleted === 0
                                ?
                                <Button variant='light' onClick={() => history.push(`/singlePost/${element.posts_id}`)}>
                                    View Post
                                </Button>
                                : <span style={{ color: 'red' }}>This post has been deleted!</span>}
                        </div>
                        <span>Disliked on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                }
                if (element.comments_id !== null && element.description === 'like') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>Comment like</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            At post with title : &nbsp;
                            {element.title}&nbsp;&nbsp;&nbsp;
                            {element.isDeleted === 0
                                ?
                                <Button variant='light' onClick={() => history.push(`/singlePost/${element.posts_id}`)}>
                                    View Post
                                </Button>
                                : <span style={{ color: 'red' }}>This post has been deleted!</span>}
                        </div>
                        <span>Liked on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                }
                if (element.user2_id !== null && element.description === 'accfriendship') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>Accepted friendship</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            Requested from: &nbsp;
                            {element.username}&nbsp;&nbsp;&nbsp;
                            {element.isDeleted === 0
                                ? <Button variant='light' onClick={() => history.push(`/users/${element.user2_id}`)}>
                                    View Profile
                                </Button>
                                : <span style={{ color: 'red' }}>This user account has been deleted!</span>}
                        </div>
                        <span>Accepted on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                }
                if (element.user2_id !== null && element.description === 'sendreq') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>Friendship request</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            Requested to: &nbsp;
                            {element.username}&nbsp;&nbsp;&nbsp;
                            {element.isDeleted === 0
                                ? <Button variant='light' onClick={() => history.push(`/users/${element.user2_id}`)}>
                                    View Profile
                                </Button>
                                : <span style={{ color: 'red' }}>This user account has been deleted!</span>}
                        </div>
                        <span>Accepted on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                }
                if (element.user2_id !== null && element.description === 'unfriend') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>Denied friendship</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            Denied friendship with: &nbsp;
                            {element.username}&nbsp;&nbsp;&nbsp;
                            {element.isDeleted === 0
                                ? <Button variant='light' onClick={() => history.push(`/users/${element.user2_id}`)}>
                                    View Profile
                                </Button>
                                : <span style={{ color: 'red' }}>This user account has been deleted!</span>}
                        </div>
                        <span>Denied on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                }
                if (element.user2_id !== null && element.description === 'ban') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>User ban</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            User: &nbsp;
                            {element.username}&nbsp;&nbsp;&nbsp;
                            {element.isDeleted === 0
                                ? <Button variant='light' onClick={() => history.push(`/users/${element.user2_id}`)}>
                                    View Profile
                                </Button>
                                : <span style={{ color: 'red' }}>This user account has been deleted!</span>}
                        </div>
                        <span>Banned on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                }
                if (element.user2_id !== null && element.description === 'unban') {
                    return <div style={{ textAlign: 'left', marginLeft: '30px' }} key={element.id}>
                        <span style={{ fontWeight: 'bold', fontSize: '20px' }}>User unban</span>&nbsp;&nbsp;&nbsp;
                        <div>
                            User: &nbsp;
                            {element.username}&nbsp;&nbsp;&nbsp;
                            {element.isDeleted === 0
                                ? <Button variant='light' onClick={() => history.push(`/users/${element.user2_id}`)}>
                                    View Profile
                                </Button>
                                : <span style={{ color: 'red' }}>This user account has been deleted!</span>}
                        </div>
                        <span>Unbanned on: {new Date(element.date).toLocaleString()} </span>
                        <hr style={{ borderColor: 'black' }}></hr>
                    </div>
                }
            })
    )
}


export default Activity;