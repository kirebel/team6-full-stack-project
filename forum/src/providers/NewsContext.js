import { createContext } from 'react';

const NewsContext = createContext({
    news: null,
    setNews: () => { }
});

export default NewsContext;