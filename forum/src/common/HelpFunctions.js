import months from "./DateMonths";

export const dateFormatter = (date) => {
    return months[new Date(date).getMonth()] + ' ' + new Date(date).getDate()
};

