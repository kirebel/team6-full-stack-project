
import API_KEY from "../common/NewsApiKey"


/**
 * Makes request to a news API (newsapi.org) for getting
 * articles about JavaScript
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
* @return {object} => a response from the API 
 */

export const getNews = () => {
    return fetch(`https://newsapi.org/v2/everything?q=javascript&apiKey=${API_KEY} `)
        .then(res => res.json())
}

