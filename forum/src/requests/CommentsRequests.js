
import MAIN_URL from "../common/MainUrl";

/**
 * Makes request to the API (localhost:3001) for
 * collecting all comments of a single post
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} postId - the id of the post from 
 * which to extract all comments
 * @return {object} => a response from the API 
 */
export const getAllCommentsOfAPost = (postId) => {
    return fetch(`${MAIN_URL}comments/posts/${postId}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    })
        .then((response) => response.json())
};

/**
 * Makes request to the API (localhost:3001) for
 * changing the comment of a post
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} commentId - the id of the comment
 * @param {object} data - the object that holds the new data
 * @param {number} postId - the id of the post where
 * the comment is situated
 * @return {object} => a response from the API 
 */
export const editComment = (commentId, postId, data) => {
    const request = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(data)
    };
    return fetch(`${MAIN_URL}comments/${commentId}/posts/${postId}`, request)
        .then((response) => response.json());
};

/**
 * Makes request to the API (localhost:3001) for
 * deleting a comment from a post
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} commentId - the id of the comment
 * @param {number} postId - the id of the post where
 * the comment is situated
 * @return {object} => a response from the API 
 */
export const removeComment = (commentId, postId) => {
    const request = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        }
    };
    return fetch(`${MAIN_URL}comments/${commentId}/posts/${postId}`, request)
        .then((response) => response.json());
}

/**
 * Makes request to the API (localhost:3001) for
 * adding a comment to a post
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {object} comment - object that holds the data for comment
 * @param {number} commentId - the id of the comment
 * @return {object} => a response from the API 
 */
export const createComment = (comment, id) => {
    const request = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(comment)
    };

    return fetch(MAIN_URL + 'comments/posts/' + id, request)
        .then(res => res.json());
}

/**
 * Makes request to the API (localhost:3001) for
 * setting a like to a comment
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} commentId - the id of the comment
 * @param {number} postId - the id of the post where
 * the comment is situated
 * @return {object} => a response from the API 
 */

export const setCommentLike = (commentId, postId) => {
    const request = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
    };

    return fetch(MAIN_URL + `comments/${commentId}/like/${postId}`, request)
        .then(res => res.json());
};

/**
 * Makes request to the API (localhost:3001) for
 * dislike a comment
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} commentId - the id of the comment
 * @param {number} postId - the id of the post where
 * the comment is situated
 * @return {object} => a response from the API 
 */


export const deleteCommentLike = (commentId, postId) => {
    const request = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        },

    };

    return fetch(MAIN_URL + `comments/${commentId}/dislike/${postId}`, request)
        .then(res => res.json());
};


/**
 * Makes request to the API (localhost:3001) getting
 * the all likes of a comment
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} commentId - the id of the comment
 * @return {object} => a response from the API 
 */

export const getCommentLikes = (commentId) => {
    const request = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    };

    return fetch(MAIN_URL + `comments/${commentId}/likes`, request)
        .then(res => res.json())
};

/**
 * Makes request to the API (localhost:3001) getting
 * the all comments count from the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>7
 * @return {object} => a response from the API 
 */

export const getCommentsCount = () => {
    const request = {
        headers: {
            'Content-Type': 'application/json',
        }
    };

    return fetch(MAIN_URL + `comments/c_count`, request)
        .then(res => res.json())
};