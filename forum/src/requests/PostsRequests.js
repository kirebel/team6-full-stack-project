import MAIN_URL from "../common/MainUrl";

/**
 * Makes request to the API (localhost:3001) for getting all the
 * posts in the database
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @return {object} => a response from the API 
*/

export const getAllPosts = () => {

  return fetch(MAIN_URL + 'posts', {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  })

    .then(res => res.json())

};

/**
 * Makes request to the API (localhost:3001) for getting all last
 * 5 published posts
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @return {object} => a response from the API 
*/


export const getLastPosts = () => {

  return fetch(MAIN_URL + 'posts/last', {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  })

    .then(res => res.json())

};

/**
 * Makes request to the API (localhost:3001) for getting 
 * single post by id
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} id - the id of the post
* @return {object} => a response from the API 
 */

export const getSinglePost = (id) => {
  return fetch(`${MAIN_URL}posts/${id}`, {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  })
    .then((response) => response.json())
};

/**
 * Makes request to the API (localhost:3001) for creating
 * a post
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {object} post - the post information(title,text)
* @return {object} => a response from the API 
 */


export const createPost = (post) => {
  const request = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    },
    body: JSON.stringify(post)
  };

  return fetch(MAIN_URL + 'posts', request)
    .then(res => res.json())
};


/**
 * Makes request to the API (localhost:3001) for deleting
 * a post
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} id  - the post id
* @return {object} => a response from the API 
 */

export const deletePost = (id) => {
  const request = {
    method: 'DELETE',
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  };

  return fetch(MAIN_URL + 'posts/' + id, request)
    .then(res => res.json())
};

/**
 * Makes request to the API (localhost:3001) for updating
 * a post
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} id  - the post id
 * @param {object} data  - the information for updating the post
* @return {object} => a response from the API 
 */

export const updatePostRequest = (id, data) => {
  const request = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    },
    body: JSON.stringify(data)
  };

  return fetch(MAIN_URL + 'posts/' + id, request)
    .then(res => res.json());
}

/**
 * Makes request to the API (localhost:3001) for locking
 * a post
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} id  - the post id
* @return {object} => a response from the API 
 */

export const lockPostInTheDatabase = (id) => {
  const request = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    },
  };

  return fetch(`${MAIN_URL}posts/${id}/lock`, request)
    .then(res => res.json());
};

/**
 * Makes request to the API (localhost:3001) for unlocking
 * a post
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} id  - the post id
* @return {object} => a response from the API 
 */

export const unLockPost = (id) => {
  const request = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    },
  };

  return fetch(MAIN_URL + 'posts/' + id + '/unlock', request)
    .then(res => res.json());
};

/**
 * Makes request to the API (localhost:3001) for flagging
 * a post
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} id  - the post id
* @return {object} => a response from the API 
 */

export const flagPostInDatabase = (id) => {
  const request = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    },
  };

  return fetch(MAIN_URL + 'posts/' + id + '/flag', request)
    .then(res => res.json());
};

/**
 * Makes request to the API (localhost:3001) for unflagging
 * a post
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} id  - the post id
* @return {object} => a response from the API 
 */

export const unFlagPost = (id) => {
  const request = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    },
  };

  return fetch(MAIN_URL + 'posts/' + id + '/unflag', request)
    .then(res => res.json());
};


/**
 * Makes request to the API (localhost:3001) for all
 * flagged posts
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
* @return {object} => a response from the API 
 */


export const getFlaggedPosts = () => {

  return fetch(MAIN_URL + 'posts/flagged', {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  })

    .then(res => res.json())
}

/**
 * Makes request to the API (localhost:3001) for setting
 * a like to a post
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} postId - the id of the post
 * @return {object} => a response from the API 
 */


export const setPostLike = (postId) => {
  const request = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    },
  };

  return fetch(MAIN_URL + `posts/${postId}/like`, request)
    .then(res => res.json());
};

/**
 * Makes request to the API (localhost:3001) for setting
 * a dislike to a post
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} postId - the id of the post
 * @return {object} => a response from the API 
 */


export const deletePostLike = (postId) => {
  const request = {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    },

  };

  return fetch(MAIN_URL + `posts/${postId}/dislike`, request)
    .then(res => res.json());
};

/**
 * Makes request to the API (localhost:3001) getting
 * all likes of a post
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} postId - the id of the post
 * @return {object} => a response from the API 
 */


export const getPostLikes = (postId) => {
  const request = {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  };

  return fetch(MAIN_URL + `posts/${postId}/likes`, request)
    .then(res => res.json())
};

/**
 * Makes request to the API (localhost:3001) getting
 * all posts that pass a criteria
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {string} searchTerm - the criteria to pass
 * @return {object} => a response from the API 
 */


export const searchPostsFromTheDatabase = (searchTerm) => {

  return fetch(`${MAIN_URL}posts?q=${searchTerm}`, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  })

    .then(res => res.json())

};

/**
 * Makes request to the API (localhost:3001) getting
 * the 3 most liked posts in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @return {object} => a response from the API 
 */


export const getMostLikedPosts = () => {

  return fetch(MAIN_URL + 'posts/most_liked', {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  })

    .then(res => res.json())

};

/**
 * Makes request to the API (localhost:3001) getting
 * the 3 most commented posts in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @return {object} => a response from the API 
 */

export const getMostCommentedPosts = () => {

  return fetch(MAIN_URL + 'posts/most_commented', {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  })

    .then(res => res.json())

};

/**
 * Makes request to the API (localhost:3001) getting
 * the count of all posts in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @return {object} => a response from the API 
 */

export const getPostsCount = () => {

  return fetch(MAIN_URL + 'posts/p_count', {
    headers: {
      'Content-Type': 'application/json',
    }
  })

    .then(res => res.json())

};

/**
 * Makes request to the API (localhost:3001) getting
 * the count of all likes in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @return {object} => a response from the API 
 */

export const getLikesCount = () => {
  return fetch(MAIN_URL + 'posts/l_count', {
    headers: {
      'Content-Type': 'application/json',
    }
  })
    .then(res => res.json())

}