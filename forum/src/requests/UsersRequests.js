import MAIN_URL from "../common/MainUrl";


/**
 * Makes request to the API (localhost:3001) 
 * for creating new user
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {object} data - object that holds the data with user info
 * @return {object} => a response from the API 
 */
export const createUser = (data) => {
    const request = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    };
    return fetch(`${MAIN_URL}users`, request)
};

/**
 * Makes request to the API (localhost:3001) 
 * for logging into the site
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {object} data - object that holds the data with user info
 * @return {object} => a response from the API 
 */
export const loginUser = (data) => {
    const request = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(data)
    };

    return fetch(`${MAIN_URL}auth/signin`, request);

};

/**
 * Makes request to the API (localhost:3001) 
 * for displaying all users
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @return {object} => a response from the API 
 */
export const showAllUsers = () => {
    const request = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },
    }

    return fetch(`${MAIN_URL}users`, request)
        .then((res) => res.json());
}

/**
 * Makes request to the API (localhost:3001) 
 * for displaying a single user details
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} id - id of the user
 * @return {object} => a response from the API 
 */
export const getSingleUser = (id) => {
    const request = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },
    }

    return fetch(`${MAIN_URL}users/${id}`, request)
        .then((res) => res.json());
}

/**
 * Makes request to the API (localhost:3001) 
 * for updating users credentials
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} id - id of the user
 * @param {object} data - object that holds the data with user info
 * @return {object} => a response from the API 
 */
export const updateUser = (id, data) => {
    const request = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },

        body: JSON.stringify(data)
    }
    return fetch(`${MAIN_URL}users/${id}`, request)
        .then((res) => res.json());
}

/**
 * Makes request to the API (localhost:3001) 
 * for deleting an user
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} id - id of the user
 * @return {object} => a response from the API 
 */
export const deleteUser = (id) => {
    const request = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },

    }
    return fetch(`${MAIN_URL}users/${id}`, request)
        .then((res) => res.json());
}

/**
 * Makes request to the API (localhost:3001) 
 * for making an user ban
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} id - id of the user
 * @param {object} data - object that holds the data about the ban reason
 * @return {object} => a response from the API 
 */
export const banUser = (id, data) => {
    const request = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },

        body: JSON.stringify(data)
    }
    return fetch(`${MAIN_URL}users/${id}/banstatus`, request)
        .then((res) => res.json());
}

/**
 * Makes request to the API (localhost:3001) 
 * for making an user active again
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} id - id of the user
 * @return {object} => a response from the API 
 */
export const unbanUser = (id) => {
    const request = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },

    }
    return fetch(`${MAIN_URL}users/${id}/banstatus`, request)
        .then((res) => res.json());
}

/**
 * Makes request to the API (localhost:3001) 
 * for displaying activity of a comment
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} id - id of the comment
 * @return {object} => a response from the API 
 */
export const getCommentsActivity = (id) => {
    const request = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },
    }

    return fetch(`${MAIN_URL}users/${id}/c_activity`, request)
        .then((res) => res.json());

}

/**
 * Makes request to the API (localhost:3001) 
 * for displaying activity of a post
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} id - id of the post
 * @return {object} => a response from the API 
 */
export const getPostsActivity = (id) => {
    const request = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },
    }

    return fetch(`${MAIN_URL}users/${id}/p_activity`, request)
        .then((res) => res.json());

}

/**
 * Makes request to the API (localhost:3001) 
 * for displaying users friendship
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @return {object} => a response from the API 
 */
export const getFriendshipInfo = () => {
    const request = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },
    }

    return fetch(`${MAIN_URL}users/friendship/info`, request)
        .then((res) => res.json());

}

/**
 * Makes request to the API (localhost:3001) 
 * for displaying user`s friendship
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} secondId - id of the user
 * @return {object} => a response from the API 
 */
export const friendshipRequest = (secondId) => {
    const request = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },
    }
    return fetch(`${MAIN_URL}users/${secondId}/request_friendship`, request)
        .then((res) => res.json());
}

/**
 * Makes request to the API (localhost:3001) 
 * for approving user`s friendship
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} requestUserId - id of the user
 * @return {object} => a response from the API 
 */
export const acceptFriendship = (requestUserId) => {
    const request = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },

    }
    return fetch(`${MAIN_URL}users/${requestUserId}/accept_friendship`, request)
        .then((res) => res.json());
}

/**
 * Makes request to the API (localhost:3001) 
 * for removing user`s friendship
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} userId - id of the user to be removed
 * @return {object} => a response from the API 
 */
export const unFriend = (userId) => {
    const request = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },

    }
    return fetch(`${MAIN_URL}users/${userId}/unfriend`, request)
        .then((res) => res.json());
}

/**
 * Makes request to the API (localhost:3001) 
 * for declining user`s friendship
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} userId - id of the user to be declined
 * @return {object} => a response from the API 
 */
export const declineFriendship = (userId) => {
    const request = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },

    }
    return fetch(`${MAIN_URL}users/${userId}/unfriend`, request)
        .then((res) => res.json());
}

/**
 * Makes request to the API (localhost:3001) getting
 * users passing a specific criteria
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>7
 * @param {string} searchTerm - the criteria that has to be passed
 * @return {object} => a response from the API 
 */

export const searchUsersFromTheDataBase = (searchTerm) => {

    return fetch(`${MAIN_URL}users?q=${searchTerm}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    })

        .then(res => res.json())

};

/**
 * Makes request to the API (localhost:3001) getting
 * the activity of an user
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>7
 * @param {number} id - the id of the user
 * @return {object} => a response from the API 
 */

export const getUsersActivity = (id) => {
    const request = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },
    }

    return fetch(`${MAIN_URL}users/${id}/u_activity`, request)
        .then((res) => res.json());

}

/**
 * Makes request to the API (localhost:3001) getting
 * the posts liked by the user
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>7
 * @param {number} userId - the id of the user
 * @return {object} => a response from the API 
 */


export const getUserLikedPosts = (userId) => {

    return fetch(MAIN_URL + `users/${userId}/liked_posts`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    })

        .then(res => res.json())

};

/**
 * Makes request to the API (localhost:3001) getting
 * the comments liked by the user
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>7
 * @param {number} userId - the id of the user
 * @return {object} => a response from the API 
 */


export const getUserLikedComments = (userId) => {

    return fetch(MAIN_URL + `users/${userId}/liked_comments`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    })

        .then(res => res.json())

};

/**
 * Makes request to the API (localhost:3001) getting
 * the count of all friendships in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @return {object} => a response from the API 
 */


export const getFriendshipsCount = () => {

    return fetch(MAIN_URL + `users/f_count`, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => res.json())

};


/**
 * Makes request to the API (localhost:3001) getting
 * the count of all users in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @return {object} => a response from the API 
 */


export const usersCount = () => {

    return fetch(MAIN_URL + `users/u_count`, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => res.json())
}