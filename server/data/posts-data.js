
import pool from './pool.js';

/**
 * Makes sql request to the database and return the count of all posts
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {}
 * @return {object} => Returns an object from the database
 * with property 'c' which value is the count of all posts in the database
 */

const getPostsCount = async () => {

    const sql = `
    SELECT count(*) as c 
    FROM posts
    WHERE isDeleted = 0`;

    const result = await pool.query(sql);
    return result[0];

};

/**
 * Makes sql request to the database and return the count of all likes
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {}
 * @return {object} => Returns an object from the database
 * with property 'c' which value is the count of all actual
 * (deleted posts and comments likes are not counted)
 * likes in the database
 */

const getAllLikesCount = async () => {
    const sql = `
    SELECT count(l.id) as c
    FROM likes as l
    JOIN posts as p on p.id = l.posts_id
    WHERE p.isDeleted = 0`;

    const sql2 = `SELECT count(l.id) as c
    FROM likes as l
    JOIN comments as c on c.id = l.comments_id
    WHERE c.isDeleted = 0`;

    const result1 = await pool.query(sql);
    const result2 = await pool.query(sql2);

    return { c: result1[0].c + result2[0].c };
};

/**
 * Makes sql request to the database and return the 3 most liked posts
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {}
 * @return {array} => Returns an array from the database
 * with 3 posts which are most liked
 */

const getMostLikedPosts = async () => {

    const sql = `
    SELECT p1.id, p1.title,p1.post_date, (select count(p.id) 
    from posts p 
    inner join likes l on l.posts_id=p.id
    WHERE p.id = p1.id) as number_likes,
    (select username
    from users as u 
    where u.id = p1.users_id)
    as username
    from posts as p1
    WHERE p1.isDeleted = 0
    ORDER BY number_likes
    DESC LIMIT 3
    `;

    return await pool.query(sql);

};

/**
 * Makes sql request to the database and return the 3 most commented posts
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {}
 * @return {array} => Returns an array from the database
 * with 3 posts which are most commented
 */

const getMostCommentedPosts = async () => {
    const sql = `SELECT p1.id, p1.post_date,  p1.title, (select count(c.id) 
    from posts p 
    inner join comments c on c.posts_id=p.id
    WHERE p.id = p1.id AND c.isDeleted = 0) as number_comments,
    (select username
    from users as u 
    where u.id = p1.users_id)
    as username
    from posts as p1
    WHERE p1.isDeleted = 0
    ORDER BY number_comments
    DESC LIMIT 3
`;
    return await pool.query(sql);

};

/**
 * Makes sql request to the database and return all posts in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {}
 * @return {array} => Returns an array from the database
 * with all posts
 */

const getAllPosts = async () => {
    return await pool.query(`select p.id, p.title, p.text, p.post_date, p.isDeleted, p.users_id,u.username 
    from posts as p
    JOIN users as u on u.id = p.users_id
    where p.isDeleted = 0`);
};

/**
 * Makes sql request to the database and return last 5 posts created
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {}
 * @return {array} => Returns an array from the database
 * with lat 3 posts created
 */

const getLastPosts = async () => {
    return await pool.query(`select p.id, p.title, p.text, p.post_date,p.isDeleted, p.users_id,u.username 
    from posts as p
    JOIN users as u on u.id = p.users_id
    where p.isDeleted = 0
    order by p.id DESC
    limit 5`);
};

/**
 * Makes sql request to the database for one post searched by id,title ect.
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {string} column - the name of the column in the database table
 * @param {string or number} value - the value of the searched item in the column
 * @return {object} => Returns an object of the searched post
 */

const getPostBy = async (column, value) => {
    const result = await pool.query(`select p.id, p.title,p.isLocked,p.isFlagged, p.text, 
    p.post_date,p.isDeleted, p.users_id,u.username, count(c.id)  as comments
    from posts as p
    INNER JOIN users as u on u.id = p.users_id
    INNER JOIN comments as c on c.posts_id = p.id
    where p.isDeleted = 0 and p.${column} = ? `, [value]);

    return result[0];
};

/**
 * Makes sql request to the database for one post searched by id,title ect.
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {string} column - the name of the column in the database table
 * @param {string or number} value - the criteria for searching(title,text,id ect.)
 * @return {array} => Returns an array with objects that pass the criteria
 */

const searchPostBy = async (column, value) => {
    const result = await pool.query(`select p.id,p.title,p.text,p.post_date,p.users_id , u.username
    from posts as p 
    JOIN users as u on u.id = p.users_id
    where p.isDeleted = 0 and p.${column} like '%${value}%'`);

    return result;

};

/**
 * Makes sql request to the database for creating a post and
 * write the user action in the activity table in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {string} title - the title of the post
 * @param {string } text- the text of the post
 * @param {number } userId- the id of the user that made the post
 * @return {object} => Returns the post created
 */

const create = async (title, text, userId) => {
    const result = await pool.query('insert into posts(title,text, users_id) values (?,?,?)', [title, text, userId]);

    const res1 = await getPostBy('id', result.insertId);

    await pool.query('insert into activity(users_id,posts_id,description) values(?,?,?)', [userId, res1.id, 'post']);

    return res1;
};

/**
 * Makes sql request to the database for updating a post and 
 * writes the user action in the activity table in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {object} post - the information about the post(title,text,id)
 * @param {number } userId- the id of the users that updates the post
 * @return {object} => Returns the updated post
 */

const update = async (post, userId) => {
    const { id, title, text } = post;
    const date = new Date();
    const result = await pool.query(`update posts set
    title = ?,
    post_date= ?,
    text = ?
    where id =? `, [title, date, text, id]);

    await pool.query('insert into activity(users_id,posts_id,description) values(?,?,?)', [userId, id, 'put']);

    return result;
};

/**
 * Makes sql request to the database for deleting a post, deleting all the
 * likes of the post and writes the users action to the activity table in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {object} post - the information about the post(isDeleted,id)
 * @param {number } userId- the id of the user that deletes the post
 * @return {object} => Returns the deleted post
 */

const deleteP = async (post, userId) => {
    const { isDeleted, id } = post;
    const result = await pool.query(`update posts
    set isDeleted = ?
    where id =?`, [isDeleted, id]);

    await pool.query('insert into activity(users_id,posts_id,description) values(?,?,?)', [userId, id, 'del']);

    await pool.query(`UPDATE
      comments 
      SET  isDeleted=1
      WHERE posts_id = ?`, [id]);

    await pool.query(`DELETE from
    likes 
    where posts_id  = ?`, [id]);

    return result;
};

/**
 * Makes sql request to the database for the likes of a single post
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} postId- the id of the post that likes info has to be get
 * @return {array} => the information about the likes from the database
 */

const postsLikes = async (postId) => {

    const sql = `SELECT l.id, l.isLiked,l.users_id, l.posts_id,l.comments_id,u.username
    FROM likes as l
    JOIN users as u on l.users_id = u.id
    WHERE l.posts_id = ?
    AND l.isLiked = 0`;

    const result = await pool.query(sql, [postId]);
    return result;

};

/**
 * Makes sql request to the database for setting a like, getting single post
 * like, and writes the users action in the activity table in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} userId- the id of the user setting the like
 * @param {number} postId- the id of the post that like is set
 * @return {array} => the information about the likes of the post from the database
 */

const createLike = async (userId, postId) => {

    const sql = `
    INSERT INTO
    likes(users_id,posts_id) values(?,?) `;
    const _ = await pool.query(sql, [userId, postId]);

    const sql1 = `
    SELECT 
    * FROM 
    likes 
    WHERE 
    posts_id = ?`;

    await pool.query('insert into activity(users_id,posts_id,description) values(?,?,?)', [userId, postId, 'like']);

    const result1 = await pool.query(sql1, [postId]);

    return result1;
};

/**
 * Makes sql request to the database for setting a dislike, getting single post
 * like, and writes the users action in the activity table in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} userId- the id of the user setting the like
 * @param {number} postId- the id of the post that dislike is set
 * @return {array} => the information about the likes of the post from the database
 */

const deleteLike = async (userId, postId) => {

    const sql = `DELETE FROM
    likes
    WHERE posts_id = ?
    AND users_id = ?
    `;

    const _ = await pool.query(sql, [postId, userId]);

    const sql1 = `
    SELECT 
    * FROM 
    likes 
    WHERE 
    posts_id = ?`;

    const result1 = pool.query(sql1, [postId]);

    await pool.query('insert into activity(users_id,posts_id,description) values(?,?,?)', [userId, postId, 'dislike']);

    return result1;

};

/**
 * Makes sql request to the database for getting the flagged posts in
 * the database and writes the user action in the activity table in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {} 
 * @return {array} => an array with all the flagged posts
 */

const getFlagged = async () => {
    const sql = `
    SELECT * 
    FROM posts
    WHERE isFlagged = 1
    AND isDeleted = 0
    `;

    return await pool.query(sql);
};

/**
 * Makes sql request to the database for lock a single post
 * in the database and writes the user action in the activity table in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} userId - the user id that is locking the post
 * @param {number} id - the id of the locked post 
 * @return {object} => the information about the locked post 
 * in the database(insertId,affectedRows ect.)
 */

const lock = async (userId, id) => {

    const sql = `UPDATE posts 
    SET isLocked = 1
    WHERE id = ?
    `;

    await pool.query('insert into activity(users_id,posts_id,description) values(?,?,?)', [userId, id, 'lock']);

    return await pool.query(sql, [id]);

};

/**
 * Makes sql request to the database for unlock a single post
 * in the database and writes the user action in the activity table in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} userId - the user id that is unlocking the post
 * @param {number} id - the id of the unlocked post 
 * @return {object} => the information about the unlocked post 
 * in the database(insertId,affectedRows ect.)
 */

const unLock = async (userId, id) => {
    const sql = `UPDATE posts 
    SET isLocked = 0
    WHERE id = ?
    `;

    await pool.query('insert into activity(users_id,posts_id,description) values(?,?,?)', [userId, id, 'unlock']);

    return await pool.query(sql, [id]);
};

/**
 * Makes sql request to the database for flagging a single post
 * in the database and writes the user action in the activity table in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} userId - the user id that is flagging the post
 * @param {number} id - the id of the flagged post 
 * @return {object} => the information about the flagged post 
 * in the database(insertId,affectedRows ect.)
 */

const flag = async (userId, id) => {
    const sql = `UPDATE posts 
    SET isFlagged = 1
    WHERE id = ?
    `;

    await pool.query('insert into activity(users_id,posts_id,description) values(?,?,?)', [userId, id, 'flag']);

    return await pool.query(sql, [id]);
};

/**
 * Makes sql request to the database for unflagging a single post
 * in the database and writes the user action in the activity table in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} userId - the user id that is unflagging the post
 * @param {number} id - the id of the unflagged post 
 * @return {object} => the information about the unflagged post 
 * in the database(insertId,affectedRows ect.)
 */

const unFlag = async (userId, id) => {
    const sql = `UPDATE posts 
    SET isFlagged = 0
    WHERE id = ?
    `;

    await pool.query('insert into activity(users_id,posts_id,description) values(?,?,?)', [userId, id, 'unflag']);

    return await pool.query(sql, [id]);
};

export default {
    getAllPosts,
    getLastPosts,
    getPostBy,
    searchPostBy,
    create,
    update,
    deleteP,
    postsLikes,
    createLike,
    deleteLike,
    lock,
    unLock,
    flag,
    unFlag,
    getFlagged,
    getMostCommentedPosts,
    getMostLikedPosts,
    getPostsCount,
    getAllLikesCount,
};