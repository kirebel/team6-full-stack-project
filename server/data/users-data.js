import pool from './pool.js';
import { isDeleted, isBanned } from '../common/constants.js';

/**
 * Makes sql request to the database for getting
 * all users
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @return {object} => an object with all
 * user`s data
 */

const getAll = async () => {
    const sql = `
        SELECT u.id, u.username, u.reg_date, r.role
        FROM users as u
        JOIN role as r on u.role_id = r.id 
        WHERE u.isDeleted =0
    `;

    return await pool.query(sql);
};

/**
 * Makes sql request to the database for getting
 * the number of users
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @return {object} => an object with the 
 * number of users
 */

const getUsersCount = async () => {
    const sql = `
    SELECT count(*) as c
    FROM users
    WHERE isDeleted =0
`;

    const result = await pool.query(sql);
    return result[0];

};

/**
 * Makes sql request to the database for getting
 * an user`s number of friends
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @return {object} => an object with the 
 * number of user`s friends
 */

const getFriendshipsCount = async () => {

    const sql = `
    SELECT count(*) as c
    FROM friends
    WHERE friendship = 1
`;

    const result = await pool.query(sql);
    return result[0];

};

/**
 * Makes sql request to the database for getting
 * an user`s role info
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {string} username - string for column username
 * @return {object} => an object with the 
 * user`s data
 */

const getWithRole = async (username) => {
    const sql = `
    SELECT  u.id,u.username, u.password, isBanned,ban_reason,  r.role 
    FROM users u
    JOIN role as r on u.role_id = r.id
    WHERE username=? AND isDeleted =0
    `;

    const result = await pool.query(sql, [username]);

    return result[0];
};

/**
 * Makes sql request to the database for checking
 * if username already exist
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {string} column - string for column name
 * @param {number} value - id of the user
 * @return {object} => an object with the 
 * username if exist
 */

const checkForDuplicateUsername = async (column, value) => {
    const sql = `
    SELECT username
    FROM users
    WHERE ${column}=?`;

    const result = await pool.query(sql, [value]);

    return result[0];
};

/**
 * Makes sql request to the database for getting
 * an user
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {string} column - string for column name
 * @param {number} value - id of the user
 * @return {object} => an object with the 
 * updated user`s data
 */

const getBy = async (column, value) => {
    const sql1 = `
    SELECT u.id, u.username, u.reg_date, u.isBanned, count(p.id) as posts_count
    FROM users as u
    JOIN posts as p 
    WHERE u.${column} = ?   AND u.id = p.users_id
    AND u.isDeleted =0 AND p.isDeleted =0
`;

    const sql2 = `
    SELECT u.id, u.username, u.reg_date, u.isBanned, count(c.id) as comments_count
    FROM users as u
    JOIN comments as c
    WHERE u.${column} = ?   AND u.id = c.users_id
    AND u.isDeleted =0 AND c.isDeleted =0`;

    const result = await pool.query(sql1, [value]);
    const result2 = await pool.query(sql2, [value]);

    const a = { ...result[0], ...result2[0] };
    return a;
};

/**
 * Makes sql request to the database for getting
 * an user
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {string} column - string for column name
 * @param {number} value - id of the user
 * @return {object} => an object with the 
 * updated user`s data
 */

const searchBy = async (column, value) => {
    const sql = `
        SELECT id, username, reg_date 
        FROM users
        WHERE ${column} LIKE '%${value}%' 
        AND isDeleted =0
    `;

    return await pool.query(sql);
};

/**
 * Makes sql request to the database for creating
 * a new user
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {string} username - string with user`s username
 * @param {string} password - string with user`s password
 * @param {number} role - id of the role
 * @return {object} => an object with the 
 * created user`s data
 */

const create = async (username, password, role) => {
    const sql = `
        INSERT INTO users(username, password, role_id)
        VALUES (?,?,(SELECT id FROM role WHERE role = ?))
    `;

    const result = await pool.query(sql, [username, password, role]);

    return {
        id: result.insertId,
        username: username
    };
};

/**
 * Makes sql request to the database for updating
 * an user`s credentials
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {object} user - object that holds the user`s data
 * @return {object} => an object with the 
 * updated user`s data
 */

const update = async (user) => {
    const { id, username } = user;
    const sql = `
        UPDATE users SET
          username = ?
        WHERE id = ?
        AND isDeleted =0
    `;

    return await pool.query(sql, [username, id]);
};

/**
 * Makes sql request to the database for removing
 * an user 
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {object} user - object that holds the id of the user to be deleted
 * @return {object} => an object with the 
 * deleted user`s data
 */

const remove = async (user) => {
    const sql = `
    UPDATE users SET
    isDeleted = ?
  WHERE id = ?
    `;

    return await pool.query(sql, [isDeleted, user.id]);
};

/**
 * Makes sql request to the database for making
 * an user ban
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} userId - id of the user to make ban
 * @param {object} user - object that holds the id of the user to be banned and ban reason 
 * @return {object} => an object with the 
 * baned user`s data
 */

const ban = async (userId, user) => {
    const { id, ban_reason } = user;
    const sql = `
    UPDATE users SET
    isBanned = ?,
    ban_reason = ?
    WHERE id =?`;

    await pool.query('insert into activity(users_id,user2_id,description) values(?,?,?)', [userId, id, 'ban']);

    return await pool.query(sql, [isBanned, ban_reason, id]);
};

/**
 * Makes sql request to the database for making
 * an user active again
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} userId - id of the user to make ban
 * @param {number} id - id of the user to be unban
 * @return {object} => an object with the 
 * unbaned user`s data
 */

const unban = async (userId, id) => {
    const sql = `UPDATE users SET
    isBanned = DEFAULT,
    ban_reason =DEFAULT
    WHERE id =?`;

    await pool.query('insert into activity(users_id,user2_id,description) values(?,?,?)', [userId, id, 'unban']);

    return await pool.query(sql, [id]);
};

/**
 * Makes sql request to the database for getting the 
 * activity of a user`s comments
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} id - id of the user
 * @return {object} => an object with the 
 * user`s comments activity data
 */

const getCommentsActivity = async (id) => {
    const sql = `SELECT  a.id,a.users_id,a.comments_id,a.posts_id,a.description,a.date,
    p.title,p.isDeleted,a.user2_id, p.text,c.text as comment_text
    FROM activity as a
    JOIN posts as p
    JOIN comments as c
    WHERE a.users_id=? AND p.id=a.posts_id AND c.id=a.comments_id `;

    return await pool.query(sql, [id]);
};

/**
 * Makes sql request to the database for getting the 
 * activity of a user`s posts
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} id - id of the user
 * @return {object} => an object with the 
 * user`s posts activity data
 */

const getPostsActivity = async (id) => {
    const sql = `   SELECT a.id,a.users_id,a.comments_id,a.posts_id,a.description,a.date,p.title,p.isDeleted, a.user2_id
    FROM activity as a
    JOIN posts as p on p.id= a.posts_id
    WHERE a.users_id=?  AND a.comments_id IS NULL  `;

    return await pool.query(sql, [id]);
};

/**
 * Makes sql request to the database for getting the 
 * activity of a single user
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} id - id of the user
 * @return {object} => an object with the 
 * user`s activity data
 */

const getUsersActivity = async (id) => {

    const sql = `SELECT a.id,a.description, a.user2_id,a.date,u.username, u.isDeleted
    FROM activity as a 
    JOIN users as u on u.id = user2_id
    WHERE users_id = ? AND posts_id IS NULL`;

    return await pool.query(sql, [id]);

};

/**
 * Makes sql request to the database for sending a friendship
 * request and writes the user action in the activity table in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} requestId the id of the user that ask for friendship
 * @param {number} secondId the id of the user that the request is sent to
* @return {object} => the information about the friendship
    request (insertId,affectedRows ect.)
 */

const sendFriendRequest = async (requestId, secondId) => {
    const sql = `INSERT INTO friends(users_id, users_id1)
    VALUES (?,?)`;

    await pool.query('insert into activity(users_id,user2_id,description) values(?,?,?)', [requestId, secondId, 'sendreq']);

    return await pool.query(sql, [requestId, secondId]);
};

/**
 * Makes sql request to the database for accepting a friendship
 * request and writes the user action in the activity table in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} otherId the id of the user that ask for friendship
 * @param {number} secondId the id of the user that accepts the request
* @return {object} => the information about the friendship
    accepted (insertId,affectedRows ect.)
 */

const acceptFriendRequest = async (otherId, acceptId) => {
    const sql = `UPDATE friends 
    SET friendship = 1
    WHERE users_id = ?
    AND users_id1 = ?`;

    await pool.query('insert into activity(users_id,user2_id,description) values(?,?,?)', [acceptId, otherId, 'accfriendship']);

    return await pool.query(sql, [otherId, acceptId]);
};

/**
 * Makes sql request to the database for decline friendship request
 * or unfriend some of the user friends and writes the user action
 * in the activity table in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} firstId the id of user that unfriends 
 * @param {number} secondId the id of other user 
* @return {object} => the information about the friendship
    denied (insertId,affectedRows ect.)
 */

const unfriend = async (firstId, secondId) => {
    const sql = `DELETE
    FROM friends
    WHERE users_id = ? AND users_id1 = ?
    OR users_id1 = ? ANd users_id = ?`;

    await pool.query('insert into activity(users_id,user2_id,description) values(?,?,?)', [secondId, firstId, 'unfriend']);

    return await pool.query(sql, [firstId, secondId, firstId, secondId]);
};

/**
 * Makes sql request to the database for all of the
 * friendships info in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {}
 * @return {array} => the friendships information
 */

const friendsInfo = async () => {
    const sql = `
    SELECT f.id, f.users_id, f.users_id1, f.friendship,u.isDeleted as isDeleted1,u2.isDeleted as isDeleted2, u.username as first_username, u2.username as second_username
    FROM friends as f 
	JOIN users as u 
    on u.id = f.users_id 
	JOIN users as u2 
	on  f.users_id1 = u2.id
`;

    const result = await pool.query(sql);

    return result;
};

/**
 * Makes sql request to the database for getting the count
 * of the liked posts for a single user
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} userId - the single user id
 * @return {object} => an object with property count
 * which value is the count of the posts likes
 */

const getUserLikedPosts = async (userId) => {
    const sql = `SELECT  l.id,  l.posts_id, u.username, p.title, p.post_date
    FROM likes as l
    JOIN posts as p on p.id = l.posts_id
    JOIN users as u on u.id = p.users_id
    WHERE l.users_id = ?  AND p.isDeleted = 0`;
    const result = await pool.query(sql, [userId]);
    return result;
};

/**
 * Makes sql request to the database for getting the count
 * of the liked comments for a single user
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} userId - the single user id
 * @return {object} => an object with property count
 * which value is the count of the comments likes
 */

const getUserCommentLikes = async (userId) => {
    const sql = `SELECT  count(*) as count FROM
    likes as l 
    JOIN users as u on u.id = l.users_id
    JOIN comments as c on l.comments_id = c.id
    WHERE l.comments_id IS NOT NULL
    AND l.users_id = ? AND c.isDeleted = 0 `;

    const result = await pool.query(sql, [userId]);

    return result[0];
};

export default {
    getAll,
    searchBy,
    getBy,
    create,
    update,
    remove,
    getWithRole,
    ban,
    unban,
    checkForDuplicateUsername,
    getCommentsActivity,
    getPostsActivity,
    sendFriendRequest,
    acceptFriendRequest,
    unfriend,
    friendsInfo,
    getUsersActivity,
    getUserLikedPosts,
    getUserCommentLikes,
    getUsersCount,
    getFriendshipsCount,
};
