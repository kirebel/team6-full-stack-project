
import pool from './pool.js';

/**
 * Makes sql request to the database for getting
 * all comments of a single post
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {number} id = id of the post 
 * @return {object} => an object with 
 * all the comments details
 */

const getPostComments = async (id) => {
    const result = await pool.query(`SELECT  c.id, c.text, c.posts_id, c.post_date, c.users_id, u.username
    FROM comments as c
    JOIN users as u on u.id = c.users_id
    WHERE c.isDeleted =0 AND c.posts_id = ?`, [id]);
    return result;
};

/**
 * Makes sql request to the database for the count
 * of all likes on comments in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {} 
 * @return {object} => and object with property
 * c which value is the comments count
 */

const allComments = async () => {
    const sql = `SELECT count(*) as c
    FROM comments
    WHERE isDeleted = 0`;

    const result = await pool.query(sql);

    return result[0];
};

/**
 * Makes sql request to the database for adding
 * new comment of a single post
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {object} text = object with the text of the comment 
 * @param {number} userId = id of the user 
 * @param {number} posts_id = id of the post 
 * @return {object} => an object with 
 * the comment details of a post
 */

const createComments = async (text, userId, posts_id) => {
    const result = await pool.query('insert into comments(text,users_id,posts_id) values(?,?,?)', [text, userId, posts_id]);

    const res1 = await getCommentBy('id', result.insertId);

    await pool.query('insert into activity(users_id,comments_id,posts_id,description) values(?,?,?,?)', [userId, res1.id, posts_id, 'post']);

    return res1;
};

/**
 * Makes sql request to the database for getting
 * a comment of a single post
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {string} column = string for selecting the column from comment table 
 * @param {number} value = id of the comment
 * @return {object} => an object with 
 * the comment data
 */

const getCommentBy = async (column, value) => {
    const result = await pool.query(`SELECT  c.id, c.text, c.posts_id, c.post_date, c.users_id, u.username
    FROM comments as c
    JOIN users as u on u.id = c.users_id
    where c.isDeleted = 0 and c.${column} = ? `, [value]);

    return result[0];
};

/**
 * Makes sql request to the database for deleting
 * a comment of a single post
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {object} comment = object with the data of the comment 
 * @param {number} userId = id of the user  
 * @return {object} => an object with 
 * the comment details of a post
 */

const deleteComments = async (comment, userId) => {
    const { isDeleted, id, posts_id } = comment;

    const result = await pool.query('update comments set isDeleted = ? where id = ?', [isDeleted, id]);

    await pool.query('insert into activity(users_id,comments_id,posts_id,description) values(?,?,?,?)', [userId, id, posts_id, 'del']);

    return result;
};

/**
 * Makes sql request to the database for updating
 * a comment of a single post
 * @author Milen Petkov <milen.petkov.a29@learn.telerikacademy.com>
 * @param {object} comment = object with the data of the comment 
 * @param {number} userId = id of the user  
 * @return {object} => an object with updated
 * comment details of a post
 */

const update = async (comment, userId) => {
    const { id, text, posts_id } = comment;
    const date = new Date();
    const result = await pool.query(`update comments set
    text = ?,
    post_date =?
    where id =?`, [text, date, id]);

    const resultWithUpdatedDate = getCommentBy('id', result.insertId);

    await pool.query('insert into activity(users_id,comments_id,posts_id,description) values(?,?,?,?)', [userId, id, posts_id, 'put']);

    return resultWithUpdatedDate;
};

/**
 * Makes sql request to the database for like a single comment
 * and writes user action to the activity table in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} userId - the id of user who likes
 * @param {number} commentId - the id of the comment 
 * @param {number} postId - the id of post that comment is situated
 * @return {array} => the information about the 
 * likes in the database
 */

const createLike = async (userId, commentId, postId) => {

    const sql = `
    INSERT INTO
    likes(users_id,comments_id) values(?,?) `;

    const sql1 = `
    SELECT 
    * FROM 
    likes 
    WHERE 
    comments_id = ?`;

    const _ = await pool.query(sql, [userId, commentId]);

    await pool.query('insert into activity(users_id,comments_id,posts_id,description) values(?,?,?,?)', [userId, commentId, postId, 'like']);

    const result1 = await pool.query(sql1, [commentId]);
    return result1;

};

/**
 * Makes sql request to the database for dislike a single comment
 * and writes user action to the activity table in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} commentId - the id of the comment 
 * @param {number} userId - the id of user who dislikes
 * @param {number} postId - the id of post that comment is situated
 * @return {array} => the information about the 
 * likes in the database
 */

const deleteLike = async (commentId, userId, postId) => {

    const sql = `DELETE FROM
    likes
    WHERE comments_id = ?
    AND users_id = ?
    `;

    const sql1 = `
    SELECT 
    * FROM 
    likes 
    WHERE 
    comments_id = ?`;

    await pool.query('insert into activity(users_id,comments_id,posts_id,description) values(?,?,?,?)', [userId, commentId, postId, 'dislike']);

    const _ = await pool.query(sql, [commentId, userId]);

    const result1 = await pool.query(sql1, [commentId]);
    return result1;

};

/**
 * Makes sql request to the database for information about
 * all the users and likes of a single comment
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} commentId - the id of the comment 
 * @return {array} => the information about the 
 * likes and the users set likes of the comment
 */

const commentLikes = async (commentId) => {
    const sql = `SELECT l.id, l.isLiked,l.users_id, l.posts_id,l.comments_id,u.username
    FROM likes as l
    JOIN users as u on l.users_id = u.id
    WHERE l.comments_id = ?
    AND l.isLiked = 0`;

    const result = await pool.query(sql, [commentId]);

    return result;
};

export default {
    createComments,
    getPostComments,
    getCommentBy,
    deleteComments,
    update,
    createLike,
    deleteLike,
    commentLikes,
    allComments,
};