import mariadb from 'mariadb';

import dotenv from 'dotenv';

const config = dotenv.config().parsed;

const pool = mariadb.createPool({
    port: +config.DB_PORT,
    database: config.DATABASE,
    user: config.USER,
    password: config.PASSWORD,
    host: config.HOST,

});

export default pool;