
import pool from '../data/pool.js';

/**
 * A middleware that writes to the activity table in the database on user action
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {object} req - the request parameter 
 * @param {object} res - the response parameter
 * @param {function} next - function that activates the
 * next middleware
 */

export const getActivityMiddleware = async (req, res, next) => {

    const description = req.method;
    const fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    const commentOrPostId = +req.params.id;

    let sql = '';

    if (fullUrl[22] === 'p') {
        sql = `insert into 
        activity(users_id,posts_id, description) values (?,?,?)`;
        await pool.query(sql, [+req.user.id, commentOrPostId, description]);
    }
    else if (fullUrl[22] === 'c') {
        sql = `insert into 
        activity(users_id,posts_id, description) values (?,?,?)`;
        await pool.query(sql, [+req.user.id, commentOrPostId, description]);
    }
    next();
};

