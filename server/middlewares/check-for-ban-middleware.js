import { isBanned } from '../common/constants.js';

/**
 * A middleware that checks if the user is banned and
 * forbids user actions if the user is banned
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {object} req - the request parameter 
 * @param {object} res - the response parameter
 * @param {function} next - function that activates the
 * next middleware if user it not banned
 */

export const banCheckMiddleware = async (req, res, next) => {
    if (req.user.isBanned !== isBanned) {
        next();
    }
    else {
        res.status(403).json({
            message: `You are temporary banned for ${req.user.ban_reason}`
        });
    }
};
