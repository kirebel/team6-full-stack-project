import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import dotenv from 'dotenv';
import passport from 'passport';

import authController from './controllers/auth-controller.js';
import { postsController } from './controllers/posts-controller.js';
import usersController from './controllers/users-controller.js';
import { commentsController } from './controllers/comments-controller.js';
import errorMiddleware from './middlewares/error-middleware.js';
import jwtStrategy from './auth/strategy.js';
const app = express();

const config = dotenv.config().parsed;
const PORT = +config.PORT;

passport.use(jwtStrategy);
app.use(express.json());
app.use(cors());
app.use(helmet());
app.use(passport.initialize());

app.use('/posts', postsController);
app.use('/users', usersController);
app.use('/comments', commentsController);
app.use('/auth', authController);

app.use(errorMiddleware);

app.all('*', (req, res) =>
    res.status(400).json({ message: 'Resource not found!' })
);

app.listen(PORT, () => console.log(`App is listening ar port ${PORT}`));