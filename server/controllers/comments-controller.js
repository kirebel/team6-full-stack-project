import express from 'express';
import errors from '../services/service-errors.js';
import commentsService from '../services/comments-service.js';
import commentsData from '../data/comments-data.js';
import validateBody from '../middlewares/validate-body.js';
import createCommentScheme from '../validations/create-comment.scheme.js';
import updateCommentsScheme from '../validations/update-comment.scheme.js';
import { authMiddleware } from '../auth/auth.middleware.js';
import { banCheckMiddleware } from '../middlewares/check-for-ban-middleware.js';

export const commentsController = express.Router();

commentsController

    .get('/posts/:id', authMiddleware, async (req, res) => {
        const { id } = req.params;
        const comments = await commentsService.readPostComments(commentsData)(+id);
        res.status(200).json(comments);
    })
    .post('/posts/:id', authMiddleware, validateBody(createCommentScheme), async (req, res) => {
        const commentData = req.body;
        const id = +req.params.id;

        const userId = req.user.id;

        const { comment, error } = await commentsService.createPostComment(commentsData)(id, commentData, userId);
        if (error === errors.RECORD_NOT_FOUND) {
            res.status(404).json({ message: 'Resource not found!' });
        } else {
            res.status(200).json(comment);

        }

    })


    .get('/c_count', async (req, res) => {
        const result = await commentsService.getAllCommentsCount(commentsData)();
        res.status(200).json(result);
    })
    .get('/:id/likes', authMiddleware, async (req, res) => {

        const commentId = +req.params.id;

        const result = await commentsService.getCommentLikes(commentsData)(commentId);

        res.status(200).json(result);

    })
    //like comment
    .post('/:id/like/:postId', authMiddleware, async (req, res) => {

        const userId = +req.user.id;
        const commentId = +req.params.id;
        const postId = +req.params.postId;

        const result = await commentsService.createCommentLike(commentsData)(commentId, userId, postId);

        res.status(200).json(result);

    })

    .delete('/:id/dislike/:postId', authMiddleware, async (req, res) => {

        const userId = +req.user.id;
        const commentId = +req.params.id;
        const postId = +req.params.postId;

        const result = await commentsService.deleteCommentLike(commentsData)(commentId, userId, postId);

        res.status(200).json(result);

    })
    .delete('/:commentId/posts/:id', authMiddleware, banCheckMiddleware, async (req, res) => {
        const commentId = +req.params.commentId;

        const userId = req.user.id;
        const role = req.user.role;

        const { error, comment } = await commentsService.deletePostComment(commentsData)(+commentId, role, userId);

        if (error === errors.RECORD_NOT_FOUND) {
            res.status(404).json({ message: 'Record does not exist' });
        } else if (error === errors.UNALLOWED_ACTION) {
            res.status(405).json({ message: 'Unallowed action' });
        } else {
            res.status(201).json(comment);
        }
    })

    // eslint-disable-next-line consistent-return
    .put('/:commentId/posts/:id', authMiddleware, banCheckMiddleware, validateBody(updateCommentsScheme), async (req, res) => {

        const userId = req.user.id;
        const role = req.user.role;

        const id = +req.params.commentId;
        const postId = +req.params.id;

        const updateData = req.body;
        const { error, comment } = await commentsService.updateComment(commentsData)(id, updateData, role, userId, postId);

        if (error === errors.RECORD_NOT_FOUND) {
            res.status(400).json({ message: 'Record does not exist!' });
        } else if (error === errors.UNALLOWED_ACTION) {
            res.status(405).json({ message: 'Unallowed action' });
        } else {
            res.status(200).json(comment);
        }
    });
