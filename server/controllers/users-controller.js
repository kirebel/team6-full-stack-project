import express from 'express';
import usersService from '../services/user-service.js';
import usersData from '../data/users-data.js';
import errors from '../services/service-errors.js';
import { createUserSchema, updateUserSchema } from '../validations/user-validations.js';
import { authMiddleware, roleMiddleware } from '../auth/auth.middleware.js';
import { createValidator } from '../middlewares/user-creation-validator.js';
import userService from '../services/user-service.js';

const usersController = express.Router();

usersController
    // get all users with searching
    .get('/', authMiddleware, async (req, res) => {
        const { q } = req.query;
        const users = await usersService.getAllUsers(usersData)(q);
        res.status(200).send(users);
    })

    .get('/u_count', async (req, res) => {
        const result = await usersService.usersCount(usersData)();

        res.status(200).json(result);
    })

    .get('/f_count', async (req, res) => {
        const result = await usersService.friendshipsCount(usersData)();

        res.status(200).json(result);
    })

    //get comment activity
    .get('/:id/c_activity', authMiddleware, async (req, res) => {
        const id = +req.params.id;
        const activity = await userService.getUserCommentsActivity(usersData)(id);

        res.status(200).send(activity);
    })

    //get post activity
    .get('/:id/p_activity', authMiddleware, async (req, res) => {
        const id = +req.params.id;
        const activity = await userService.getUserPostsActivity(usersData)(id);

        res.status(200).send(activity);
    })

    //get users activity
    .get('/:id/u_activity', authMiddleware, async (req, res) => {
        const id = +req.params.id;

        const activity = await userService.getUserUsersActivity(usersData)(id);

        res.status(200).json(activity);
    })

    // get user by id    
    .get('/:id',
        authMiddleware,
        async (req, res) => {
            const { id } = req.params;

            const { error, user } = await usersService.getUserById(usersData)(+id);

            if (error === errors.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'User not found!' });
            } else {
                res.status(200).send(user);
            }
        })
    // create user
    .post('/',
        createValidator(createUserSchema),
        async (req, res,) => {
            const createData = req.body;

            const { error, user } = await usersService.createUser(usersData)(createData);
            if (error === errors.DUPLICATE_RECORD) {
                res.status(409).send({ message: 'Name not available' });
            } else {
                res.status(201).send(user);
            }
        })

    .get('/:id/liked_posts', authMiddleware, async (req, res) => {
        const userId = +req.params.id;

        const posts = await userService.getLikedPosts(usersData)(userId);

        res.status(200).json(posts);
    })

    .get('/:id/liked_comments', authMiddleware, async (req, res) => {
        const userId = + req.params.id;

        const comments = await userService.getLikedComments(usersData)(userId);

        res.status(200).json(comments);

    })
    // update user by id
    .put('/:id',
        authMiddleware,
        createValidator(updateUserSchema),
        async (req, res) => {
            const { id } = req.params;
            const updateData = req.body;

            const userId = req.user.id;
            const role = req.user.role;

            const { error, user } = await usersService.updateUser(usersData)(+id, updateData, role, userId);

            if (error === errors.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'User not found!' });
            } else if (error === errors.DUPLICATE_RECORD) {
                res.status(409).send({ message: 'Name not available' });
            } else if (error === errors.UNALLOWED_ACTION) {
                res.status(405).json({ message: 'Unallowed action' });
            } else {
                res.status(200).send(user);
            }
        })
    // delete user by id
    .delete('/:id', authMiddleware, roleMiddleware('admin'), async (req, res) => {
        const { id } = req.params;
        const { error, user } = await usersService.deleteUser(usersData)(+id);

        if (error === errors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'User not found!' });
        } else {
            res.status(200).send(user);
        }
    })

    .put('/:id/banstatus', authMiddleware, roleMiddleware('admin'), async (req, res) => {
        const id = +req.params.id;
        const banDescription = req.body;
        const userId = +req.user.id;
        const { error, user } = await usersService.banUser(usersData)(userId, id, banDescription);

        if (error === errors.RECORD_NOT_FOUND) {
            res.status(404).json({ message: 'User not found!' });
        } else {
            res.status(200).json(user);
        }
    })

    .delete('/:id/banstatus', authMiddleware, roleMiddleware('admin'), async (req, res) => {
        const id = +req.params.id;
        const userId = +req.user.id;

        const { error, user } = await userService.unbanUser(usersData)(userId, id);

        if (error === errors.RECORD_NOT_FOUND) {
            res.status(400).json({ message: 'User not found!' });
        } else {
            res.status(200).json(user);
        }
    })

    //send friend request 
    .post('/:id/request_friendship', authMiddleware, async (req, res) => {
        const secondId = +req.params.id;
        const requestId = +req.user.id;

        const request = await userService.requestFriendShip(usersData)(requestId, secondId);

        res.status(200).json(request);

    })

    //accept friendship request
    .put('/:id/accept_friendship', authMiddleware, async (req, res) => {
        const acceptId = +req.user.id;
        const otherId = +req.params.id;

        const accepted = await userService.acceptFriendShip(usersData)(otherId, acceptId);

        res.status(200).json(accepted);

    })

    //unfriend
    .delete('/:id/unfriend', authMiddleware, async (req, res) => {
        const firstId = +req.user.id;
        const secondId = +req.params.id;

        const deleted = await userService.deleteFriendship(usersData)(secondId, firstId);

        res.status(200).json(deleted);
    })

    //get friendship information
    .get('/friendship/info', authMiddleware, async (req, res) => {

        const result = await userService.getFriendshipInfo(usersData)();

        res.status(200).json(result);

    });

export default usersController;
