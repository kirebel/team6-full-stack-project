import express from 'express';

import errors from '../services/service-errors.js';
import postsData from '../data/posts-data.js';
import postsService from '../services/posts-service.js';

import createPostScheme from '../validations/create-post.scheme.js';
import updatePostScheme from '../validations/update-post.scheme.js';

import { banCheckMiddleware } from '../middlewares/check-for-ban-middleware.js';
import validateBody from '../middlewares/validate-body.js';
import { authMiddleware } from '../auth/auth.middleware.js';
import { getActivityMiddleware } from '../middlewares/activity-middleware.js';

export const postsController = express.Router();

postsController

    .get('/', authMiddleware, async (req, res) => {

        const userId = +req.user.id;

        const { q } = req.query;
        const posts = await postsService.getPosts(postsData)(q, userId);
        res.status(200).json(posts);
    })

    .get('/last', async (req, res) => {
        const lastPosts = await postsService.getLastFivePosts(postsData)();
        res.status(200).json(lastPosts);
    })

    .get('/p_count', async (req, res) => {
        const count = await postsService.allPosts(postsData)();
        res.status(200).json(count);
    })

    .get('/l_count', async (req, res) => {
        const count = await postsService.allLikes(postsData)();
        res.status(200).json(count);
    })

    .get('/most_liked', authMiddleware, async (req, res) => {
        const posts = await postsService.mostLiked(postsData)();

        res.status(200).json(posts);
    })

    .get('/most_commented', authMiddleware, async (req, res) => {
        const posts = await postsService.mostCommented(postsData)();

        res.status(200).json(posts);
    })

    .get('/:id/likes', authMiddleware, async (req, res) => {
        const postId = +req.params.id;

        const postLike = await postsService.getPostsLikes(postsData)(postId);
        res.status(200).json(postLike);
    })
    //get all flagged posts

    .get('/flagged', authMiddleware, async (req, res) => {
        const posts = await postsService.getFlaggedPosts(postsData)();
        res.status(200).json(posts);
    })

    .get('/:id', authMiddleware, getActivityMiddleware, async (req, res) => {

        const userId = req.user.id;

        const { id } = req.params;

        const { error, post } = await postsService.getSinglePostById(postsData)(+id, +userId);

        if (error === errors.RECORD_NOT_FOUND) {
            res.status(404).json({ message: 'Post does not exist!' });
        } else {
            res.status(200).json(post);
        }
    })

    .post('/', authMiddleware, banCheckMiddleware, validateBody(createPostScheme), async (req, res) => {
        const postData = req.body;

        const createdPost = await postsService.createPost(postsData)(postData, req.user.id);

        res.status(201).json(createdPost);

    })

    .put('/:id/like', authMiddleware, async (req, res) => {
        const userId = +req.user.id;
        const postId = +req.params.id;

        const liked = await postsService.createPostLike(postsData)(userId, postId);

        res.status(201).json(liked);
    })

    .put('/:id', authMiddleware, banCheckMiddleware, validateBody(updatePostScheme), async (req, res) => {
        const updateData = req.body;
        const { id } = req.params;

        const userId = req.user.id;
        const role = req.user.role;

        const { error, post } = await postsService.updatePost(postsData)(+id, updateData, role, userId);

        if (error === errors.RECORD_NOT_FOUND) {
            res.status(404).json({ message: 'Post does not exist!' });
        } else if (error === errors.UNALLOWED_ACTION) {
            res.status(405).json({ message: 'Unallowed action' });
        } else {
            res.status(200).json(post);
        }
    })

    .delete('/:id/dislike', authMiddleware, async (req, res) => {
        const userId = +req.user.id;
        const postId = +req.params.id;

        const deletedLike = await postsService.deletePostLike(postsData)(userId, postId);

        res.status(201).json(deletedLike);
    })

    .delete('/:id', authMiddleware, banCheckMiddleware, async (req, res) => {
        const { id } = req.params;

        const userId = req.user.id;
        const role = req.user.role;

        const { error, post } = await postsService.deletePost(postsData)(+id, userId, role);

        if (error === errors.RECORD_NOT_FOUND) {
            res.status(404).json({ message: 'Post does not exist!' });
        } else if (error === errors.UNALLOWED_ACTION) {
            res.status(405).json({ message: 'Unallowed action' });
        } else {
            res.status(200).json(post);
        }
    })

    //lock post
    .put('/:id/lock', authMiddleware, async (req, res) => {

        const id = +req.params.id;
        const userId = +req.user.id;
        const result = await postsService.lockPost(postsData)(userId, id);

        res.status(200).json(result);

    })

    //unlock post
    .put('/:id/unlock', authMiddleware, async (req, res) => {

        const id = +req.params.id;
        const userId = +req.user.id;
        const result = await postsService.unLockPost(postsData)(userId, id);

        res.status(200).json(result);

    })

    //flag post
    .put('/:id/flag', authMiddleware, async (req, res) => {

        const id = +req.params.id;
        const userId = +req.user.id;
        const result = await postsService.flagPost(postsData)(userId, id);

        res.status(200).json(result);

    })

    //unFlagPost
    .put('/:id/unflag', authMiddleware, async (req, res) => {

        const id = +req.params.id;
        const userId = +req.user.id;
        const result = await postsService.unFlagPost(postsData)(userId, id);

        res.status(200).json(result);

    });
