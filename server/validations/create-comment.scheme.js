const createCommentScheme = {
    text: value => typeof value === 'string' && value.length <= 1000 && value.length > 0,
};

export default createCommentScheme;