const updatePostScheme = {
    title: value => typeof value === 'string' && value.length <= 250 && value.length > 0,
    text: value => typeof value === 'string' && value.length <= 1000 && value.length > 0,
};

export default updatePostScheme;