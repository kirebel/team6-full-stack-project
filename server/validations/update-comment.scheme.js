const updateCommentsScheme = {
    text: value => typeof value === 'string' && value.length <= 1500 && value.length > 0,

};

export default updateCommentsScheme;