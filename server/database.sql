CREATE DATABASE  IF NOT EXISTS `forum_database` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `forum_database`;
-- MariaDB dump 10.19  Distrib 10.5.10-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: forum_database
-- ------------------------------------------------------
-- Server version	10.5.10-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `comments_id` int(11) DEFAULT NULL,
  `posts_id` int(11) DEFAULT NULL,
  `description` varchar(45) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `user2_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_activity_users1_idx` (`users_id`),
  KEY `fk_activity_comments1_idx` (`comments_id`),
  KEY `fk_activity_posts1_idx` (`posts_id`),
  CONSTRAINT `fk_activity_comments1` FOREIGN KEY (`comments_id`) REFERENCES `comments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_activity_posts1` FOREIGN KEY (`posts_id`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_activity_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1582 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity`
--

LOCK TABLES `activity` WRITE;
/*!40000 ALTER TABLE `activity` DISABLE KEYS */;
/*!40000 ALTER TABLE `activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(750) NOT NULL,
  `post_date` datetime NOT NULL DEFAULT current_timestamp(),
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  `users_id` int(11) NOT NULL,
  `posts_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comments_posts1_idx` (`posts_id`),
  KEY `fk_comments_users1_idx` (`users_id`),
  CONSTRAINT `fk_comments_posts1` FOREIGN KEY (`posts_id`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,'i dont know how to resolve the problem','2021-07-23 14:55:32',1,5,102),(2,'ss','2021-07-17 16:38:49',1,5,103),(27,'komentarasasdf','2021-07-21 12:54:33',1,5,103),(28,'kfsdf','2021-07-21 10:38:27',1,5,103),(29,'Brczx\n','2021-07-19 02:21:30',1,5,102),(30,'aaaaaaaaaaaaaa','2021-07-19 02:05:49',1,5,102),(31,'ffsdsfdsf','2021-07-19 10:04:40',1,5,102),(32,'ffsdsfdsf','2021-07-19 10:04:52',1,5,102),(33,'i really dont know how to fix this problem, but i know css is a fkin shit, i hate it','2021-07-24 17:46:35',1,5,105),(34,'kirekire','2021-07-21 10:38:25',1,5,103),(35,'you can','2021-07-23 14:15:16',1,5,104),(36,'drfdfg','2021-07-24 16:54:19',1,5,103),(37,'I dont know how to help neither but i also agree that css is a fkin shit!','2021-07-24 21:19:43',1,5,105),(38,'dfgdfgdfg','2021-07-24 21:23:56',1,5,105),(39,'f','2021-07-24 21:26:01',1,5,105),(40,'fdggf','2021-07-24 21:26:28',1,5,105),(41,'Tell my world around','2021-07-24 21:26:48',1,5,105),(42,'Tell my world around','2021-07-24 21:26:57',1,5,105),(43,'Tell my world around','2021-07-24 21:27:02',1,5,105),(44,'Tell my world around','2021-07-24 21:27:06',1,5,105),(45,'dfefsdfgd','2021-07-24 21:27:29',1,5,105),(46,'dfef','2021-07-24 21:27:37',1,5,105),(47,'dfef','2021-07-24 21:28:11',1,5,105),(48,'g','2021-07-24 21:29:50',1,5,105),(49,'g','2021-07-24 21:29:54',1,5,105),(50,'Comment','2021-07-24 21:32:03',1,5,105),(51,'TECHNOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO','2021-07-24 21:32:44',1,5,105),(52,'sdfsdfsdfdvfdfvfvd','2021-07-24 21:46:51',1,5,103),(53,'nbcgnghn','2021-07-24 21:46:54',1,5,103),(54,'sdfsdfsdfsdf','2021-07-25 10:05:21',1,5,103),(55,'sd','2021-07-25 17:59:10',1,5,103),(56,'tuka e miromiro','2021-07-25 10:18:02',1,40,103),(57,'gfhdjjdghjdgh','2021-07-25 10:43:25',1,40,115),(58,'dfgdsfgdsfgdsfg','2021-07-25 11:15:00',1,40,115),(59,'dgsg','2021-07-25 11:25:32',1,40,115),(60,'sdgdgdf','2021-07-25 14:50:19',1,40,115),(61,'sdfsdf','2021-07-25 16:34:08',1,5,103),(62,'wdqwdq','2021-07-25 18:01:54',1,5,103),(63,'hasmenchi','2021-07-25 20:41:56',1,5,103),(64,'gegggfgh','2021-07-25 20:43:05',1,5,103),(65,'dfgdfg','2021-07-25 20:47:09',1,5,103),(66,'dfsdfdsgdfg','2021-07-25 21:31:55',1,5,105),(67,'dfgdfgdfg','2021-07-25 21:32:03',1,5,105),(68,'ertertert','2021-07-26 06:42:17',1,5,105),(69,'ertertert','2021-07-26 06:42:45',1,5,105),(70,'dfgdfgdfgdgf','2021-07-26 06:42:54',1,5,105),(71,'dfgdfgdfgdgf','2021-07-26 06:43:04',1,5,105),(72,'sdfsdfsdf','2021-07-26 06:43:11',1,5,105),(73,'dgrdfgdf','2021-07-26 06:52:04',1,5,111),(74,'dfgdfgdfg','2021-07-26 06:52:06',1,5,111),(75,'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee','2021-07-26 06:52:10',1,5,111),(76,'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa','2021-07-26 06:55:27',1,5,111),(77,'ledena kralica','2021-07-26 06:59:29',1,5,111),(78,'sdfsddfgdfgdfgfsd','2021-07-28 17:39:33',1,5,115),(79,'dssvfddfv','2021-07-26 07:13:42',1,5,115),(80,'sffgdfgdfgdfg','2021-07-26 07:14:04',1,5,115),(81,'ffffffffffffffffffffffff','2021-07-26 07:17:09',1,5,115),(82,'riders of the storm','2021-07-26 07:19:43',1,5,111),(83,'dsfsdfdsf','2021-07-26 07:22:48',1,5,111),(84,'dfgdfg','2021-07-26 07:23:24',1,5,111),(85,'dfgdfg','2021-07-26 07:23:50',1,5,111),(86,'dfgdfgdfgdfg','2021-07-26 07:24:13',1,5,111),(87,'eseeeeeeeeeeeeeeeee','2021-07-26 07:25:32',1,5,111),(88,'sdfsdfsdf','2021-07-26 07:25:46',1,5,111),(89,'sdfsdfsdf','2021-07-26 07:26:12',1,5,111),(90,'sdfsdf','2021-07-26 07:27:09',1,5,111),(91,'ertertert','2021-07-26 07:28:36',1,5,111),(92,'kireet','2021-07-26 07:32:38',1,5,111),(93,'sdfgdfgdfgdfgdfgdf','2021-07-26 07:36:25',1,5,104),(94,'sdfsdfsdf','2021-07-26 09:05:22',1,5,115),(95,'miro miro','2021-07-26 09:36:36',1,40,105),(96,'miro mir','2021-07-26 09:37:14',1,40,123),(97,'sdfsdfsdf','2021-07-26 09:45:40',1,40,115),(98,'raboti aktivnosttaa qko','2021-07-26 14:04:51',1,40,111),(99,'edited comment','2021-07-26 15:00:58',1,5,105),(100,'sdfsdf','2021-07-28 14:34:38',1,41,111),(101,'dfgdfg','2021-07-28 21:29:06',1,41,115),(102,'vdfvvdfv','2021-07-29 13:25:21',1,41,112),(103,'dsfdfg','2021-07-29 16:19:00',1,5,112),(104,'ssfdfdfg','2021-07-30 06:30:57',1,5,130),(105,'dfgdfgdfg','2021-07-30 07:41:27',1,5,131),(106,'eerrgrtg','2021-07-30 07:54:41',1,5,129),(107,'dsfsdfsdfsdf','2021-07-30 09:51:16',1,5,132),(108,'sdfsdfdsfdsdfsdf','2021-07-30 14:12:21',1,44,133),(109,'sdfsdf','2021-07-30 14:12:55',1,44,134),(110,'Благодарим за запитването и интереса към Телерик Академия. Курсът с QA се провежда веднъж годишно, като тазгодишното издание на програмата тече в момента. Не бихме могли отсега да потвърдим дати за следваща отворена приемна кампания през 2022 г.','2021-07-30 15:10:29',0,45,136),(111,'Здравейте,\nбих искал да изясним позволено ли е използването на допълнителни UI библиотеки като bootstrap за сегашния проект, евентуални препоръки за такива и ако остане време, малък пример за използването им. Благодаря!','2021-07-30 16:36:24',0,40,137),(112,'Би било супер, ако може да разгледаме как да вържем frontend & backend - така че да работим със заявки от сървъра и данни от базата. Ако още е рано (IMO на мен ми е рано : ), как можем да действаме сега, така че да градим frontend-a малко по малко?','2021-07-30 16:57:18',0,45,137),(113,'Здравей,\nмен все още ме обърква рендерирането на различни компоненти при кликането на даден бутон,\nако може пак да го обясним.\nБлагодаря!','2021-07-30 17:10:14',0,46,137),(114,'dfgdfgdfg','2021-07-30 18:32:52',1,5,138),(115,'dfgdfgdfgdf','2021-07-30 18:32:56',1,5,138),(116,'dfgdfgdfg','2021-07-30 18:32:59',1,5,138),(117,'sdfsdf','2021-07-30 19:22:49',1,5,137),(118,'fgdfgdfgdfg','2021-07-30 19:23:24',1,5,137),(119,'komentar','2021-07-30 19:25:31',1,5,137),(120,'ertert','2021-07-30 19:28:09',1,5,140),(121,'dfgdfgdf','2021-07-31 05:31:57',1,5,142),(122,'dgdgdfgdf','2021-07-31 09:17:01',1,40,144),(123,'dfgdfgdfgdfg','2021-07-31 09:17:05',1,40,144),(124,'dfgdfgdfgdfg','2021-07-31 09:17:09',1,40,144),(125,'dfgdfgdfgdfgdfg','2021-07-31 09:17:13',1,40,144),(126,'dfgdfgdfgdfg','2021-07-31 09:20:55',1,40,146),(127,'dfgdfgdfgdfg','2021-07-31 09:20:59',1,40,146),(128,'dfgdfgdfgdfg','2021-07-31 09:21:02',1,40,146),(129,'dfgdfgdfgdfg','2021-07-31 09:21:06',1,40,146),(130,'dfgdfgdfgdfg','2021-07-31 09:21:14',1,40,146),(131,'dfgdfgdfg','2021-07-31 16:56:22',1,5,150),(132,'dfgdfgdfgdfg','2021-07-31 16:56:25',1,5,150),(133,'dfgdfgdfgdfg','2021-07-31 16:56:28',1,5,150),(134,'gfgdfdfg','2021-08-01 06:06:59',1,40,137),(135,'I agree with that','2021-08-01 08:20:08',1,5,148),(136,'Здравейте, системата вече е достъпна. Моля да ни извините за причиненото неудобство.','2021-08-01 15:23:07',0,5,152),(137,'Здравей, аз съм с Firefox и с него продължава да не работи. Пробвах с Opera, с нея работи. Не съм почитател на Chrome, който предполагам повече хора ползват.','2021-08-01 15:23:47',0,49,152),(138,'Здравей, може ли да пробваш с инкогнито във Firefox?','2021-08-01 15:24:18',0,5,152),(139,'Здравей, да, още вчера го направих по случайност и стана…Изтрих си cookie-тата след това и започна да зарежда и в нормален tab.','2021-08-01 15:24:38',0,49,152),(140,'Здравей! Това, което виждам на пръв поглед е, че първият ти for-цикъл е от 0 до n включително, махни “=” там, защото така ще чака да въведеш още един масив. Според мен също последният for-цикъл с j ти е излишен, може да го махнеш и директно в if-проверката да проверяваш дали numbers[k] > numbers[k + 1].','2021-08-01 15:28:02',0,40,153),(141,'Здравей,\nзапазвай си някъде резултат, който да печаташ накрая, защото твоят код печата резултат след всяко завъртане на цикъла.\n\nЗа да избегнеш печатането веднага след първия ред инпут, добави това след ред 6:\nint n = Integer.parseInt(scanner.next());\nscanner.nextLine();\n\nbtw това не ти трябва - if (n>=1&&n<=10) просто по условие n ще е между 1 и 10 и няма нужда да го проверяваш изрично:)\n\nУспех!','2021-08-01 15:28:38',0,45,153),(142,'Мерси много, това помогна! ','2021-08-01 15:30:08',0,49,153),(143,'Привет,\n\nМоже ли да поговорим малко за Git commits - съвети / branching?','2021-08-01 15:39:29',0,5,155),(144,'Здравей,\n\nможе ли да обясниш разликата между криптиране, хеширане и кодиране на данни, тъй като малко ме объркват термините?\n\nХубав ден!','2021-08-01 15:40:09',0,41,155),(145,'Здравей,\nа ще може ли цялата схема по вписване LogIn да я обясниш отново?\n\nБлагодаря!\n\nЗдравей,\nа ще може ли цялата схема по вписване LogIn да я обясниш отново?\n\nБлагодаря!\n\n','2021-08-01 15:40:47',0,47,155),(146,'Аз лично бих искал да обърнем малко повече внимание на function(method) overriding понеже леееко ме забърква. И по-конкретно някакъв по-дълбок пример ако примерно имаме parent class , child class и child-a евентуално да си има негови дечица и примерно метода в базовия клас е празен или не прави нищо , но се вика от него , а този в детето прави едно , а неговите деца друго или просто някакъв mindblowning пример, че да го хвана напълно.','2021-08-01 15:43:36',0,5,156),(147,'Здравейте,\nбих се радвал да разгледаме още примери с композиране - вместо използване на унаследяване.\nБлагодаря предварително.','2021-08-01 15:44:16',0,47,156),(148,'Ще е супер да разгледаме в какви случаи / за какви цели ползваме съответно static, private & public field / method - т.е. как да изберем кое ни трябва.\n\n','2021-08-01 15:45:08',0,40,156),(149,'Мисля, че просто някои дни не са отчитани. Така няма как да имаш отсъствие или присъствие на тях. Но пък на тези, в които се е отчитало, си присъствал и затова имаш 100% присъственост.','2021-08-01 15:47:24',0,5,157),(150,'Наистина нещо не е наред там, ще проуча какво е станало и ще пиша за още инфо.','2021-08-01 15:51:30',0,45,157);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `friends`
--

DROP TABLE IF EXISTS `friends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friendship` tinyint(1) NOT NULL DEFAULT 0,
  `users_id` int(11) NOT NULL,
  `users_id1` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_friends_users1_idx` (`users_id`),
  KEY `fk_friends_users2_idx` (`users_id1`),
  CONSTRAINT `fk_friends_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_friends_users2` FOREIGN KEY (`users_id1`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friends`
--

LOCK TABLES `friends` WRITE;
/*!40000 ALTER TABLE `friends` DISABLE KEYS */;
INSERT INTO `friends` VALUES (146,0,5,21),(147,0,5,22),(148,0,5,23),(150,0,5,1),(152,1,40,41),(157,1,41,5),(161,0,45,41),(169,1,47,5),(170,1,47,45),(171,0,47,40),(172,1,45,5),(173,1,5,2),(174,0,50,5),(175,0,50,45);
/*!40000 ALTER TABLE `friends` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isLiked` tinyint(1) NOT NULL DEFAULT 0,
  `posts_id` int(11) DEFAULT NULL,
  `users_id` int(11) NOT NULL,
  `comments_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_likes_posts1_idx` (`posts_id`),
  KEY `fk_likes_users1_idx` (`users_id`),
  KEY `fk_likes_comments1_idx` (`comments_id`),
  CONSTRAINT `fk_likes_comments1` FOREIGN KEY (`comments_id`) REFERENCES `comments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_likes_posts1` FOREIGN KEY (`posts_id`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_likes_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=605 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likes`
--

LOCK TABLES `likes` WRITE;
/*!40000 ALTER TABLE `likes` DISABLE KEYS */;
INSERT INTO `likes` VALUES (511,0,NULL,5,120),(514,0,NULL,5,112),(515,0,NULL,5,113),(518,0,NULL,5,110),(521,0,NULL,40,120),(524,0,NULL,40,112),(525,0,NULL,40,113),(526,0,136,40,NULL),(527,0,NULL,40,110),(529,0,NULL,5,111),(531,0,NULL,41,113),(532,0,NULL,41,112),(533,0,NULL,41,111),(545,0,NULL,47,111),(546,0,NULL,47,112),(547,0,NULL,47,113),(549,0,136,5,NULL),(553,0,NULL,5,135),(556,0,137,45,NULL),(557,0,136,45,NULL),(558,0,137,41,NULL),(559,0,136,41,NULL),(560,0,NULL,41,110),(561,0,137,47,NULL),(562,0,136,47,NULL),(563,0,NULL,47,110),(564,0,152,47,NULL),(565,0,152,5,NULL),(566,0,152,49,NULL),(567,0,NULL,49,136),(568,0,NULL,5,137),(569,0,NULL,49,138),(570,0,153,40,NULL),(571,0,153,45,NULL),(572,0,NULL,45,140),(573,0,153,49,NULL),(574,0,155,5,NULL),(575,0,155,41,NULL),(576,0,NULL,41,143),(577,0,NULL,41,144),(578,0,NULL,47,145),(579,0,NULL,47,144),(580,0,NULL,47,143),(581,0,155,47,NULL),(582,0,NULL,5,146),(583,0,156,5,NULL),(584,0,NULL,40,146),(585,0,156,40,NULL),(586,0,NULL,40,147),(587,0,NULL,40,148),(588,0,NULL,45,149),(589,0,NULL,45,150),(590,0,157,45,NULL),(591,0,157,5,NULL),(592,0,NULL,5,150),(593,0,NULL,5,148),(594,0,NULL,5,144),(595,0,NULL,5,140),(596,0,NULL,5,142),(597,0,NULL,5,139),(598,0,137,5,NULL),(599,0,157,40,NULL),(600,0,NULL,40,150),(601,0,NULL,40,143),(602,0,NULL,40,145),(603,0,NULL,40,141),(604,0,137,40,NULL);
/*!40000 ALTER TABLE `likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `text` varchar(1000) NOT NULL,
  `post_date` datetime NOT NULL DEFAULT current_timestamp(),
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  `users_id` int(11) NOT NULL,
  `isLocked` tinyint(1) NOT NULL DEFAULT 0,
  `isFlagged` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_posts_users1_idx` (`users_id`),
  CONSTRAINT `fk_posts_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=158 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (102,'ZAglavie','tema','2021-07-24 09:24:39',1,5,0,0),(103,'sdfsdfsdfdfgdfg','sdfdsfsdfsdfsd','2021-07-25 20:49:11',1,5,0,0),(104,'You dsdfsdsfddfgdfg','me nesdfsdf','2021-07-26 07:36:33',1,5,0,0),(105,'Invalid  not return','I have a big problem with one of your tasks. The judge system really annoys me!Its really slow!','2021-07-26 15:00:46',1,5,0,0),(106,'fgbfxg','fgbfgb','2021-07-25 09:09:00',1,5,0,0),(107,'dsfsdf','dsfgdsfg','2021-07-25 09:24:18',1,5,0,0),(108,'sdfsadf','dfgdsfg','2021-07-25 09:32:31',1,5,0,0),(109,'dgdfgd','dfgdfg','2021-07-25 09:34:23',1,5,0,0),(110,'dsfsdf','dfdfg','2021-07-25 09:37:06',1,5,0,0),(111,'The problem is that modals are very sdfsdfvery COOL','BUT CSS IS dfgdfgdgfg','2021-07-28 17:39:27',1,5,0,0),(112,'dsfsdfsdfdfgdfg','sdfsdfsdf','2021-07-29 16:18:54',1,5,0,0),(113,'efdfgdf','sdfgdfg','2021-07-25 10:04:46',1,5,0,0),(114,'sdfsdf','sdfsdfds','2021-07-25 10:06:00',1,5,0,0),(115,'proba za aktivnost','proba za aktivndfgdfgdfgost','2021-07-28 17:39:36',1,40,0,0),(116,'dfgdfgdsfg','dsfgsdfgdsf','2021-07-25 17:35:57',1,5,0,0),(117,'sddfsd','sdfsdf','2021-07-25 17:55:40',1,5,0,0),(118,'dsdfsdf','sdfsdf','2021-07-25 17:55:47',1,5,0,0),(119,'sdfsdf','sdfsdfffff','2021-07-26 07:34:52',1,5,0,0),(120,'sdfsdf','sdfsdf','2021-07-25 21:10:10',1,5,0,0),(121,'sdsdfdsf','sdfsdfsdf','2021-07-25 21:12:57',1,5,0,0),(122,'sdfsdfsdf','sdfsdfsdf','2021-07-26 07:37:01',1,5,0,0),(123,'MIROOOOOOOOOO','miro prai post','2021-07-26 09:34:56',1,40,0,0),(124,'fgddgdfg','dfgdfgdf','2021-07-26 09:39:08',1,40,0,0),(125,'lqlqlqlql','lqlqlqlq','2021-07-26 15:00:34',1,5,0,0),(126,'ghrhdgh','fdghdfghdfgh','2021-07-26 15:33:18',1,5,0,0),(127,'dfgdfgdfg','dfgdfgdfgdfd','2021-07-29 15:32:47',1,41,0,0),(128,'reversed posts','reversed posts','2021-07-30 05:57:43',1,5,0,0),(129,'new post','new post','2021-07-30 05:59:43',1,5,0,0),(130,'sdfsdfsdf','sdfsdfsfd','2021-07-30 06:01:38',1,5,0,0),(131,'sorted postsdfdfg','sorted posts','2021-07-30 09:49:56',1,5,0,0),(132,'sdfsdfsd','sdfsdfsdf','2021-07-30 09:50:05',1,5,0,0),(133,'sdsfgdfgdf','dfggdfg','2021-07-30 10:38:42',1,41,0,1),(134,'sdfsdfsdfsdf','sdfsdfsdsdfsdf','2021-07-30 14:12:51',1,44,0,1),(135,'babaiiiiiii','tatkoiiiiii','2021-07-30 14:18:53',1,44,0,0),(136,'Информация за alpha QA курса','Здравейте, да попитам понеже на сайта няма обявена начална дата дали бихте могли да ми кажете приблизително след колко месеца се очаква да започне QA курсът (понеже приятелката ми иска да се запише)?','2021-07-30 16:22:19',0,5,0,0),(137,'Q&A Session - July 13','Привет, @A29-JS.\n\nКакто говорихме днес, утре ще проведем Q&A сесия с основен фокус отборния проект, но може да действаме и с въпроси, свързани с реакт и експрес (каквито е логично да има). Може да задавате въпроси тук.','2021-07-30 19:25:52',0,5,0,0),(138,'fdgdfgdfg','dfgdfgdfgdfg','2021-07-30 18:32:49',1,5,0,0),(139,'fgdfgdfg','dfgdfgdfg','2021-07-30 19:27:31',1,5,0,0),(140,'ertgerter','erterter','2021-07-30 19:28:05',1,5,0,0),(141,'fghfghfgh','fghfghfgh','2021-07-30 20:57:09',1,45,0,0),(142,'dfgsdfgsdfg','sdfgsdfgdsf','2021-07-30 21:22:42',1,41,0,0),(143,'dfgdfgdfg','dfgdfgdfg','2021-07-31 07:57:21',1,41,0,1),(144,'dfgdfgdfg','dfgdfgdfg','2021-07-31 08:55:18',1,5,0,0),(145,'sdfsdf','sdfsdf','2021-07-31 09:07:20',1,40,0,0),(146,'dfgdfgdfg','dfgdfgdfg','2021-07-31 09:20:52',1,40,0,0),(147,'sdfsdfsdf','sdfsdfsdf','2021-07-31 09:23:24',1,40,0,0),(148,'Breakfast is the most important meal for the day','kireee','2021-07-31 12:10:31',1,5,0,0),(149,'fdfgdfgdfgdfgdf','dfgdfgdfgdfgdfgdfg','2021-07-31 15:53:45',1,5,0,0),(150,'dfgdfgdfgddfgdfg','fgfdfgdfgdfgdfgdfg','2021-07-31 16:49:55',1,5,0,0),(151,'dfgdfgdfgdfgdf','dfgdfgdfgdfgdfgdfg','2021-08-01 05:57:26',1,40,0,0),(152,'Learn System Down','Здравейте,\n\nОт известно време Learn системата хвърля грешка 503 Service Temporarily Unavailable. Може ли някой от екипа да погледне? Благодаря предвително.','2021-08-01 15:21:21',0,47,0,0),(153,'Is a List Sorted','Здравейте,\nрешавам задачата \" Is a List Sorted?\" и логиката функционира. Проблема, който имам, е че при въвеждането на “N” ми излиза един път “true” без да съм въвел първия лист от номера.\nЕто го и кодът ми: https://pastebin.com/acAH5FVF .\nНякой има ли идея защо се появява този пробле и как да го реша?\nМерси предварително!','2021-08-01 15:26:36',0,49,0,0),(154,'Cognitive Test Result','Кога и как ще може да си проверим резултатите от когнитивния тест?','2021-08-01 15:37:14',1,45,0,0),(155,'Q&A Session - Backend Technologies (July 2)','Привет, \n\nутре ще проведем сесия с въпроси и отговори, като тема са бекенд технологиите, с които се сблъскахме последните 2.5 седмици. Та - за да има отговори, първо трябва да има въпроси, които може да задавате направо тук.','2021-08-01 15:38:54',0,45,0,0),(156,'Q&A Session - OOP - 14.05','Здравейте, \n\nтози петък ще решим заедно Workshop II, но ще ни остане достатъчно време да минем и през други теми, въпроси и проблеми, които ви вълнуват. Отварям тази тема, за да добавите всичко, което искате да изговорим в петък. Можете да питате въпроси, да добавят код и примери, които ви интересуват и искате да обсъдим.','2021-08-01 15:43:13',0,50,0,0),(157,'Problem with the attendance tracker','Здравейте всички.Имам въпрос относно присъствията и отсъствията ни. Онзи ден, понеже стоях само половината от лекцията, понеже трябваше да седим по buddy групите в разговори и ми хрумна да си проверя дали все пак имам някакви отсъствия все пак, защото досега не съм отсъствал и забелязах, че от 5-ти април насам нямам нито присъствия, нито отсъствия, а под всичко, пише че за дадената сесия имам 100% присъствия, нищо, че сме 6-ти, 7-ми(тоест че все едно 5-ти април още не е дошъл).','2021-08-01 15:52:41',0,48,0,0);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_UNIQUE` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'admin'),(2,'user');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(500) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  `isBanned` tinyint(1) NOT NULL DEFAULT 0,
  `ban_reason` varchar(500) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `reg_date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `fk_users_role1_idx` (`role_id`),
  CONSTRAINT `fk_users_role1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'rado','kire',1,0,NULL,1,'2021-07-19 02:30:27'),(2,'duster99','milen',0,0,NULL,1,'2021-07-19 02:30:27'),(3,'mrusnik','$2b$10$ddpb5UjPe9vNoEaJQY8Di.bncLvZowxTQKelJbZqgEqKi9jqCLr16',1,0,NULL,1,'2021-07-19 02:30:27'),(5,'kirebel','$2b$10$GP.d3w/q4yWoOoYPNdxQWOUVTJC5tR.MCH1yzErdoXmWOpXJtXoTW',0,0,NULL,1,'2021-07-19 02:30:27'),(21,'fff','$2b$10$hYjvV/SZNUuJcfoYtM4qUOSmyuoFa9iCuGnNDS2hxx5fr4xeWRyLq',1,0,'',2,'2021-07-19 02:30:27'),(22,'sssedr','$2b$10$wk7w71gpn5G8YsaYakal8uME8/XssS.Fnl1MR0RrrR0E4lANmjE4C',1,0,NULL,2,'2021-07-19 02:30:27'),(23,'verbal abuse','$2b$10$f0TNaJHDgM1WrlPDt9V6S.F4EF7SExAEcXxFfXkDF7qnWa1hPSdYO',1,0,NULL,2,'2021-07-19 02:30:27'),(24,'verbalAbuse','$2b$10$YlY4Ra1NE1.SQ58a6RXykuHXFcGMu39r7RVMCCMHXkpfjXKvBffHG',1,0,NULL,2,'2021-07-19 02:30:27'),(40,'miro88','$2b$10$VaKQk9NV3eNe61sVv5b8Dut9nIuhpVv/hu146Sen79J8r3GOjjasC',0,0,'verbal abuse',2,'2021-07-25 10:17:43'),(41,'a_iliev88','$2b$10$sNbWGtbEsm7v97YYShSZQuk86WCmJU2E31/d6nSazYJwYDvrRbgIG',0,0,NULL,2,'2021-07-28 14:34:18'),(42,'hasan','$2b$10$96Ex/YWXSBAyD/H5Z9hy6OKoSpQZ1WGz1lylSEm36eP7riPdGFQz.',1,0,NULL,2,'2021-07-30 13:17:13'),(44,'bazikam se','$2b$10$pKiN7IMXklWNYJQ0Wg8Ok.iqCHLMGftjLBlrK2/R2Cpqn6Aa6GqOy',1,0,NULL,2,'2021-07-30 14:05:49'),(45,'gameChanger','$2b$10$.ElFj/KGttrx3Kh0gRoHvOFskRM1i/uyFwG3pz2jKv.6akx7BXQpK',0,0,NULL,2,'2021-07-30 15:16:41'),(46,'shadowinthedark05','$2b$10$jo8V6kLM5nzwcwoXZtU.2./g7OeSDjehatIdzN8RU0ILcfMorucfK',0,0,NULL,2,'2021-07-30 15:49:45'),(47,'iveta_hr22','$2b$10$PXNosrStNe6YbgybOvvq6egwM9WTLNUbfmovJ9kIiIIeEaQf50j02',0,0,NULL,2,'2021-07-31 17:15:29'),(48,'anton_petrov','$2b$10$xekKi0I2VDU3HcP8Nx7ZxuZJV8MHeSInEjPrsom5IueuY3rTe1J5G',0,0,NULL,2,'2021-07-31 17:16:13'),(49,'hamstera','$2b$10$vGMm4oYEa5fhe15HsZBX/e0MuCyDWlaJgIofd2sJbdb26oY05r54y',0,0,NULL,2,'2021-07-31 17:16:37'),(50,'huawei','$2b$10$yjUD5E76Kk3EBnevr/Q1G.VV30GCmD5PXKb9m43MwPk/ZcwDVd/FO',0,0,NULL,2,'2021-07-31 17:17:18'),(51,'milen_petkov','$2b$10$ilqeAWkUARN5vbxjfz.fF.QhrFEWPpSZmy1iGyIAbDNpWPc8rjJHe',0,0,NULL,1,'2021-08-01 15:58:13');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'forum_database'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-01 16:04:02
