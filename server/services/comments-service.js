import { DEFAULT_USER_ROLE, isDeleted } from '../common/constants.js';
import errors from './service-errors.js';
import postFind from '../data/posts-data.js';

const readPostComments = commentsData => {
    return async (id) => {
        return await commentsData.getPostComments(id);
    };
};

const createPostComment = commentsData => async (id, data, userId) => {
    const { text } = data;
    const result = await postFind.getPostBy('id', id);
    if (!result) {
        return {
            error: errors.RECORD_NOT_FOUND,
            comment: null,
        };
    }
    const comment = await commentsData.createComments(text, userId, id);

    return { comment: comment, error: null };
};

const deletePostComment = commentsData => async (id, role, userId) => {
    const comment = await commentsData.getCommentBy('id', id);

    if (!comment) {
        return {
            error: errors.RECORD_NOT_FOUND,
            comment: null,
        };
    }
    if (role === DEFAULT_USER_ROLE) {
        if (comment.users_id !== userId) {
            return {
                error: errors.UNALLOWED_ACTION,
                comment: null,
            };
        }
    }
    const deletedComment = { ...comment, isDeleted };
    const _ = await commentsData.deleteComments(deletedComment, userId);

    return { error: null, comment: deletedComment };
};

const updateComment = commentsData => async (id, updateData, role, userId, postId) => {
    const comment = await commentsData.getCommentBy('id', id);
    const post = await postFind.getPostBy('id', postId);
    if (!post) {
        return {
            error: errors.RECORD_NOT_FOUND,
            post: null,
        };
    }
    if (!comment) {
        return {
            error: errors.RECORD_NOT_FOUND,
            comment: null,
        };
    }
    if (role === DEFAULT_USER_ROLE) {
        if (comment.users_id !== userId) {
            return {
                error: errors.UNALLOWED_ACTION,
                comment: null,
            };
        }
    }

    const updatedComment = { ...comment, ...updateData };
    const _ = await commentsData.update(updatedComment, userId);

    return { error: null, comment: updatedComment };
};

/**
 * Active sql request function from commentsData module for
 * getting the information about the likes of a comment
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} commentsData- the module that includes all sql request
 * @return {closure} - function that calls commentLikes function for 
 * getting the information about likes of a comment(count, people who have liked ect.)
 */

const getCommentLikes = commentsData => async (commentId) => {
    return await commentsData.commentLikes(commentId);
};

/**
 * Active sql request function from commentsData module for
 * setting a like to a comment
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} commentsData- the module that includes all sql request
 * @return {closure} - function that calls createLike function for 
 * setting a like to a comment
 */

const createCommentLike = commentsData => async (commentId, userId, postId) => {
    return await commentsData.createLike(userId, commentId, postId);
};

/**
 * Active sql request function from commentsData module for
 * setting a dislike to a comment
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} commentsData- the module that includes all sql request
 * @return {closure} - function that calls deleteLike function for 
 * setting a dislike to a comment
 */

const deleteCommentLike = commentsData => async (commentId, userId, postId) => {
    return await commentsData.deleteLike(commentId, userId, postId);
};

/**
 * Active sql request function from commentsData module for
 * getting all comments count
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} commentsData- the module that includes all sql request
 * @return {closure} - function that calls allComments function for 
 * getting all comments count
 */

const getAllCommentsCount = commentsData => async () => {
    return await commentsData.allComments();
};

export default {
    createPostComment,
    readPostComments,
    deletePostComment,
    updateComment,
    getCommentLikes,
    createCommentLike,
    deleteCommentLike,
    getAllCommentsCount,
};