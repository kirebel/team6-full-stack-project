
import bcrypt from 'bcrypt';
import { DEFAULT_USER_ROLE } from '../common/constants.js';
import errors from './service-errors.js';

const signInUser = usersData => {
    return async (username, password) => {
        const user = await usersData.getWithRole(username);
        if (!user || !(await bcrypt.compare(password, user.password))) {
            return {
                error: errors.INVALID_SIGNIN,
                user: null
            };
        }

        return {
            error: null,
            user: user
        };
    };
};

const getUserCommentsActivity = usersData => {
    return async (id) => {
        const result = await usersData.getCommentsActivity(id);
        return result;
    };
};

const getUserPostsActivity = usersData => {
    return async (id) => {
        const result = await usersData.getPostsActivity(id);
        return result;
    };
};

const getUserUsersActivity = usersData => {
    return async (id) => {
        const result = await usersData.getUsersActivity(id);
        return result;
    };
};

const getAllUsers = usersData => {
    return async (filter) => {
        return filter
            ? await usersData.searchBy('username', filter)
            : await usersData.getAll();
    };
};

const getUserById = usersData => {
    return async (id) => {
        const user = await usersData.getBy('id', id);

        if (!user) {
            return {
                error: errors.RECORD_NOT_FOUND,
                user: null
            };
        }

        return { error: null, user: user };
    };
};

const createUser = usersData => {
    return async (userCreate) => {
        const { username, password } = userCreate;

        const existingUser = await usersData.checkForDuplicateUsername('username', username);
        if (existingUser) {
            return {
                error: errors.DUPLICATE_RECORD,
                user: null
            };
        }

        const passwordHash = await bcrypt.hash(password, 10);
        const user = await usersData.create(username, passwordHash, DEFAULT_USER_ROLE);

        return { error: null, user: user };
    };
};

const updateUser = usersData => {
    return async (id, userUpdate, role, userId) => {
        const user = await usersData.getBy('id', id);
        if (!user) {
            return {
                error: errors.RECORD_NOT_FOUND,
                user: null
            };
        }
        if (role === DEFAULT_USER_ROLE) {
            if (id !== userId) {
                return {
                    error: errors.UNALLOWED_ACTION,
                    user: null,
                };
            }
        }
        if (userUpdate.username && !(await usersData.getBy('username', userUpdate.username))) {
            return {
                error: errors.DUPLICATE_RECORD,
                user: null
            };
        }

        const updated = { ...user, ...userUpdate };
        const _ = await usersData.update(updated);

        return { error: null, user: updated };
    };
};

const deleteUser = usersData => {
    return async (id) => {
        const userToDelete = await usersData.getBy('id', id);
        if (!userToDelete) {
            return {
                error: errors.RECORD_NOT_FOUND,
                user: null
            };
        }

        const _ = await usersData.remove(userToDelete);

        return { error: null, user: userToDelete };
    };
};

const banUser = usersData => {
    return async (userId, id, banDescription) => {
        const userToBan = await usersData.getBy('id', id);
        if (!userToBan) {
            return {
                error: errors.RECORD_NOT_FOUND,
                user: null
            };
        }
        const bannedUser = { ...userToBan, ...banDescription };
        const _ = await usersData.ban(userId, bannedUser);

        return { error: null, user: bannedUser };

    };
};

const unbanUser = usersData => {
    return async (userId, id) => {
        const userToUnban = await usersData.getBy('id', id);
        if (!userToUnban) {
            return {
                error: errors.RECORD_NOT_FOUND,
                user: null
            };
        }
        const _ = await usersData.unban(userId, userToUnban.id);

        return { error: null, user: userToUnban };
    };
};

const requestFriendShip = usersData => {
    return async (requestId, secondId) => {
        const result = await usersData.sendFriendRequest(requestId, secondId);
        return result;
    };
};

const acceptFriendShip = usersData => {
    return async (acceptId, otherId) => {
        const result = await usersData.acceptFriendRequest(acceptId, otherId);
        return result;
    };
};

/**
 * Active sql request function from usersData module for
 * unfriend or decline friendship 
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} commentsData- the module that includes all sql request
 * @return {closure} - function that calls unfriend function for 
 * unfriend or decline friendship 
 */

const deleteFriendship = usersData => {
    return async (firstId, secondId) => {
        const result = await usersData.unfriend(firstId, secondId);
        return result;
    };
};

/**
 * Active sql request function from usersData module for
 * getting the information about friendships in the forum
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} commentsData- the module that includes all sql request
 * @return {closure} - function that calls friendsInfo function for 
 * getting the information about friendships in the forum
 */

const getFriendshipInfo = usersData => {
    return async () => {
        const result = await usersData.friendsInfo();
        return result;
    };
};

/**
 * Active sql request function from usersData module for
 * getting the count of all liked posts by the user
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} commentsData- the module that includes all sql request
 * @return {closure} - function that calls getUserLikedPosts function for 
 * getting the count of all liked posts by the user
 */

const getLikedPosts = usersData => {
    return async (userId) => {
        const result = await usersData.getUserLikedPosts(userId);
        return result;
    };
};

/**
 * Active sql request function from usersData module for
 * getting the count of all liked comments by the user
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} commentsData- the module that includes all sql request
 * @return {closure} - function that calls getUserCommentsLikes function for 
 * getting the count of all liked comments by the user
 */

const getLikedComments = usersData => {
    return async (userId) => {
        const result = await usersData.getUserCommentLikes(userId);
        return result;
    };
};

/**
 * Active sql request function from usersData module for
 * getting all users count from the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} commentsData- the module that includes all sql request
 * @return {closure} - function that calls getUsersCount function for 
 * getting all users count
 */

const usersCount = usersData => {
    return async () => {
        const result = await usersData.getUsersCount();
        return result;
    };
};

/**
 * Active sql request function from usersData module for
 * getting all friendships
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} commentsData- the module that includes all sql request
 * @return {closure} - function that calls getFriendshipsCount function for 
 * getting all friendships count
 */

const friendshipsCount = usersData => {
    return async () => {
        const result = await usersData.getFriendshipsCount();
        return result;
    };
};

export default {
    signInUser,
    getAllUsers,
    getUserById,
    createUser,
    updateUser,
    deleteUser,
    banUser,
    unbanUser,
    getUserCommentsActivity,
    getUserPostsActivity,
    requestFriendShip,
    acceptFriendShip,
    deleteFriendship,
    getFriendshipInfo,
    getUserUsersActivity,
    getLikedPosts,
    getLikedComments,
    friendshipsCount,
    usersCount
};
