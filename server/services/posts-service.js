
import errors from './service-errors.js';
import { DEFAULT_USER_ROLE, isDeleted } from '../common/constants.js';

/**
 * Active sql request function from postsData module for
 * getting all the posts in the database  
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} postsData- the module that includes all sql request
 * @return {closure} - function that calls functions from the postsData
 * module
 * v1- if there is a query parameter for search
 * v2 - if there isn't query parameter for search
 */

const getPosts = postsData => {
    return async (filter, userId) => filter
        ? await postsData.searchPostBy('title', filter, userId)
        : await postsData.getAllPosts();
};

/**
 * Active sql request function from postsData module for
 * getting all the flagged posts in the database  
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} postsData- the module that includes all sql request
 * @return {closure} - function that calls functions from the postsData
 * module
 */

const getFlaggedPosts = postsData => {
    return async () => {
        return await postsData.getFlagged();
    };
};

/**
 * Active sql request function from postsData module for
 * getting the last five posts published  
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} postsData- the module that includes all sql request
 * @return {closure} - function that calls functions from the postsData
 * module
 */

const getLastFivePosts = postsData => async () => {
    return await postsData.getLastPosts();
};

/**
 * Active sql request function from postsData module for
 * getting a single post by id  
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} postsData- the module that includes all sql request
 * @return {closure} - function that calls getPostBy function
 *  from the postsData module
 */

const getSinglePostById = postsData => async (id, userId) => {
    const post = await postsData.getPostBy('id', id, userId);
    if (!post) {
        return {
            error: errors.RECORD_NOT_FOUND,
            post: null,
        };
    }
    return { error: null, post: post };
};

/**
 * Active sql request function from postsData module for
 * creating a post
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} postsData- the module that includes all sql request
 * @return {closure} - function that calls create function
 *  from the postsData module
 */

const createPost = postsData => async (postData, userId) => {
    const { title, text } = postData;

    return await postsData.create(title, text, userId);
};

/**
 * Active sql request function from postsData module for
 * updating a post, searched by id
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} postsData- the module that includes all sql request
 * @return {closure} - function that calls getPostBy function for searching the post
 * and then update function for updating the post from the postsData module
 */

const updatePost = postsData => async (id, updateData, role, userId) => {
    const post = await postsData.getPostBy('id', id);
    if (!post) {
        return {
            error: errors.RECORD_NOT_FOUND,
            post: null,
        };
    }
    if (role === DEFAULT_USER_ROLE) {
        if (post.users_id !== userId) {
            return {
                error: errors.UNALLOWED_ACTION,
                post: null,
            };
        }
    }
    const updatedPost = { ...post, ...updateData };
    const _ = await postsData.update(updatedPost, userId);

    return { error: null, post: updatedPost };
};

/**
 * Active sql request function from postsData module for
 * deleting a post, searched by id
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} postsData- the module that includes all sql request
 * @return {closure} - function that calls getPostBy function for searching the post
 * and then deleteP function for deleting the post from the postsData module
 */

const deletePost = postsData => async (id, userId, role) => {
    const post = await postsData.getPostBy('id', id);
    if (!post) {
        return {
            error: errors.RECORD_NOT_FOUND,
            post: null,
        };
    }
    if (role === DEFAULT_USER_ROLE) {
        if (post.users_id !== userId) {
            return {
                error: errors.UNALLOWED_ACTION,
                post: null,
            };
        }
    }
    const deletedPost = { ...post, isDeleted };
    const _ = await postsData.deleteP(deletedPost, userId);

    return { error: null, post: deletedPost };
};

/**
 * Active sql request function from postsData module for
 * getting all the likes of a post
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} postsData- the module that includes all sql request
 * @return {closure} - function that calls postsLikes function for 
 * getting all the likes of a post from the postsData module
 */

const getPostsLikes = postsData => async (postId) => {
    return await postsData.postsLikes(postId);
};

/**
 * Active sql request function from postsData module for
 * setting like to a single post
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} postsData- the module that includes all sql request
 * @return {closure} - function that calls createLike function for 
 * set like  to a post from the postsData module
 */

const createPostLike = postData => async (userId, postId) => {
    return await postData.createLike(userId, postId);
};

/**
 * Active sql request function from postsData module for
 * setting dislike to a single post
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} postsData- the module that includes all sql request
 * @return {closure} - function that calls deleteLike function for 
 * set dislike to a post from the postsData module
 */

const deletePostLike = postData => async (userId, postId) => {
    return await postData.deleteLike(userId, postId);
};

/**
 * Active sql request function from postsData module for
 * locking a post
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} postsData- the module that includes all sql request
 * @return {closure} - function that calls getPostBy function for 
 * searching the post and then lock function for locking the post
 * from the postsData module
 */

const lockPost = postsData => {
    return async (userId, id) => {

        const post = await postsData.getPostBy('id', id);
        if (!post) {
            return {
                error: errors.RECORD_NOT_FOUND,
                post: null,
            };
        }

        const _ = await postsData.lock(userId, id);

        return { error: null, post: post };
    };
};

/**
 * Active sql request function from postsData module for
 * unlocking a post
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} postsData- the module that includes all sql request
 * @return {closure} - function that calls getPostBy function for 
 * searching the post and then unLock function for locking the post
 * from the postsData module
 */

const unLockPost = postsData => {
    return async (userId, id) => {
        const post = await postsData.getPostBy('id', id);
        if (!post) {
            return {
                error: errors.RECORD_NOT_FOUND,
                post: null,
            };
        }

        const _ = await postsData.unLock(userId, id);

        return { error: null, post: post };
    };
};

/**
 * Active sql request function from postsData module for
 * flagging post
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} postsData- the module that includes all sql request
 * @return {closure} - function that calls getPostBy function for 
 * searching the post and then flag function for flagging the post
 * from the postsData module
 */

const flagPost = postsData => {
    return async (userId, id) => {
        const post = await postsData.getPostBy('id', id);
        if (!post) {
            return {
                error: errors.RECORD_NOT_FOUND,
                post: null,
            };
        }

        const _ = await postsData.flag(userId, id);

        return { error: null, post: post };
    };
};

/**
 * Active sql request function from postsData module for
 * unflagging post
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} postsData- the module that includes all sql request
 * @return {closure} - function that calls getPostBy function for 
 * searching the post and then unFlag function for unflagging the post
 * from the postsData module
 */

const unFlagPost = postsData => {
    return async (userId, id) => {
        const post = await postsData.getPostBy('id', id);
        if (!post) {
            return {
                error: errors.RECORD_NOT_FOUND,
                post: null,
            };
        }

        const _ = await postsData.unFlag(userId, id);

        return { error: null, post: post };
    };
};

/**
 * Active sql request function from postsData module for
 * the 3 most liked posts
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} postsData- the module that includes all sql request
 * @return {closure} - function that calls getMostLikedPosts function for 
 * getting the 3 most liked posts from the postsData module
 */

const mostLiked = postsData => {
    return async () => {
        const result = await postsData.getMostLikedPosts();
        return result;
    };
};

/**
 * Active sql request function from postsData module for
 * searching the 3 most commented posts
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} postsData- the module that includes all sql request
 * @return {closure} - function that calls getMostCommentedPosts function for 
 * getting the 3 most commented posts from the postsData module
 */

const mostCommented = postsData => {
    return async () => {
        const result = await postsData.getMostCommentedPosts();
        return result;
    };
};

/**
 * Active sql request function from postsData module for
 * getting the count of all posts in the database 
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} postsData- the module that includes all sql request
 * @return {closure} - function that calls getPostsCount function for 
 * getting all posts count
 */

const allPosts = postsData => {
    return async () => {
        const result = await postsData.getPostsCount();
        return result;
    };
};

/**
 * Active sql request function from postsData module for
 * getting the count of all likes in the database
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} postsData- the module that includes all sql request
 * @return {closure} - function that calls getAllLikesCount function for 
 * getting all likes count in the database
 */

const allLikes = postsData => {
    return async () => {
        const result = await postsData.getAllLikesCount();
        return result;
    };
};

export default {
    getPosts,
    getLastFivePosts,
    getSinglePostById,
    createPost,
    updatePost,
    deletePost,
    getPostsLikes,
    createPostLike,
    deletePostLike,
    lockPost,
    unLockPost,
    flagPost,
    unFlagPost,
    getFlaggedPosts,
    mostLiked,
    mostCommented,
    allPosts,
    allLikes,
};