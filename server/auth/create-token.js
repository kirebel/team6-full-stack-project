import jwt from 'jsonwebtoken';
import { TOKEN_LIFETIME } from '../common/constants.js';
import dotenv from 'dotenv';

const SECRET_KEY = dotenv.config().parsed.SECRET_KEY;

const createToken = (payload) => {
    const token = jwt.sign(
        payload,
        SECRET_KEY,
        { expiresIn: TOKEN_LIFETIME }
    );

    return token;
};

export default createToken;
