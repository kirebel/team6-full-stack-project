# Team 6 full-stack project - Telerik forum


## 1. Description

This is a forum project with different functionalities. The application will be build using the best practices of functional programming.


## 2. Project information
* Language and version: JavaScript ES2020
* Platform and version: Node 14.0+
* Core Packages: Express.js, React.js

## 3. Project architecture
* database part  - MariaDB server
* server part - build on Express.js package
* client part - build on React.js package

## 4. Setup
1. Upload the database to your database tool (MySQL Workbench, ect.)
2. Go inside the server folder.
3. Create a .env file with the following content:
<br>
<br>
PORT=3001
<br>
HOST=localhost
<br>
DB_PORT=3306
<br>
USER=root
<br>
PASSWORD= *Your MariaDB password*
<br>
DATABASE=forum_database
<br>
SECRET_KEY=kir3_e_got3n
<br>

4. Run npm install to restore all dependencies
5. Go inside the forum folder.
6. Run npm install to restore all dependencies
7. Starting the server: <br>
In the server folder run:
 * npm run start (shorthand: npm start) - Runs the src/main.js file  - for normal start of the server
 * npm run start:dev - Runs the src/main.js file - for developer mode start of the server
8. Starting the client:
In the forum folder run:
* npm run start(shorthand :npm start) - Runs the src/main.js file 

## 5. Users
All registered users are with default role 'user'.
For explore the full functionality of the forum:
<br>
Admin accounts:
<br>
<br>
Username : kirebel
<br>
Password :asencho
<br>
<br>
Username : milen_petkov
<br>
Password: 123456

### Happy hacking :)

